import * as helmet from 'helmet';

import { ClassSerializerInterceptor, Logger } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { NestFactory, Reflector } from '@nestjs/core';

import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
    /* Creates an instance of the NestApplication */
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    const config = app.get(ConfigService);

    /* Add global Express Middlewares */
    app.enableCors();
    app.use(helmet());
    app.useWebSocketAdapter(new IoAdapter(app));
    // app.use(
    //     rateLimit({
    //         windowMs: 10 * 60 * 1000, // 10 minutes
    //         max: 100, // limit each IP to 100 requests per windowMs
    //     }),
    // );
    app.useGlobalInterceptors(
        new ClassSerializerInterceptor(app.get(Reflector)),
    );
    /* Swagger */
    const options = new DocumentBuilder()
        .setTitle(config.get('app.title'))
        .setVersion(config.get('app.version'))
        .addBearerAuth()
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('doc', app, document);

    const logger = new Logger('Bootstrap');
    await app.listen(config.get('app.port'), () => {
        logger.log(
            `Server is listen on http://localhost:${config.get('app.port')}`,
        );
    });
}

bootstrap();
