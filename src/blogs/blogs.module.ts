import { BlogCategory } from './blog-category.entity';
import { BlogPost } from './blog-post.entity';
import { BlogsController } from './blogs.controller';
import { BlogsService } from './blogs.service';
import { CommonHelperService } from 'shared/helpers/common-helper.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([BlogPost, BlogCategory])],
    controllers: [BlogsController],
    providers: [BlogsService, CommonHelperService],
})
export class BlogsModule {}
