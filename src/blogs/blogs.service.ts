import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository, SelectQueryBuilder } from 'typeorm';

import { BlogCategory } from './blog-category.entity';
import { BlogCategoryPaginateQuery } from './dto/blog-category.query';
import { BlogCategoryStatus } from './constant/blog-category-status.enum';
import { BlogCategoryUpdateDTO } from './dto/blog-category-update.dto';
import { BlogPost } from './blog-post.entity';
import { BlogPostPaginateQuery } from './dto/blog-post.query';
import { BlogPostStatus } from './constant/blog-post-status.enum';
import { BlogUpdateDTO } from './dto/blog-update.dto';
import { CommonHelperService } from 'shared/helpers/common-helper.service';
import { InjectRepository } from '@nestjs/typeorm';
import { ShowBlogCategoryPaginateQuery } from './dto/show-blog-category.query';
import { ShowBlogPostPaginateQuery } from './dto/show-blog-post.query';
import { paginate } from 'shared/helpers/pagination';

@Injectable()
export class BlogsService {
    constructor(
        @InjectRepository(BlogCategory)
        private blogCategoriesRepository: Repository<BlogCategory>,
        @InjectRepository(BlogPost)
        private blogPostsRepository: Repository<BlogPost>,
        private readonly commonHelperService: CommonHelperService,
    ) {}

    /** Category */

    async listOnlyShowCategoriesPaginated(
        query: ShowBlogCategoryPaginateQuery,
    ) {
        return paginate<BlogCategory>(
            this.blogCategoriesRepository,
            query,
            (builder: SelectQueryBuilder<BlogCategory>) => {
                builder.andWhere(
                    `BlogCategory.status = '${BlogCategoryStatus.Show}'`,
                );
            },
        );
    }

    async listFullCategoriesPaginated(query: BlogCategoryPaginateQuery) {
        return paginate<BlogCategory>(this.blogCategoriesRepository, query);
    }

    async findOneCategoryById(categoryId: string) {
        return this.blogCategoriesRepository.findOne(categoryId);
    }

    async createCategory(category: BlogCategory) {
        return this.blogCategoriesRepository.save(category);
    }

    async updateCategory(
        id: string,
        updateInfo: BlogCategoryUpdateDTO,
    ): Promise<void> {
        const category = await this.blogCategoriesRepository.findOne(id);
        if (!category) {
            throw new NotFoundException('Danh mục bài viết không tồn tại');
        }

        await this.blogCategoriesRepository.update(id, updateInfo);

        if (updateInfo.status) {
            const status =
                updateInfo.status === 'show'
                    ? BlogPostStatus.Show
                    : BlogPostStatus.Hide;

            await this.blogPostsRepository.update(
                { categoryId: category.id },
                { status },
            );
        }
    }

    /** Blog post */

    async listOnlyShowBlogPostsPaginated(query: ShowBlogPostPaginateQuery) {
        return paginate<BlogPost>(
            this.blogPostsRepository,
            query,
            (builder: SelectQueryBuilder<BlogPost>) => {
                builder
                    .leftJoinAndSelect('BlogPost.category', 'category')
                    .andWhere(`BlogPost.status = '${BlogPostStatus.Show}'`);
            },
        );
    }

    async listFullBlogPostsPaginated(query: BlogPostPaginateQuery) {
        return paginate<BlogPost>(
            this.blogPostsRepository,
            query,
            (builder: SelectQueryBuilder<BlogPost>) => {
                builder.leftJoinAndSelect('BlogPost.category', 'category');
            },
        );
    }

    async findOneBlogPostById(blogPostId: string) {
        return this.blogPostsRepository.findOne({
            where: {
                id: blogPostId,
            },
            relations: ['category'],
        });
    }

    async createBlog(blog: BlogPost) {
        const thumbnail = this.commonHelperService.findBlogThumbnail(
            blog.content,
        );
        const summary = this.commonHelperService.findBlogSummary(blog.content);

        blog.thumbnail = thumbnail;
        blog.summary = summary;

        return this.blogPostsRepository.save(blog);
    }

    async updateBlog(id: string, updateInfo: BlogUpdateDTO): Promise<void> {
        const blog = await this.blogPostsRepository.findOne(id);
        if (!blog) {
            throw new NotFoundException('Bài viết không tồn tại');
        }

        if (updateInfo.categoryId) {
            const category = await this.blogCategoriesRepository.findOne(
                updateInfo.categoryId,
            );
            if (!category) {
                throw new NotFoundException('Danh mục bài viết không tồn tại');
            }
        }

        if (updateInfo.content && updateInfo.content.trim().length) {
            const thumbnail = this.commonHelperService.findBlogThumbnail(
                updateInfo.content,
            );
            const summary = this.commonHelperService.findBlogSummary(
                blog.content,
            );
            await this.blogPostsRepository.update(id, {
                ...updateInfo,
                thumbnail,
                summary,
            });
        } else {
            await this.blogPostsRepository.update(id, updateInfo);
        }
    }

    async deleteBlogPost(blogPostId: string) {
        return this.blogPostsRepository.delete(blogPostId);
    }
}
