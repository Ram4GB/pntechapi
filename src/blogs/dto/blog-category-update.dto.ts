import { IsEnum, IsOptional } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { BlogCategoryStatus } from 'blogs/constant/blog-category-status.enum';

export class BlogCategoryUpdateDTO {
    @ApiPropertyOptional()
    @IsOptional()
    name: string;

    @ApiPropertyOptional({ default: BlogCategoryStatus.Show })
    @IsOptional()
    @IsEnum(BlogCategoryStatus)
    status: BlogCategoryStatus;
}
