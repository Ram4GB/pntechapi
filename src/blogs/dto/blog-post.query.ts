import { IsEnum, IsOptional } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { BlogPostStatus } from 'blogs/constant/blog-post-status.enum';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class BlogPostFilter {
    @IsOptional()
    title: any;

    @IsOptional()
    createdById: any;

    @IsOptional()
    categoryId: any;

    @IsOptional()
    @IsEnum(BlogPostStatus)
    status: BlogPostStatus;
}

export class BlogPostPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            title: 'like',
            createdById: '=',
            categoryId: '=',
            status: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            title: 'title',
            createdById: 'user uuid',
            categoryId: 'uuid',
            status: 'show',
        },
    })
    @IsOptional()
    @Type(() => BlogPostFilter)
    filter: BlogPostFilter;
}
