import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class ShowBlogCategoryFilter {
    @IsOptional()
    name: any;
}

export class ShowBlogCategoryPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            name: 'like',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            name: 'name',
        },
    })
    @IsOptional()
    @Type(() => ShowBlogCategoryFilter)
    filter: ShowBlogCategoryFilter;
}
