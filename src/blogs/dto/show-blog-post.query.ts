import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class ShowBlogPostFilter {
    @IsOptional()
    title: any;

    @IsOptional()
    createdById: any;

    @IsOptional()
    categoryId: any;
}

export class ShowBlogPostPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            title: 'like',
            createdById: '=',
            categoryId: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            title: 'title',
            createdById: 'user uuid',
            categoryId: 'uuid',
        },
    })
    @IsOptional()
    @Type(() => ShowBlogPostFilter)
    filter: ShowBlogPostFilter;
}
