import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsUUID } from 'class-validator';

import { BlogPostStatus } from 'blogs/constant/blog-post-status.enum';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class BlogCreateDTO {
    @ApiProperty()
    @IsNotEmptyString()
    title: string;

    @ApiProperty()
    @IsNotEmptyString()
    content: string;

    @ApiPropertyOptional({ default: BlogPostStatus.Show })
    @IsOptional()
    @IsEnum(BlogPostStatus)
    status: BlogPostStatus;

    @ApiProperty()
    @IsUUID()
    categoryId: string;
}
