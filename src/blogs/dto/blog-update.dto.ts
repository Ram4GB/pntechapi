import { IsEnum, IsOptional } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { BlogPostStatus } from 'blogs/constant/blog-post-status.enum';

export class BlogUpdateDTO {
    @ApiPropertyOptional()
    @IsOptional()
    title: string;

    @ApiPropertyOptional()
    @IsOptional()
    content: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsEnum(BlogPostStatus)
    status: BlogPostStatus;

    @ApiPropertyOptional()
    @IsOptional()
    categoryId: string;
}
