import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';

import { BlogCategoryStatus } from 'blogs/constant/blog-category-status.enum';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class BlogCategoryCreateDTO {
    @ApiProperty()
    @IsNotEmptyString()
    name: string;

    @ApiPropertyOptional({ default: BlogCategoryStatus.Show })
    @IsOptional()
    @IsEnum(BlogCategoryStatus)
    status: BlogCategoryStatus;
}
