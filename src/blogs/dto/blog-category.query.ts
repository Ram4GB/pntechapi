import { IsEnum, IsOptional } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { BlogCategoryStatus } from 'blogs/constant/blog-category-status.enum';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class BlogCategoryFilter {
    @IsOptional()
    name: any;

    @IsOptional()
    @IsEnum(BlogCategoryStatus)
    status: BlogCategoryStatus;
}

export class BlogCategoryPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            name: 'like',
            status: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            name: 'name',
            status: 'show',
        },
    })
    @IsOptional()
    @Type(() => BlogCategoryFilter)
    filter: BlogCategoryFilter;
}
