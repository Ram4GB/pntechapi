import {
    BadRequestException,
    Body,
    Controller,
    Get,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { BlogCategory } from './blog-category.entity';
import { BlogCategoryCreateDTO } from './dto/blog-category-create.dto';
import { BlogCategoryPaginateQuery } from './dto/blog-category.query';
import { BlogCategoryUpdateDTO } from './dto/blog-category-update.dto';
import { BlogCreateDTO } from './dto/blog-create.dto';
import { BlogPost } from './blog-post.entity';
import { BlogPostPaginateQuery } from './dto/blog-post.query';
import { BlogUpdateDTO } from './dto/blog-update.dto';
import { BlogsService } from './blogs.service';
import { PagedList } from 'shared/helpers/pagination';
import { ReqUser } from 'shared/decorators/request-user.decorator';
import { RequestUser } from 'auth/dto/req-user.interface';
import { Roles } from 'shared/constants/roles';
import { ShowBlogCategoryPaginateQuery } from './dto/show-blog-category.query';
import { ShowBlogPostPaginateQuery } from './dto/show-blog-post.query';

@Controller('blogs')
@ApiTags('Blogs')
export class BlogsController {
    constructor(private readonly blogsService: BlogsService) {}

    @Authorize(Roles.Anonymous)
    @Get('categories')
    async listCategories(
        @ReqUser() user: RequestUser,
        @Query() queryFull: BlogCategoryPaginateQuery,
        @Query() query: ShowBlogCategoryPaginateQuery,
    ): Promise<PagedList<BlogCategory>> {
        if (user.role === Roles.Admin) {
            return this.blogsService.listFullCategoriesPaginated(queryFull);
        } else {
            return this.blogsService.listOnlyShowCategoriesPaginated(query);
        }
    }

    @Get('categories/:id')
    async findBlogCategoryById(@Param('id') id: string): Promise<BlogCategory> {
        const blogCategory = await this.blogsService.findOneCategoryById(id);
        if (!blogCategory) {
            throw new NotFoundException('Danh mục bài viết không tồn tại');
        }
        return blogCategory;
    }

    @Authorize(Roles.Admin)
    @Post('categories')
    async createCategory(
        @Body() categoryCreateInfo: BlogCategoryCreateDTO,
    ): Promise<void> {
        const category = new BlogCategory({
            ...categoryCreateInfo,
        });

        await this.blogsService.createCategory(category);
    }

    @Authorize(Roles.Admin)
    @Put('categories/:id')
    async updateCategoryPost(
        @Param('id') id: string,
        @Body() categoryUpdateInfo: BlogCategoryUpdateDTO,
    ): Promise<void> {
        await this.blogsService.updateCategory(id, categoryUpdateInfo);
    }

    @Authorize(Roles.Anonymous)
    @Get('posts')
    async listBlogs(
        @ReqUser() user: RequestUser,
        @Query() queryFull: BlogPostPaginateQuery,
        @Query() query: ShowBlogPostPaginateQuery,
    ): Promise<PagedList<BlogPost>> {
        if (user.role === Roles.Admin) {
            return this.blogsService.listFullBlogPostsPaginated(queryFull);
        } else {
            return this.blogsService.listOnlyShowBlogPostsPaginated(query);
        }
    }

    @Get('posts/:id')
    async findBlogPostById(@Param('id') id: string): Promise<BlogPost> {
        const blogPost = await this.blogsService.findOneBlogPostById(id);
        if (!blogPost) {
            throw new NotFoundException('Bài viết không tồn tại');
        }
        return blogPost;
    }

    @Authorize(Roles.Admin)
    @Post('posts')
    async create(
        @Body() blogCreateInfo: BlogCreateDTO,
        @ReqUser() requestUser: RequestUser,
    ): Promise<void> {
        const category = await this.blogsService.findOneCategoryById(
            blogCreateInfo.categoryId,
        );
        if (!category) {
            throw new BadRequestException('Danh mục bài viết không tồn tại');
        }

        const blog = new BlogPost({
            ...blogCreateInfo,
            createdById: requestUser.id,
        });

        await this.blogsService.createBlog(blog);
    }

    @Authorize(Roles.Admin)
    @Put('posts/:id')
    async updateBlogPost(
        @Param('id') id: string,
        @Body() blogUpdateInfo: BlogUpdateDTO,
    ): Promise<void> {
        await this.blogsService.updateBlog(id, blogUpdateInfo);
    }
}
