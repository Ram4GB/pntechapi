import {
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { BlogCategoryStatus } from './constant/blog-category-status.enum';
import { BlogPost } from './blog-post.entity';
import { Expose } from 'class-transformer';

@Entity()
export class BlogCategory {
    constructor(partial: Partial<BlogCategory>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Column()
    name: string;

    @Column({
        type: 'enum',
        enum: BlogCategoryStatus,
        default: BlogCategoryStatus.Show,
    })
    status: BlogCategoryStatus;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(
        type => BlogPost,
        blogPost => blogPost.category,
    )
    blogPosts: BlogPost[];
}
