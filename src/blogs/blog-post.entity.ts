import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { BlogCategory } from './blog-category.entity';
import { BlogPostStatus } from './constant/blog-post-status.enum';
import { Expose } from 'class-transformer';
import { User } from 'users/user.entity';

@Entity()
export class BlogPost {
    constructor(partial: Partial<BlogPost>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Column()
    title: string;

    @Column({ nullable: true })
    thumbnail: string;

    @Column({ nullable: true })
    summary: string;

    @Column({ type: 'text' })
    content: string;

    @Column({
        type: 'enum',
        enum: BlogPostStatus,
        default: BlogPostStatus.Show,
    })
    status: BlogPostStatus;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    categoryId: string;

    @Column()
    createdById: string;

    @ManyToOne(
        type => BlogCategory,
        category => category.blogPosts,
    )
    category: BlogCategory;

    @ManyToOne(type => User)
    createdBy: User;
}
