import { ChatGateway } from './chat.gateway';
import { ChatMessage } from './chat-message.entity';
import { ChatRoom } from './chat-room.entity';
import { ChatsController } from './chats.controller';
import { ChatsService } from './chats.services';
import { ConfigModule } from 'config/config.module';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { Participants } from './participants.entity';
import { SocketGuard } from './socket.guard';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'users/users.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([ChatRoom, ChatMessage, Participants]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => {
                return {
                    secret: configService.get<string>('JWT_SECRET_KEY'),
                    signOptions: {
                        expiresIn: configService.get<string>(
                            'JWT_EXPIRATION_TIME',
                        ),
                    },
                };
            },
            inject: [ConfigService],
        }),
        UsersModule,
    ],
    controllers: [ChatsController],
    providers: [ChatGateway, ChatsService, SocketGuard],
    exports: [ChatsService],
})
export class ChatsModule {}
