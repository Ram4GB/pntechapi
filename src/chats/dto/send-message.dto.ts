export interface SendMessageDTO {
    room: string;
    message: string;
}
