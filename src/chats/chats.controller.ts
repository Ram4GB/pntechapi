import { Controller, Get, Param } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { ChatMessage } from './chat-message.entity';
import { ChatsService } from './chats.services';
import { Participants } from './participants.entity';
import { ReqUser } from 'shared/decorators/request-user.decorator';
import { RequestUser } from 'auth/dto/req-user.interface';
import { Roles } from 'shared/constants/roles';
import { plainToClass } from 'class-transformer';

@Controller('chats')
@ApiTags('Chats')
export class ChatsController {
    constructor(private readonly chatsService: ChatsService) {}

    @Get('my-rooms')
    @Authorize(Roles.Customer, Roles.Admin, Roles.Seller)
    async listMyRoom(@ReqUser() user: RequestUser) {
        const participants = await this.chatsService.listRoomByUserId(user.id);
        return { data: plainToClass(Participants, participants) };
    }

    @Get(':roomId/messages')
    @Authorize(Roles.Customer, Roles.Seller, Roles.Admin)
    async listRoomIamSupporting(@Param('roomId') roomId: string) {
        //validate user is in room
        const messages = await this.chatsService.listMessageByRoomId(roomId);
        return { data: plainToClass(ChatMessage, messages) };
    }

    @Get()
    @Authorize(Roles.Admin)
    async listRoomIsNotSupportedYet(): Promise<void> {}
}
