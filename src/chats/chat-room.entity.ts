import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Expose, Transform } from 'class-transformer';

import { ChatMessage } from './chat-message.entity';
import { Order } from 'orders/entities/order.entity';
import { Participants } from './participants.entity';
import { RoomStatus } from './constant/room-status';
import { parseOrderIdToOrderCode } from 'orders/helpers/parse-order-id.helper';

@Entity()
export class ChatRoom {
    constructor(partial: Partial<ChatRoom>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Column({ type: 'enum', enum: RoomStatus, default: RoomStatus.Opening })
    status: RoomStatus;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Expose()
    @Transform(value => parseOrderIdToOrderCode(value))
    @Column({ unique: true })
    orderId: number;

    @OneToOne(type => Order)
    @JoinColumn()
    order: Order;

    @OneToMany(
        type => Participants,
        participants => participants.chatRoom,
    )
    participants: Participants[];

    @OneToMany(
        type => ChatMessage,
        message => message.room,
    )
    messages: ChatMessage[];
}
