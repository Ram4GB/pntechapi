import { EntityManager, IsNull, Repository } from 'typeorm';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';

import { ChatMessage } from './chat-message.entity';
import { ChatRoom } from './chat-room.entity';
import { MessageType } from './constant/message-type';
import { Participants } from './participants.entity';
import { RoomStatus } from './constant/room-status';
import { UsersService } from 'users/users.service';

@Injectable()
export class ChatsService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager: EntityManager,
        @InjectRepository(ChatRoom)
        private readonly chatRoomsRepository: Repository<ChatRoom>,
        @InjectRepository(ChatMessage)
        private readonly chatMessagesRepository: Repository<ChatMessage>,
        @InjectRepository(Participants)
        private readonly parcitipantsRepository: Repository<Participants>,
        private readonly usersService: UsersService,
    ) {}

    async createChatRoom(
        orderId: number,
        customerId: string,
        manager: EntityManager = this.entityManager,
    ): Promise<void> {
        const room = new ChatRoom({
            orderId,
        });

        await manager.save(room);

        /** Add customer to created chat room */
        await this.joinRoom(room.id, customerId, manager);

        /** Add all active admin to created chat room */
        const admins = await this.usersService.getActiveAdmins();
        if (admins) {
            const adminIds = admins.map(admin => admin.id);
            await this.joinManyIntoRoom(room.id, adminIds, manager);
        }
    }

    async joinRoom(
        chatRoomId: string,
        userId: string,
        manager: EntityManager,
    ): Promise<void> {
        const parcitipant = new Participants({
            userId,
            chatRoomId,
        });
        await manager.save(parcitipant);
    }

    async joinManyIntoRoom(
        chatRoomId: string,
        userIds: string[],
        manager: EntityManager,
    ): Promise<void> {
        const parcitipants = userIds.map(
            userId =>
                new Participants({
                    userId,
                    chatRoomId,
                }),
        );

        await manager.save(parcitipants);
    }

    async getParcitipantsByUserId(userId: string): Promise<Participants[]> {
        return this.parcitipantsRepository.find({
            where: {
                userId,
            },
        });
    }

    async listRoomIsNotSupportedYet(): Promise<ChatRoom[]> {
        return this.chatRoomsRepository.find({
            where: {
                status: RoomStatus.Opening,
                adminId: IsNull(),
            },
        });
    }

    async listRoomIsOpening(): Promise<ChatRoom[]> {
        return this.chatRoomsRepository.find({
            where: {
                status: RoomStatus.Opening,
            },
        });
    }

    async saveChatMessage(
        content: string,
        chatRoomId: string,
        senderId: string,
    ): Promise<void> {
        const message = new ChatMessage({
            roomId: chatRoomId,
            content,
            senderId,
            messageType: MessageType.Text,
        });

        await this.chatMessagesRepository.save(message);
    }

    async listRoomByUserId(userId: string): Promise<Participants[]> {
        return this.parcitipantsRepository
            .createQueryBuilder()
            .leftJoinAndSelect('Participants.chatRoom', 'ChatRoom')
            .where('Participants.userId = :userId', { userId })
            .getMany();
    }

    async listMessageByRoomId(roomId: string): Promise<ChatMessage[]> {
        const room = this.chatRoomsRepository.findOne({ id: roomId });
        if (!room) {
            throw new NotFoundException('Room not found');
        }
        return this.chatMessagesRepository
            .createQueryBuilder()
            .leftJoinAndSelect('ChatMessage.sender', 'Sender')
            .leftJoin('ChatMessage.room', 'ChatRoom')
            .where('ChatRoom.id = :roomId', { roomId })
            .getMany();
    }
}
