import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { Observable } from 'rxjs';
import { Socket } from 'socket.io';
import { WsException } from '@nestjs/websockets';

@Injectable()
export class SocketGuard implements CanActivate {
    constructor() {}

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const client: Socket = context.switchToWs().getClient();
        const token = client.handshake.headers['Authorization'];
        console.log(token);
        console.log('-----------------------');

        if (token && token === 'huyquyen') {
            return true;
        }
        throw new WsException('sdf');
    }

    matchRoles(roles: string[], userRole: string): boolean {
        return roles.includes(userRole);
    }
}
