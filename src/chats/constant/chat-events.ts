export enum ChatEvents {
    SEND_MESSAGE = 'sendMessage',
    NEW_MESSAGE = 'newMessage',
    JOIN_ROOM = 'joinRoom',
}
