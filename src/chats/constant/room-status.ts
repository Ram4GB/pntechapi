export enum RoomStatus {
    Opening = 'opening',
    Closed = 'closed',
}
