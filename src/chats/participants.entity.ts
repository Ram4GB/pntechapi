import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';

import { ChatRoom } from './chat-room.entity';
import { User } from 'users/user.entity';

@Entity()
export class Participants {
    constructor(partial: Partial<Participants>) {
        Object.assign(this, partial);
    }

    @PrimaryColumn()
    userId: string;

    @PrimaryColumn()
    chatRoomId: string;

    @Column({ default: 0 })
    unreadMessageCount: number;

    @Column({ default: false })
    muted: boolean;

    @ManyToOne(type => User)
    user: User;

    @ManyToOne(type => ChatRoom)
    chatRoom: ChatRoom;

    lastMessage: any;
}
