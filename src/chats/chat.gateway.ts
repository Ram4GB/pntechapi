import {
    ConnectedSocket,
    MessageBody,
    OnGatewayConnection,
    OnGatewayDisconnect,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

import { ChatEvents } from './constant/chat-events';
import { ChatsService } from './chats.services';
import { JwtService } from '@nestjs/jwt';
import { Logger } from '@nestjs/common';
import { RequestUser } from 'auth/dto/req-user.interface';
import { SendMessageDTO } from './dto/send-message.dto';
import { options } from 'shared/options/gateway.options';

// import { UseFilters, UseGuards } from '@nestjs/common';

// import { SocketGuard } from 'chats/socket.guard';

// @UseGuards(SocketGuard)
@WebSocketGateway({
    ...options,
    namespace: '/chat',
})
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer() wss: Server;

    private readonly logger: Logger;

    constructor(
        private readonly jwtService: JwtService,
        private readonly chatsService: ChatsService,
    ) {
        this.logger = new Logger(ChatGateway.name);
    }

    handleDisconnect(_client: any) {
        this.logger.log('Client disconnected');
    }

    async handleConnection(@ConnectedSocket() client: Socket, ..._args: any[]) {
        const token: string = client.handshake.headers['authorization'];
        if (!token) {
            this.logger.error('Client connected not found token in header');
            client.error('Unauthorize');
            client.disconnect();
        } else {
            try {
                const user: RequestUser = await this.jwtService.verifyAsync(
                    token,
                );
                client.request.user = user;

                /** Join room */
                const parcitipants = await this.chatsService.getParcitipantsByUserId(
                    user.id,
                );
                parcitipants.forEach(parcitipant =>
                    client.join(parcitipant.chatRoomId),
                );
                this.logger.log('Client connected');
            } catch (error) {
                this.logger.error('Client connected token is invalid');
                client.error('Unauthorize');
                client.disconnect();
            }
        }
    }

    @SubscribeMessage(ChatEvents.SEND_MESSAGE)
    async handleMessage(
        @MessageBody() data: SendMessageDTO,
        @ConnectedSocket() client: Socket,
    ) {
        try {
            await this.chatsService.saveChatMessage(
                data.message,
                data.room,
                client.request.user.id,
            );
            client.to(data.room).emit(ChatEvents.NEW_MESSAGE, {
                room: data.room,
                message: data.message,
                sender: client.request.user.email,
            });
            this.logger.log(
                `${client.request.user.email} send message to room ${data.room}`,
            );
        } catch (error) {
            this.logger.error(
                `Error when ${client.request.user.email} send message to room ${data.room}`,
            );
            this.wss
                .to(`${client.id}`)
                .emit('error', 'Lỗi mạng, vui lòng thử gửi lại tin nhắn khác');
        }
    }
}
