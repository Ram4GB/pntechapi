import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { ChatRoom } from './chat-room.entity';
import { Expose } from 'class-transformer';
import { MessageType } from './constant/message-type';
import { User } from 'users/user.entity';

@Entity()
export class ChatMessage {
    constructor(partial: Partial<ChatMessage>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Column({ type: 'text' })
    content: string;

    @Column()
    messageType: MessageType;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    senderId: string;

    @Column()
    roomId: string;

    @ManyToOne(type => User)
    sender: User;

    @ManyToOne(type => ChatRoom)
    room: ChatRoom;
}
