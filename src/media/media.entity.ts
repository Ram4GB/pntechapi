import {
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';

import { Exclude } from 'class-transformer';
import { MediaUsage } from './media-usage.entity';

@Entity()
export class Media {
    constructor(partial: Partial<Media>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column()
    uri: string;

    @Column()
    filemine: string;

    @Exclude()
    @OneToMany(
        type => MediaUsage,
        mediaUsage => mediaUsage.media,
    )
    mediaUsages: MediaUsage;

    @CreateDateColumn()
    createdAt: Date;

    @CreateDateColumn()
    updatedAt: Date;
}
