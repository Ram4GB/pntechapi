import { Injectable, NotFoundException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Media } from './media.entity';
import { MediaPaginateQuery } from './dto/media.query';
import { MediaUsage } from './media-usage.entity';
import { Repository } from 'typeorm';
import { UseMediaDTO } from './dto/use-media.dto';
import { paginate } from 'shared/helpers/pagination';

@Injectable()
export class MediaService {
    constructor(
        @InjectRepository(Media)
        private readonly mediaRepository: Repository<Media>,
        @InjectRepository(MediaUsage)
        private readonly mediaUsageRepository: Repository<MediaUsage>,
    ) {}

    async getOneMediaUsedByEntity(entityType: string, entityId: string) {
        const media = await this.mediaUsageRepository
            .createQueryBuilder('MediaUsage')
            .select('Media.id', 'id')
            .addSelect('Media.name', 'name')
            .addSelect('Media.uri', 'uri')
            .addSelect('Media.filemine', 'filemine')
            .addSelect('MediaUsage.weight', 'weight')
            .innerJoin(
                Media,
                'Media',
                'MediaUsage.mediaId = Media.id and MediaUsage.Type = :type',
                { type: entityType },
            )
            .where('MediaUsage.usedById = :id', { id: entityId })
            .getRawOne();
        return media;
    }

    async getAllMediaUsedByEntity(entityType: string, entityId: string) {
        const media = await this.mediaUsageRepository
            .createQueryBuilder('MediaUsage')
            .select('Media.id', 'id')
            .addSelect('Media.name', 'name')
            .addSelect('Media.uri', 'uri')
            .addSelect('Media.filemine', 'filemine')
            .addSelect('MediaUsage.weight', 'weight')
            .innerJoin(
                Media,
                'Media',
                'MediaUsage.mediaId = Media.id and MediaUsage.Type = :type',
                { type: entityType },
            )
            .where('MediaUsage.usedById = :id', { id: entityId })
            .getRawMany();
        return media;
    }

    async listMediaPaginated(query: MediaPaginateQuery) {
        return paginate<Media>(this.mediaRepository, query);
    }

    async create(media: Media[]): Promise<void> {
        await this.mediaRepository.save(media);
    }

    async validateMediaIds(ids: string[]): Promise<Media[]> {
        const media = await this.mediaRepository.findByIds(ids);
        if (!media) {
            throw new NotFoundException('Media not found');
        }
        return media;
    }

    async unUsedMediaAnymore(usedByType: string, usedById: string) {
        await this.mediaUsageRepository.delete({
            usedById,
            type: usedByType,
        });
    }

    async useMedia(
        usedByType: string,
        usedById: string,
        mediaId: string,
        weight?: number,
    ) {
        const media = await this.mediaRepository.findOne(mediaId);
        if (!media) {
            throw new NotFoundException('Media not found');
        }

        const mediaUsage = new MediaUsage({
            usedById,
            mediaId,
            type: usedByType,
        });

        if (weight) {
            mediaUsage.weight = weight;
        }

        await this.mediaUsageRepository.save(mediaUsage);
    }

    async useMultiMedia(
        usedByType: string,
        usedById: string,
        useMedia: UseMediaDTO[],
    ) {
        const mediaIds = useMedia.map(item => item.id);
        const media = await this.mediaRepository.findByIds(mediaIds);
        if (!media || media.length !== mediaIds.length) {
            throw new NotFoundException('Media not found');
        }

        const thumbnail = useMedia.find(item => item.weight === 1);
        if (!thumbnail) {
            useMedia[0].weight = 1;
        }

        const mediaUsages = useMedia.map(
            item =>
                new MediaUsage({
                    usedById,
                    type: usedByType,
                    mediaId: item.id,
                    weight: item.weight,
                }),
        );
        await this.mediaUsageRepository.save(mediaUsages);
    }
}
