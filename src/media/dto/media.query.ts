import { IsEnum, IsNumberString, IsOptional, IsString } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';
import { UserStatus } from 'users/constant/userstatus';

class MediaFilter {
    @IsOptional()
    email: string;

    @IsOptional()
    @IsString()
    firstName: string;

    @IsOptional()
    @IsString()
    lastName: string;

    @IsOptional()
    @IsEnum(UserStatus)
    status: UserStatus;

    @IsOptional()
    @IsNumberString()
    phone: string;
}

export class MediaPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            email: 'like',
            firstName: 'like',
            lastName: 'like',
            status: '=',
            phone: 'like',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            email: '@hotmail.com',
            status: UserStatus.Inactive,
        },
    })
    @IsOptional()
    @Type(() => MediaFilter)
    filter: MediaFilter;
}
