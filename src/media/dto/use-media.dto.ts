import { IsNotEmpty, IsUUID, Min } from 'class-validator';

import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class UseMediaDTO {
    @IsNotEmptyString()
    @IsUUID()
    id: string;

    @IsNotEmpty()
    @Min(1)
    weight: number;
}
