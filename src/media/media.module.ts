import { ConfigModule } from 'config/config.module';
import { Media } from './media.entity';
import { MediaController } from './media.controller';
import { MediaService } from './media.service';
import { MediaUsage } from './media-usage.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [ConfigModule, TypeOrmModule.forFeature([Media, MediaUsage])],
    providers: [MediaService],
    exports: [MediaService],
    controllers: [MediaController],
})
export class MediaModule {}
