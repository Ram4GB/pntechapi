import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';

import { Media } from './media.entity';

@Entity()
export class MediaUsage {
    constructor(partial: Partial<MediaUsage>) {
        Object.assign(this, partial);
    }

    @PrimaryColumn()
    mediaId: string;

    @PrimaryColumn()
    usedById: string;

    @Column()
    type: string;

    @Column({ default: 1 })
    weight: number;

    @ManyToOne(
        type => Media,
        media => media.mediaUsages,
    )
    media: Media;
}
