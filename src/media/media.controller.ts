import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import {
    BadRequestException,
    Controller,
    Get,
    HttpCode,
    Post,
    Query,
    UploadedFile,
    UploadedFiles,
    UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';

import { Authorize } from 'shared/decorators/authorize.decorator';
import { ConfigService } from '@nestjs/config';
import { Media } from './media.entity';
import { MediaPaginateQuery } from './dto/media.query';
import { MediaService } from './media.service';
import { PagedList } from 'shared/helpers/pagination';
import { multerBlogImageOptions } from 'shared/options/multer-blog-image.options';
import { multerImageOptions } from 'shared/options/multer-image.options';

type MulterFile = Express.Multer.File;

@Controller('media')
@ApiTags('Media')
export class MediaController {
    constructor(
        private readonly mediaService: MediaService,
        private readonly configService: ConfigService,
    ) {}

    @ApiBearerAuth()
    @Authorize()
    @UseInterceptors(FilesInterceptor('media', 20, multerImageOptions))
    @ApiBody({
        required: true,
        schema: {
            example: {
                media: 'Array one or more media files',
            },
        },
    })
    @Post()
    @HttpCode(201)
    async addMedia(@UploadedFiles() files: MulterFile[]): Promise<void> {
        if (!files || !files.length) {
            throw new BadRequestException('Không có file để upload');
        }
        const media: Media[] = files.map(
            file =>
                new Media({
                    name: file.originalname,
                    filemine: file.mimetype,
                    uri: file.path,
                }),
        );

        await this.mediaService.create(media);
    }

    @Authorize()
    @UseInterceptors(FileInterceptor('image', multerBlogImageOptions))
    @Post('blog-image')
    @HttpCode(201)
    async uploadImageForBlog(@UploadedFile() file: MulterFile) {
        if (!file) {
            throw new BadRequestException('Không có file để upload');
        }
        file.path =
            this.configService.get<string>('SERVER_URL') + '/' + file.path;
        return file;
    }

    @Authorize()
    @Get()
    async listMedia(
        @Query() query: MediaPaginateQuery,
    ): Promise<PagedList<Media>> {
        return this.mediaService.listMediaPaginated(query);
    }
}
