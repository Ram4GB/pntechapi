import { Module, forwardRef } from '@nestjs/common';

import { CartsModule } from 'carts/carts.module';
import { ChatsModule } from 'chats/chats.module';
import { CustomOrderItem } from './entities/custom-order-item.entity';
import { CustomOrderItemsService } from './services/custom-order-items.service';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/order-item.entity';
import { OrderItemsService } from './services/order-items.service';
import { OrdersController } from './orders.controller';
import { OrdersService } from './services/orders.service';
import { PriceCalculateHelper } from './helpers/price-calculate.helper';
import { ProductsModule } from 'products/products.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'users/users.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([Order, OrderItem, CustomOrderItem]),
        forwardRef(() => ProductsModule),
        forwardRef(() => UsersModule),
        forwardRef(() => CartsModule),
        ChatsModule,
    ],
    providers: [
        OrdersService,
        OrderItemsService,
        CustomOrderItemsService,
        PriceCalculateHelper,
    ],
    controllers: [OrdersController],
    exports: [
        OrdersService,
        OrderItemsService,
        CustomOrderItemsService,
        PriceCalculateHelper,
    ],
})
export class OrdersModule {}
