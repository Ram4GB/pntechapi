export const parseOrderCodeToOrderId = (code: string) => {
    /**  example: DH-0000001, after split
     [0] DH
     [1] 0000001
    */
    const numberPart = code.split('-')[1];

    /** remove redundance 0 number before */
    const id = parseInt(numberPart);

    return id;
};

export const parseOrderIdToOrderCode = (id: number) => {
    /** Valid order id is, ex: DH-0000001
     * DH- is prefix
     * 0000001 is numbers after prefix
     */

    const configNumAfterPrefix = parseInt(
        process.env.ORDER_ID_NUM_AFTER_PREFIX,
    );
    const idNumLength: number = id.toString().length;

    /** Create order id number after prefix */
    let numbersAfterPrefix = `${id}`;
    for (let i = 0; i < configNumAfterPrefix - idNumLength; i++) {
        numbersAfterPrefix = `0${numbersAfterPrefix}`;
    }

    const code = `${process.env.ORDER_ID_PREFIX}${numbersAfterPrefix}`;

    return code;
};
