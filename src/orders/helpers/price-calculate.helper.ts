import { Order } from 'orders/entities/order.entity';
import { OrderItem } from 'orders/entities/order-item.entity';

export class PriceCalculateHelper {
    calculatePriceOfSingleOrderItem(orderItem: OrderItem): OrderItem {
        const singlePriceAfterDiscount = Math.floor(
            orderItem.singlePrice -
                (orderItem.singlePrice * orderItem.discountPercentage) / 100,
        );

        orderItem.totalPrice = orderItem.quantity * orderItem.singlePrice;
        orderItem.totalPriceAfterDiscount =
            orderItem.quantity * singlePriceAfterDiscount;

        return orderItem;
    }

    calculatePriceOfOrderItemList(orderItems: OrderItem[]): OrderItem[] {
        return orderItems.map(orderItem => {
            const newOrderItem = this.calculatePriceOfSingleOrderItem(
                orderItem,
            );

            return newOrderItem;
        });
    }

    calculateOrderPriceBaseOnChangedOrderItems(
        order: Order,
        oldOrderItems: OrderItem[],
        newOrderItems: OrderItem[],
    ): Order {
        /** Just update order new price by decrease all the old items price then
         * add all new order items price
         */
        oldOrderItems.forEach(oldOrderItem => {
            order.totalPrice -= oldOrderItem.totalPrice;
            order.totalPriceAfterDiscount -=
                oldOrderItem.totalPriceAfterDiscount;
        });

        newOrderItems.forEach(newOrderItem => {
            order.totalPrice += newOrderItem.totalPrice;
            order.totalPriceAfterDiscount +=
                newOrderItem.totalPriceAfterDiscount;
        });

        return order;
    }

    updateOrderPriceAfterChangeOrderItem(
        order: Order,
        oldTotalPrice: number,
        oldTotalPriceAfterDiscount: number,
        newTotalPrice: number,
        newTotalPriceAfterDiscount: number,
    ): Order {
        /** Get rid of the old price */
        order.totalPrice -= oldTotalPrice;
        order.totalPriceAfterDiscount -= oldTotalPriceAfterDiscount;

        /** Update the new price */
        order.totalPrice += newTotalPrice;
        order.totalPriceAfterDiscount += newTotalPriceAfterDiscount;

        return order;
    }
}
