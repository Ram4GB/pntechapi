import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Expose, Transform } from 'class-transformer';

import { ChatRoom } from 'chats/chat-room.entity';
import { CustomOrderItem } from './custom-order-item.entity';
import { OrderItem } from './order-item.entity';
import { OrderStatus } from '../constants/order-status.enum';
import { PriceCurrency } from 'shared/constants/price-currency';
import { User } from 'users/user.entity';
import { parseOrderIdToOrderCode } from 'orders/helpers/parse-order-id.helper';

@Entity()
export class Order {
    constructor(partial: Partial<Order>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn()
    @Expose()
    @Transform(value => parseOrderIdToOrderCode(value))
    id: number;

    @Column({
        type: 'enum',
        enum: OrderStatus,
        default: OrderStatus.Dealing,
    })
    status: OrderStatus;

    @Column({ type: 'text', nullable: true })
    @Expose()
    description: string;

    @Column()
    @Expose()
    totalPrice: number;

    @Column()
    @Expose()
    totalPriceAfterDiscount: number;

    @Column({
        type: 'enum',
        enum: PriceCurrency,
        default: PriceCurrency.VND,
    })
    priceCurrency: PriceCurrency;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    userId: string;

    @ManyToOne(
        type => User,
        user => user.orders,
    )
    user: User;

    @OneToMany(
        type => OrderItem,
        orderItem => orderItem.order,
    )
    orderItems: OrderItem[];

    @OneToMany(
        type => CustomOrderItem,
        customOrderItem => customOrderItem.order,
    )
    customOrderItems: CustomOrderItem[];

    @OneToOne(
        type => ChatRoom,
        chatRoom => chatRoom.order,
    )
    chatRoom: ChatRoom;
}
