import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Expose } from 'class-transformer';
import { Order } from './order.entity';
import { Product } from 'products/products.entity';
@Entity()
export class OrderItem {
    constructor(partial: Partial<OrderItem>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Expose()
    quantity: number;

    @Column()
    @Expose()
    singlePrice: number;

    @Column()
    @Expose()
    totalPrice: number;

    @Column({ nullable: true })
    @Expose()
    discountPercentage: number;

    @Column()
    @Expose()
    totalPriceAfterDiscount: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(
        type => Order,
        order => order.orderItems,
    )
    order: Order;

    @Expose()
    @ManyToOne(
        type => Product,
        product => product.orderItems,
    )
    product: Product;
}
