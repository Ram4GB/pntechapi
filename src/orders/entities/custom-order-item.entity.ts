import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { Expose } from 'class-transformer';
import { Order } from './order.entity';

@Entity()
export class CustomOrderItem {
    constructor(partial: Partial<CustomOrderItem>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Expose()
    name: string;

    @Column()
    @Expose()
    quantity: number;

    @Column()
    @Expose()
    singlePrice: number;

    @Column()
    @Expose()
    totalPrice: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(
        type => Order,
        order => order.customOrderItems,
    )
    order: Order;
}
