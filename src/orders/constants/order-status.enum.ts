export enum OrderStatus {
    Canceled = 'canceled',
    Dealing = 'dealing',
    Done = 'done',
    Exported = 'exported',
}
