export enum DiscountUpdateChoice {
    /** if choose this type, only update discount percentage of order item in its order */
    OnlyInOrder = 'only-in-order',
    /** If choose this type, update discount for the whole category of this product */
    ForWholeCategory = 'for-whole-category',
}
