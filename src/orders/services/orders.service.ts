import * as _ from 'lodash';
import * as momentTimzone from 'moment-timezone';

import {
    BadRequestException,
    Inject,
    Injectable,
    NotFoundException,
    forwardRef,
} from '@nestjs/common';
import { Connection, Repository, SelectQueryBuilder } from 'typeorm';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { PagedList, PaginateQuery, paginate } from 'shared/helpers/pagination';

import { AdminCreateOrderDTO } from 'orders/dto/admin-create-order.dto';
import { Cart } from 'carts/cart.entity';
import { ClaimTypes } from 'shared/constants/claim-types';
import { CommonConfig } from 'shared/constants/common';
import { CustomCartProduct } from 'carts/custom-cart-product.entity';
import { CustomOrderItem } from 'orders/entities/custom-order-item.entity';
import { CustomOrderItemWhenAdminCreateOrderDTO } from 'orders/dto/custom-order-item-when-admin-create-order.dto';
import { CustomOrderItemsService } from './custom-order-items.service';
import { MailSubject } from 'shared/constants/mail-subject';
import { MailerService } from '@nestjs-modules/mailer';
import { Media } from 'media/media.entity';
import { MediaUsage } from 'media/media-usage.entity';
import { Order } from 'orders/entities/order.entity';
import { OrderItem } from 'orders/entities/order-item.entity';
import { OrderItemWhenAdminCreateDTO } from 'orders/dto/order-item-when-admin-create-order.dto';
import { OrderItemsService } from './order-items.service';
import { OrderStatus } from 'orders/constants/order-status.enum';
import { PriceCalculateHelper } from 'orders/helpers/price-calculate.helper';
import { PriceCurrency } from 'shared/constants/price-currency';
import { Product } from 'products/products.entity';
import { ProductsService } from 'products/products.service';
import { Roles } from 'shared/constants/roles';
import { User } from 'users/user.entity';
import { UsersService } from 'users/users.service';
import { parseOrderIdToOrderCode } from 'orders/helpers/parse-order-id.helper';

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(Order)
        private readonly ordersRepository: Repository<Order>,
        @Inject(forwardRef(() => OrderItemsService))
        private readonly orderItemsService: OrderItemsService,
        @Inject(forwardRef(() => CustomOrderItemsService))
        private readonly customOrderItemsService: CustomOrderItemsService,
        private readonly mailerService: MailerService,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => ProductsService))
        private readonly productsService: ProductsService,
        @InjectConnection()
        private connection: Connection,
        private readonly priceCalculateHelper: PriceCalculateHelper,
    ) {}

    /* -------------------------------------
          CREATE USER NEW ORDER FROM CART
      ------------------------------------- */
    async createOrderFromCart(
        order: Order,
        carts: Cart[],
        customCartProducts: CustomCartProduct[],
    ): Promise<Order> {
        /** Map produts in cart to orderItem and customOrderItem */
        const orderItems = this.orderItemsService.mapCartsToOrderItems(carts);
        const customOrderItems = this.customOrderItemsService.mapCustomCartProductsToCustomOrderItems(
            customCartProducts,
        );

        order = this.prepareNewOrderInfo(order, orderItems);
        const orderTotalQuantity = this.calculateTotalQuantityOfOrder(
            orderItems,
            customOrderItems,
        );

        /** Save to db */
        let createdOrder: Order;
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const createdOrderItems = await queryRunner.manager.save(
                orderItems,
            );
            const createdCustomOrderItems = await queryRunner.manager.save(
                customOrderItems,
            );

            order.orderItems = createdOrderItems;
            order.customOrderItems = createdCustomOrderItems;
            createdOrder = await queryRunner.manager.save(order);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            /** Send mail to annouce user */
            this.mailerService
                .sendMail({
                    to: createdOrder.user.email,
                    subject: MailSubject.CONFIRM_USER_ORDER,
                    template: 'confirm-user-order',
                    context: {
                        quantity: orderTotalQuantity,
                        user: createdOrder.user,
                        orderItems: createdOrder.orderItems,
                        customOrderItems: createdOrder.customOrderItems,
                        currentDate: momentTimzone()
                            .tz(CommonConfig.TimeZoneMoment)
                            .format('DD/MM/yyyy'),
                        orderId: parseOrderIdToOrderCode(order.id),
                    },
                })
                .catch(err => {
                    console.log(err);
                });
        }

        return createdOrder;
    }

    async create(order: Order): Promise<Order> {
        return await this.ordersRepository.save(order);
    }

    prepareNewOrderInfo(order: Order, orderItems: OrderItem[]): Order {
        /** Calculate order total price before discount and after discount */
        const [
            totalPrice,
            totalPriceAfterDiscount,
        ] = this.calculateOrderTotalPriceBeforeAndAfterDiscountBaseOnOrderItems(
            orderItems,
        );

        order.status = OrderStatus.Dealing;
        order.totalPrice = totalPrice;
        order.totalPriceAfterDiscount = totalPriceAfterDiscount;
        order.priceCurrency = PriceCurrency.VND;

        return order;
    }

    calculateOrderTotalPriceBeforeAndAfterDiscountBaseOnOrderItems(
        orderItems: OrderItem[],
    ): [number, number] {
        let totalPrice = 0;
        let totalPriceAfterDiscount = 0;

        orderItems.forEach(orderItem => {
            totalPrice += orderItem.totalPrice;
            totalPriceAfterDiscount += orderItem.totalPriceAfterDiscount;
        });

        return [totalPrice, totalPriceAfterDiscount];
    }

    validateNotEmptyCartWhenCreateOrder(
        carts: Cart[],
        customCartProducts: CustomCartProduct[],
    ) {
        if (customCartProducts.length === 0 && carts.length === 0) {
            throw new BadRequestException(
                'Không có sản phẩm nào trong giỏ hàng của bạn.',
            );
        }
    }

    async listOrdersPaginated(
        query: PaginateQuery,
        user: User,
    ): Promise<PagedList<Order>> {
        return await paginate<Order>(
            this.ordersRepository,
            query,
            (builder: SelectQueryBuilder<Order>) => {
                builder
                    .select('Order.id')
                    .addSelect([
                        'Order.status',
                        'Order.description',
                        'Order.createdAt',
                        'Order.updatedAt',
                    ])

                    .leftJoinAndSelect('Order.user', 'User')
                    .leftJoinAndSelect('Order.chatRoom', 'ChatRoom');
                builder = this.customOrderListBuilderByUserRole(builder, user);
            },
        );
    }

    customOrderListBuilderByUserRole(
        builder: SelectQueryBuilder<Order>,
        user: User,
    ) {
        /** this way apply only for one user has one role  */
        switch (user.roles[0].name) {
            case Roles.Customer: {
                // user can only get their own order
                builder.andWhere('Order.userId = :userId', {
                    userId: user.id,
                });
                break;
            }
            case Roles.Admin: {
                // user can only get their own order
                builder.addSelect('Order.totalPrice');
                builder.addSelect('Order.totalPriceAfterDiscount');
                break;
            }
            case Roles.Seller: {
                // Seller can only his/her user orders
                builder.addSelect('Order.totalPrice');
                builder.addSelect('Order.totalPriceAfterDiscount');
                builder.leftJoinAndSelect('User.userClaims', 'UserClaim');
                builder.andWhere(
                    'UserClaim.claimType = :claimTypeValue AND UserClaim.claimValue = :claimValue',
                    {
                        claimTypeValue: ClaimTypes.SellerGroupId,
                        claimValue: user.id,
                    },
                );

                break;
            }
            default:
                break;
        }

        return builder;
    }

    customGetOrderByIdBuilderByUserRole(
        builder: SelectQueryBuilder<Order>,
        user: User,
    ) {
        /** this way apply only for one user has one role  */
        switch (user.roles[0].name) {
            case Roles.Customer: {
                // user can only get their own order
                builder.andWhere('Order.userId = :userId', {
                    userId: user.id,
                });
                break;
            }
            case Roles.Admin: {
                builder.addSelect([
                    'Order.totalPrice',
                    'Order.totalPriceAfterDiscount',
                ]);
                builder.addSelect([
                    'OrderItem.quantity',
                    'OrderItem.singlePrice',
                    'OrderItem.discountPercentage',
                    'OrderItem.totalPrice',
                    'OrderItem.totalPriceAfterDiscount',
                ]);
                builder.addSelect([
                    'CustomOrderItem.quantity',
                    'CustomOrderItem.singlePrice',
                    'CustomOrderItem.totalPrice',
                ]);
                builder.addSelect([
                    'Product.quantity',
                    'Product.price',
                    'Product.note',
                    'Product.status',
                ]);

                break;
            }
            case Roles.Seller: {
                // Seller can only his/her user orders
                builder.addSelect([
                    'Order.totalPrice',
                    'Order.totalPriceAfterDiscount',
                ]);
                builder.addSelect([
                    'OrderItem.quantity',
                    'OrderItem.singlePrice',
                    'OrderItem.discountPercentage',
                    'OrderItem.totalPrice',
                    'OrderItem.totalPriceAfterDiscount',
                ]);
                builder.addSelect([
                    'CustomOrderItem.quantity',
                    'CustomOrderItem.singlePrice',
                    'CustomOrderItem.totalPrice',
                ]);
                builder.addSelect([
                    'Product.quantity',
                    'Product.price',
                    'Product.note',
                    'Product.status',
                ]);
                builder.leftJoinAndSelect('User.userClaims', 'UserClaim');
                builder.andWhere(
                    'UserClaim.claimType = :claimTypeValue AND UserClaim.claimValue = :claimValue',
                    {
                        claimTypeValue: ClaimTypes.SellerGroupId,
                        claimValue: user.id,
                    },
                );

                break;
            }
            default:
                break;
        }

        return builder;
    }

    async findById(id: string, user: User): Promise<Order> {
        /** Select field manually because each role will have another
         * permission to see response fields */
        let builder = this.ordersRepository
            /** Order */
            .createQueryBuilder('Order')
            .select([
                'Order.id',
                'Order.status',
                'Order.description',
                'Order.createdAt',
                'Order.updatedAt',
            ])
            /** Order Item */
            .leftJoin('Order.orderItems', 'OrderItem')
            .addSelect([
                'OrderItem.id',
                'OrderItem.quantity',
                'OrderItem.product',
                'OrderItem.createdAt',
                'OrderItem.updatedAt',
            ])
            /** Custom Order Item */
            .leftJoin('Order.customOrderItems', 'CustomOrderItem')
            .addSelect([
                'CustomOrderItem.id',
                'CustomOrderItem.name',
                'CustomOrderItem.quantity',
                'CustomOrderItem.createdAt',
                'CustomOrderItem.updatedAt',
            ])
            /** Product */
            .leftJoin('OrderItem.product', 'Product')
            .addSelect([
                'Product.id',
                'Product.name',
                'Product.generalDescription',
                'Product.technicalDescription',
                'Product.category',
            ])
            /** Media */
            .leftJoinAndSelect(
                MediaUsage,
                'MediaUsage',
                "Product.id = MediaUsage.usedById AND MediaUsage.type = 'product'",
            )
            .leftJoinAndMapOne(
                'Product.media',
                Media,
                'Media',
                'MediaUsage.mediaId = Media.id',
            )
            .andWhere('MediaUsage.weight = 1 OR MediaUsage.weight IS NULL')
            /** User */
            .leftJoinAndSelect('Order.user', 'User')
            .where('Order.id = :id', { id });
        builder = this.customGetOrderByIdBuilderByUserRole(builder, user);

        return await builder.getOne();
    }

    async findOnlyOrderInfoById(id: number): Promise<Order> {
        return await this.ordersRepository.findOne({ where: { id } });
    }

    async findOrderByIdAndIncludeOrderItemByProductIdAndOrderId(
        orderId: number,
        productId: string,
    ) {
        return (
            this.ordersRepository
                .createQueryBuilder('Order')
                .leftJoinAndSelect('Order.user', 'User')
                .leftJoinAndSelect(
                    'Order.orderItems',
                    'OrderItem',
                    'OrderItem.productId = :productId',
                    { productId },
                )
                .where('Order.id = :orderId', { orderId })
                // only can edit orderItem when order is dealing
                .andWhere('Order.status = :dealingStatus', {
                    dealingStatus: OrderStatus.Dealing,
                })
                .getOne()
        );
    }
    async findOrderByIdToAddCustomorderItem(id: number) {
        return (
            this.ordersRepository
                .createQueryBuilder('Order')
                .where('Order.id = :id', { id })
                // only can edit custom orderItem when order is dealing
                .andWhere('Order.status = :dealingStatus', {
                    dealingStatus: OrderStatus.Dealing,
                })
                .getOne()
        );
    }

    /* -------------------------------------
          SEND QUOTATION TO CUSTOMER
      ------------------------------------- */
    async sendQuotationToCustomerEmail(id: number) {
        const order = await this.findOrderByIdToSendQuotation(id);
        if (!order) {
            throw new NotFoundException();
        }

        const [
            totalPriceAfterDiscount,
        ] = this.calculateTotalPriceBeforeAndAfterDiscountOfOrder(
            order.orderItems,
            order.customOrderItems,
        );
        const quantity = this.calculateTotalQuantityOfOrder(
            order.orderItems,
            order.customOrderItems,
        );

        this.mailerService
            .sendMail({
                to: order.user.email,
                subject: MailSubject.QUOTATION,
                template: 'send-quotation-to-customer',
                context: {
                    totalPriceAfterDiscount: new Intl.NumberFormat().format(
                        totalPriceAfterDiscount,
                    ),
                    quantity,
                    user: order.user,
                    orderItems: order.orderItems,
                    customOrderItems: order.customOrderItems,
                    currentDate: momentTimzone()
                        .tz(CommonConfig.TimeZoneMoment)
                        .format('DD/MM/yyyy'),
                    orderId: parseOrderIdToOrderCode(order.id),
                },
            })
            .catch(err => {
                console.log(err);
            });
    }

    async findOrderByIdToSendQuotation(id: number) {
        return this.ordersRepository
            .createQueryBuilder('Order')
            .leftJoinAndSelect('Order.user', 'User')
            .leftJoinAndSelect('Order.orderItems', 'OrderItem')
            .leftJoinAndSelect('OrderItem.product', 'product')
            .leftJoinAndSelect('Order.customOrderItems', 'CustomOrderItem')
            .where('Order.id = :id', { id })
            .andWhere('Order.status = :dealingStatus', {
                dealingStatus: OrderStatus.Dealing,
            })
            .getOne();
    }

    calculateTotalPriceBeforeAndAfterDiscountOfOrder(
        orderItems: OrderItem[],
        customOrderItems: CustomOrderItem[],
    ): [number, number] {
        let totalPrice = 0;
        let totalPriceAfterDiscount = 0;

        /** Order Items */
        if (!!orderItems && orderItems.length > 0) {
            orderItems.forEach(orderItem => {
                totalPrice += orderItem.totalPrice;
                totalPriceAfterDiscount += orderItem.totalPriceAfterDiscount;
            });
        }

        /** Custom Order Items */
        if (!!customOrderItems && customOrderItems.length > 0) {
            customOrderItems.forEach(customOrderItem => {
                totalPrice += customOrderItem.totalPrice;
                totalPriceAfterDiscount += customOrderItem.totalPrice;
            });
        }

        return [totalPriceAfterDiscount, totalPrice];
    }

    calculateTotalQuantityOfOrder(
        orderItems: OrderItem[],
        customOrderItems: CustomOrderItem[],
    ): number {
        let quantity = 0;

        /** Order Items */
        orderItems.forEach(orderItem => {
            quantity += orderItem.quantity;
        });

        /** Custom Order Items */
        customOrderItems.forEach(customOrderItem => {
            quantity += customOrderItem.quantity;
        });

        return quantity;
    }
    /** <<<<< END SEND QUOTATION TO CUSTOMER >>>>> */

    /** >>>>> CREATE ORDER BY ADMIN <<<<< */
    async createOrderByAdmin(createInfo: AdminCreateOrderDTO): Promise<void> {
        const customerUser = await this.getValidUserWhenAdminCreateOrder(
            createInfo.customerId,
        );

        const order = new Order({
            status: OrderStatus.Dealing,
            user: customerUser,
        });

        const orderItems: OrderItem[] = await this.prepareOrderItemsInfoWhenAdminCreateOrder(
            createInfo.orderItems,
            createInfo.customerId,
        );

        const customOrderItems: CustomOrderItem[] = this.prepareCustomOrderItemsInfoWhenAdminCreateOrder(
            createInfo.customOrderItems,
        );

        this.updateOrderPriceByOrderItemsAndCustomOrderItems(
            order,
            orderItems,
            customOrderItems,
        );

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            if (!!orderItems && orderItems.length > 0) {
                const createdOrderItems = await queryRunner.manager.save(
                    OrderItem,
                    orderItems,
                );
                /** If order items are created then add to order to create FK */
                order.orderItems = createdOrderItems;
            }
            if (!!customOrderItems && customOrderItems.length > 0) {
                const createdCustomOrderItems = await queryRunner.manager.save(
                    CustomOrderItem,
                    customOrderItems,
                );

                /** If order items are created then add to order to create FK */
                order.customOrderItems = createdCustomOrderItems;
            }

            await queryRunner.manager.save(Order, order);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            /** if order is created successfully then send mail to user to announce him/her */
            this.mailerService
                .sendMail({
                    to: customerUser.email,
                    subject: MailSubject.USER_ORDER_IS_CREATED,
                    template: 'annouce-customer-when-admin-create-order',
                    context: {
                        name: customerUser.firstName,
                        orderId: parseOrderIdToOrderCode(order.id),
                    },
                })
                .catch(err => {
                    console.log(err);
                });
            await queryRunner.release();
        }
    }

    async getValidUserWhenAdminCreateOrder(customerId: string): Promise<User> {
        const customerUser = await this.usersService.findById(customerId);
        if (!customerUser) {
            throw new NotFoundException();
        }
        if (customerUser.roles[0].name !== Roles.Customer) {
            throw new BadRequestException('User must be customer');
        }
        delete customerUser.roles;

        return customerUser;
    }

    async prepareOrderItemsInfoWhenAdminCreateOrder(
        orderItemsInfo: OrderItemWhenAdminCreateDTO[],
        customerId: string,
    ): Promise<OrderItem[]> {
        /** Unique on productId only be in one order item*/
        orderItemsInfo = this.handleDuplicateProductIdInListOrderItemDto(
            orderItemsInfo,
        );

        let orderItems: OrderItem[] = [];
        if (!!orderItemsInfo && orderItemsInfo.length > 0) {
            const productsOfOrderItems = await this.getAllProducsOfOrderItemDto(
                orderItemsInfo,
                customerId,
            );

            orderItems = orderItemsInfo.map(o => {
                const productOfOrderItem = productsOfOrderItems.find(
                    val => val.id === o.productId,
                );
                let orderItemEntity = new OrderItem({
                    quantity: o.quantity,
                    product: productOfOrderItem,
                    singlePrice: productOfOrderItem.price,
                    discountPercentage: productOfOrderItem.category
                        .categoryDiscounts[0]
                        ? productOfOrderItem.category.categoryDiscounts[0]
                              .discountPercentage
                        : 0,
                });

                orderItemEntity = this.priceCalculateHelper.calculatePriceOfSingleOrderItem(
                    orderItemEntity,
                );

                return orderItemEntity;
            });
        }

        return orderItems;
    }

    async getAllProducsOfOrderItemDto(
        orderItems: OrderItemWhenAdminCreateDTO[],
        customerId: string,
    ): Promise<Product[]> {
        const allOrderItemProductIds = orderItems.map(
            orderItem => orderItem.productId,
        );
        const productsOfOrderItems = await this.productsService.findManyOrderItemProductsByIds(
            allOrderItemProductIds,
            customerId,
        );

        return productsOfOrderItems;
    }

    prepareCustomOrderItemsInfoWhenAdminCreateOrder(
        customOrderItemsInfo: CustomOrderItemWhenAdminCreateOrderDTO[],
    ) {
        let customOrderItems: CustomOrderItem[];

        if (!!customOrderItemsInfo && customOrderItemsInfo.length > 0) {
            customOrderItems = customOrderItemsInfo.map(co => {
                const customOrderItemEntity = new CustomOrderItem({
                    ...co,
                });

                this.customOrderItemsService.updateCustomOrderItemPriceAfterChange(
                    customOrderItemEntity,
                );

                return customOrderItemEntity;
            });
        }

        return customOrderItems;
    }

    updateOrderPriceByOrderItemsAndCustomOrderItems(
        order: Order,
        orderItems: OrderItem[],
        customOrderItems: CustomOrderItem[],
    ): void {
        const [
            totalPriceAfterDiscount,
            totalPrice,
        ] = this.calculateTotalPriceBeforeAndAfterDiscountOfOrder(
            orderItems,
            customOrderItems,
        );

        order.totalPrice = totalPrice;
        order.totalPriceAfterDiscount = totalPriceAfterDiscount;
    }

    handleDuplicateProductIdInListOrderItemDto(
        orderItems: OrderItemWhenAdminCreateDTO[],
    ): OrderItemWhenAdminCreateDTO[] {
        const result = _.uniqBy(orderItems, 'productId');
        return result;
    }
    /** <<<<< END CREATE ORDER BY ADMIN >>>>> */

    async changeOrderStatusToDone(id: number): Promise<void> {
        const order = await this.ordersRepository.findOne(id);
        if (!order) {
            throw new NotFoundException();
        }
        /** Only can done order when it's dealing */
        if (order.status !== OrderStatus.Dealing) {
            throw new BadRequestException('Can not change this order status');
        }

        await this.ordersRepository.update(id, { status: OrderStatus.Done });
    }

    async changeOrderStatusToCancel(id: number): Promise<void> {
        const order = await this.ordersRepository.findOne(id);
        if (!order) {
            throw new NotFoundException();
        }
        /** Only can cancle order when it's dealing */
        if (order.status !== OrderStatus.Dealing) {
            throw new BadRequestException('Can not change this order status');
        }

        await this.ordersRepository.update(id, {
            status: OrderStatus.Canceled,
        });
    }

    async getAllDealingOrderOfUserToWhenCategoryDiscountChange(
        userId: string,
        categoryId: string,
    ): Promise<Order[]> {
        return this.ordersRepository
            .createQueryBuilder('Order')
            .leftJoinAndSelect('Order.orderItems', 'OrderItem')
            .leftJoinAndSelect('OrderItem.product', 'Product')
            .leftJoinAndSelect('Product.category', 'Category')
            .where('Order.userId = :userId', { userId })
            .andWhere('Order.status = :dealingStatus', {
                dealingStatus: OrderStatus.Dealing,
            })
            .andWhere('Category.id = :categoryId', {
                categoryId,
            })
            .getMany();
    }
}
