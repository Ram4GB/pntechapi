import { Connection, Repository } from 'typeorm';
import {
    Inject,
    Injectable,
    NotFoundException,
    forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';

import { AddCustomOrderItemToOrderDTO } from 'orders/dto/add-customOrderItem-to-order.dto';
import { CustomCartProduct } from 'carts/custom-cart-product.entity';
import { CustomOrderItem } from 'orders/entities/custom-order-item.entity';
import { CustomOrderItemUpdateDTO } from 'orders/dto/custom-order-item-update.dto';
import { Order } from 'orders/entities/order.entity';
import { OrderStatus } from 'orders/constants/order-status.enum';
import { OrdersService } from './orders.service';
import { plainToClass } from 'class-transformer';

@Injectable()
export class CustomOrderItemsService {
    constructor(
        @InjectRepository(CustomOrderItem)
        private customOrderItemsRepository: Repository<CustomOrderItem>,
        @InjectConnection()
        private connection: Connection,
        @Inject(forwardRef(() => OrdersService))
        private readonly ordersService: OrdersService,
    ) {}

    async bulkCreate(
        customOrderItems: CustomOrderItem[],
    ): Promise<CustomOrderItem[]> {
        return await this.customOrderItemsRepository.save(customOrderItems);
    }

    mapCustomCartProductsToCustomOrderItems(
        customCartProducts: CustomCartProduct[],
    ): CustomOrderItem[] {
        let customOrderItems = plainToClass(
            CustomOrderItem,
            customCartProducts,
            {
                excludeExtraneousValues: true,
            },
        );

        customOrderItems = customOrderItems.map(customOrderItem => {
            /** Set default price of custom order item to 0 */
            customOrderItem.singlePrice = 0;
            customOrderItem.totalPrice = 0;

            return customOrderItem;
        });

        return customOrderItems;
    }

    /** >>>>> UPDATE CUSTOM ORDER ITEM <<<<< */
    async update(
        id: string,
        updateInfo: CustomOrderItemUpdateDTO,
    ): Promise<void> {
        let customOrderItem = await this.findCustomOrderItemByIdToupdate(id);
        if (!customOrderItem) {
            throw new NotFoundException();
        }
        const order = customOrderItem.order;

        /** If choose to update singlePrice or quantity =>
         * customOrderItem total price will be changed
         * order totalPrice will be change too
         */
        if (updateInfo.singlePrice || updateInfo.quantity) {
            /** Get old price to use to update order total price
             * order.totalPrice = order.totalPrice
             *      - old order item total price
             *      + new order item total price
             */
            const { totalPrice: oldTotalPrice } = customOrderItem;

            customOrderItem = {
                ...customOrderItem,
                ...updateInfo,
            };

            /** Prepare new order item info */
            this.updateCustomOrderItemPriceAfterChange(customOrderItem);

            /** Update order total price */
            this.updateOrderPriceAfterUpdateCustomOrderItem(
                order,
                customOrderItem,
                oldTotalPrice,
            );
        } else {
            /** in this case only name will be update
             * so we don't need to update price of both order and custom order item
             */
            customOrderItem = {
                ...customOrderItem,
                ...updateInfo,
            };
        }

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            await queryRunner.manager.save(CustomOrderItem, customOrderItem);
            /** quantity or singlePrice change => order chage => update it
             * if not don't touch it
             */
            if (updateInfo.singlePrice || updateInfo.quantity) {
                await queryRunner.manager.save(Order, order);
            }

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async findCustomOrderItemByIdToupdate(
        id: string,
    ): Promise<CustomOrderItem> {
        return this.customOrderItemsRepository
            .createQueryBuilder('CustomOrderItem')
            .leftJoinAndSelect('CustomOrderItem.order', 'Order')
            .where('CustomOrderItem.id = :id', { id })
            .andWhere('Order.status = :dealingStatus', {
                dealingStatus: OrderStatus.Dealing,
            })
            .getOne();
    }

    updateCustomOrderItemPriceAfterChange(
        customOrderItem: CustomOrderItem,
    ): void {
        customOrderItem.totalPrice =
            customOrderItem.quantity * customOrderItem.singlePrice;
    }

    updateOrderPriceAfterUpdateCustomOrderItem(
        order: Order,
        customOrderItem: CustomOrderItem,
        oldTotalPrice: number,
    ): void {
        order.totalPrice -= oldTotalPrice;
        order.totalPrice += customOrderItem.totalPrice;

        /** Custom order item doesn't have discount percentage
         * then just add it to totalPriceAfterDiscount of order
         */
        order.totalPriceAfterDiscount -= oldTotalPrice;
        order.totalPriceAfterDiscount += customOrderItem.totalPrice;
    }
    /** <<<<< END UPDATE CUSTOM ORDER ITEM >>>>> */

    async addCustomOrderItemToOrder(
        addInfo: AddCustomOrderItemToOrderDTO,
        orderId: number,
    ) {
        const order = await this.ordersService.findOrderByIdToAddCustomorderItem(
            orderId,
        );
        if (!order) {
            throw new NotFoundException();
        }

        /** Prepare new custom order item info */
        const newCustomOrderItem = new CustomOrderItem({
            quantity: addInfo.quantity,
            singlePrice: addInfo.singlePrice,
            name: addInfo.name,
            order,
        });
        this.updateCustomOrderItemPriceAfterChange(newCustomOrderItem);

        /** Because add new custom order item => order price will be changed */
        /** Because this is a new custom order item so when we call method
         * to update order price the oldTotalPrice argument will be 0
         */
        this.updateOrderPriceAfterUpdateCustomOrderItem(
            order,
            newCustomOrderItem,
            0,
        );

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            await queryRunner.manager.save(CustomOrderItem, newCustomOrderItem);
            await queryRunner.manager.save(Order, order);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async deleteCustomOrderItem(id: string) {
        const customOrderItem = await this.findCustomOrderItemByIdToDelete(id);
        if (!customOrderItem) {
            throw new NotFoundException();
        }

        /** Custom order Item is removed so order price has to be changed to */
        const order = customOrderItem.order;
        order.totalPrice -= customOrderItem.totalPrice;
        order.totalPriceAfterDiscount -= customOrderItem.totalPrice;

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            delete customOrderItem.order;

            await queryRunner.manager.remove(CustomOrderItem, customOrderItem);
            await queryRunner.manager.save(Order, order);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async findCustomOrderItemByIdToDelete(
        id: string,
    ): Promise<CustomOrderItem> {
        return this.customOrderItemsRepository
            .createQueryBuilder('CustomOrderItem')
            .leftJoinAndSelect('CustomOrderItem.order', 'Order')
            .where('CustomOrderItem.id = :id', { id })
            .andWhere('Order.status = :dealingStatus', {
                dealingStatus: OrderStatus.Dealing,
            })
            .getOne();
    }
}
