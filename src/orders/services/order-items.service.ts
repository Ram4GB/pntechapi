import { Connection, Repository } from 'typeorm';
import {
    Inject,
    Injectable,
    NotFoundException,
    forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';

import { AddOrderItemToOrderDTO } from 'orders/dto/add-orderItem-to-order.dto';
import { Cart } from 'carts/cart.entity';
import { Order } from 'orders/entities/order.entity';
import { OrderItem } from 'orders/entities/order-item.entity';
import { OrderItemUpdateDTO } from 'orders/dto/order-item-update.dto';
import { OrderStatus } from 'orders/constants/order-status.enum';
import { OrdersService } from './orders.service';
import { PriceCalculateHelper } from 'orders/helpers/price-calculate.helper';
import { Product } from 'products/products.entity';
import { ProductsService } from 'products/products.service';
import { plainToClass } from 'class-transformer';

@Injectable()
export class OrderItemsService {
    constructor(
        @InjectRepository(OrderItem)
        private orderItemsRepository: Repository<OrderItem>,
        @InjectConnection()
        private connection: Connection,
        @Inject(forwardRef(() => OrdersService))
        private readonly ordersService: OrdersService,
        @Inject(forwardRef(() => ProductsService))
        private readonly productsService: ProductsService,
        private readonly priceCalculateHelper: PriceCalculateHelper,
    ) {}

    async bulkCreate(orderItems: OrderItem[]): Promise<OrderItem[]> {
        return await this.orderItemsRepository.save(orderItems);
    }

    mapCartsToOrderItems(carts: Cart[]): OrderItem[] {
        /** Parse from cart entity to order item entity */
        let orderItems = plainToClass(OrderItem, carts, {
            excludeExtraneousValues: true,
        });

        /** Add the rest field to each order item to save to db */
        orderItems = orderItems.map(orderItem => {
            orderItem.singlePrice = orderItem.product.price;
            orderItem.discountPercentage = orderItem.product.category
                .categoryDiscounts[0]
                ? orderItem.product.category.categoryDiscounts[0]
                      .discountPercentage
                : 0;

            orderItem = this.priceCalculateHelper.calculatePriceOfSingleOrderItem(
                orderItem,
            );

            delete orderItem.product.category;

            return orderItem;
        });

        return orderItems;
    }

    /** >>>>> UPDATE ORDER ITEM <<<<< */
    async update(id: string, updateInfo: OrderItemUpdateDTO): Promise<void> {
        let orderItem = await this.findOrderItemByIdToupdate(id);
        if (!orderItem) {
            throw new NotFoundException();
        }
        let order = orderItem.order;

        /** Get old price to use to update order total price
         * order.totalPrice = order.totalPrice
         *      - old order item total price
         *      + new order item total price
         */
        const {
            totalPrice: oldTotalPrice,
            totalPriceAfterDiscount: oldTotalPriceAfterDiscount,
        } = orderItem;

        orderItem = {
            ...orderItem,
            ...updateInfo,
        };

        /** Prepare new order item info */
        orderItem = this.priceCalculateHelper.calculatePriceOfSingleOrderItem(
            orderItem,
        );

        /** When order item is updated then the order total price
         * and total price after discount will be changed too
         */
        order = this.priceCalculateHelper.updateOrderPriceAfterChangeOrderItem(
            order,
            oldTotalPrice,
            oldTotalPriceAfterDiscount,
            orderItem.totalPrice,
            orderItem.totalPriceAfterDiscount,
        );

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            await queryRunner.manager.save(OrderItem, orderItem);
            await queryRunner.manager.save(Order, order);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async findOrderItemByIdToupdate(id: string): Promise<OrderItem> {
        return this.orderItemsRepository
            .createQueryBuilder('OrderItem')
            .leftJoinAndSelect('OrderItem.order', 'Order')
            .where('OrderItem.id = :id', { id })
            .andWhere('Order.status = :dealingStatus', {
                dealingStatus: OrderStatus.Dealing,
            })
            .getOne();
    }

    updateOrderItemPriceAfterChange(orderItem: OrderItem): void {
        orderItem.totalPrice = orderItem.quantity * orderItem.singlePrice;
        orderItem.totalPriceAfterDiscount = Math.floor(
            orderItem.discountPercentage
                ? orderItem.totalPrice -
                      (orderItem.totalPrice * orderItem.discountPercentage) /
                          100
                : orderItem.totalPrice,
        );
    }

    updateOrderPriceAfterUpdateOrderItem(
        order: Order,
        orderItem: OrderItem,
        oldTotalPrice: number,
        oldTotalPriceAfterDiscount: number,
    ): void {
        order.totalPrice -= oldTotalPrice;
        order.totalPrice += orderItem.totalPrice;

        order.totalPriceAfterDiscount -= oldTotalPriceAfterDiscount;
        order.totalPriceAfterDiscount += orderItem.totalPriceAfterDiscount;
    }
    /** <<<<< END UPDATE ORDER ITEM >>>>> */

    /** >>>>> ADD ORDER ITEM TO ORDER <<<<< */
    async addOrderItemToOrder(
        addInfo: AddOrderItemToOrderDTO,
        orderId: number,
    ): Promise<void> {
        let order = await this.getValidOrderWhenAddOrderItemToOrder(
            orderId,
            addInfo.productId,
        );
        const product = await this.getValidProductWhenAddOrderItemToOrder(
            addInfo.productId,
            order.user.id,
        );

        /** Because include by orderId and productId then orderItems only has one item */
        let orderItem = order.orderItems[0];

        /** If order item is existed then save the old price to update order price */
        const oldTotalPriceAfterDiscount = orderItem
            ? orderItem.totalPriceAfterDiscount
            : 0;
        const oldTotalPrice = orderItem ? orderItem.totalPrice : 0;

        orderItem = this.prepareOrderItemBeforeSave(
            orderItem,
            order,
            product,
            addInfo.quantity,
        );

        order = this.priceCalculateHelper.updateOrderPriceAfterChangeOrderItem(
            order,
            oldTotalPrice,
            oldTotalPriceAfterDiscount,
            orderItem.totalPrice,
            orderItem.totalPriceAfterDiscount,
        );

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            delete order.orderItems;

            await queryRunner.manager.save(OrderItem, orderItem);
            await queryRunner.manager.save(Order, order);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async getValidOrderWhenAddOrderItemToOrder(
        orderId: number,
        productId: string,
    ): Promise<Order> {
        const order = await this.ordersService.findOrderByIdAndIncludeOrderItemByProductIdAndOrderId(
            orderId,
            productId,
        );
        if (!order) {
            throw new NotFoundException();
        }

        return order;
    }

    async getValidProductWhenAddOrderItemToOrder(
        productId: string,
        userId: string,
    ): Promise<Product> {
        const product = await this.productsService.findProductByIdToUpdateOrderItem(
            productId,
            userId,
        );
        if (!product) {
            throw new NotFoundException();
        }

        return product;
    }

    prepareOrderItemBeforeSave(
        orderItem: OrderItem,
        order: Order,
        product: Product,
        quantity: number,
    ): OrderItem {
        if (orderItem) {
            orderItem = this.prepareOrderItemInfoWhenItIsAlreadyExisted(
                orderItem,
                quantity,
            );
        } else {
            orderItem = this.prepareOrderItemInfoWhenItIsANewItem(
                orderItem,
                order,
                product,
                quantity,
            );
        }

        return orderItem;
    }

    prepareOrderItemInfoWhenItIsAlreadyExisted(
        orderItem: OrderItem,
        quantity: number,
    ): OrderItem {
        /** If this order item is already existed then just increase its quantity
         * and calculate price again
         */
        orderItem.quantity += quantity;
        orderItem = this.priceCalculateHelper.calculatePriceOfSingleOrderItem(
            orderItem,
        );

        return orderItem;
    }

    prepareOrderItemInfoWhenItIsANewItem(
        orderItem: OrderItem,
        order: Order,
        product: Product,
        quantity: number,
    ): OrderItem {
        /** Re-assign new order item entity to orderItem variable to prevent
         * duplicate check exist order item when we save to db (in transaction part)
         */
        const newOrderItem = new OrderItem({
            product,
            order,
            quantity: quantity,
            singlePrice: product.price,
            /** Because get category discount by userId and categoryId then
             * the categoryDisounts only has 1 item
             */
            discountPercentage: product.category.categoryDiscounts[0]
                ? product.category.categoryDiscounts[0].discountPercentage
                : 0,
            totalPrice: 0,
            totalPriceAfterDiscount: 0,
        });

        orderItem = {
            ...newOrderItem,
        };
        orderItem = this.priceCalculateHelper.calculatePriceOfSingleOrderItem(
            orderItem,
        );

        return orderItem;
    }
    /** <<<<< END ADD ORDER ITEM TO ORDER >>>>> */

    async deleteOrderItem(id: string) {
        const orderItem = await this.findOrderItemByIdToDelete(id);
        if (!orderItem) {
            throw new NotFoundException();
        }

        /** Update order price when we delete order item
         * because order item is deleted then new price are all 0
         */
        let order = orderItem.order;
        order = this.priceCalculateHelper.updateOrderPriceAfterChangeOrderItem(
            order,
            orderItem.totalPrice,
            orderItem.totalPriceAfterDiscount,
            0,
            0,
        );

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            delete orderItem.order;

            await queryRunner.manager.remove(OrderItem, orderItem);
            await queryRunner.manager.save(Order, order);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async findOrderItemByIdToDelete(id: string): Promise<OrderItem> {
        return this.orderItemsRepository
            .createQueryBuilder('OrderItem')
            .leftJoinAndSelect('OrderItem.order', 'Order')
            .where('OrderItem.id = :id', { id })
            .andWhere('Order.status = :dealingStatus', {
                dealingStatus: OrderStatus.Dealing,
            })
            .getOne();
    }
}
