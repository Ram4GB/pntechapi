import { CustomOrderItem } from 'orders/entities/custom-order-item.entity';
import { Expose } from 'class-transformer';
import { OrderItem } from 'orders/entities/order-item.entity';
import { OrderStatus } from 'orders/constants/order-status.enum';
import { PriceCurrency } from 'shared/constants/price-currency';
import { User } from 'users/user.entity';

export class OrderResponseDTO {
    @Expose()
    id: string;

    status: OrderStatus;

    @Expose()
    description: string;

    @Expose()
    totalPrice: number;

    @Expose()
    totalPriceAfterDiscount: number;

    @Expose()
    priceCurrency: PriceCurrency;

    @Expose()
    createdAt: Date;

    @Expose()
    updatedAt: Date;

    @Expose()
    user: User;

    @Expose()
    orderItems: OrderItem[];

    @Expose()
    customOrderItems: CustomOrderItem[];
}
