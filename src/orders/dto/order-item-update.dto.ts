import { IsNumber, IsOptional, Min } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class OrderItemUpdateDTO {
    @ApiProperty()
    @IsOptional()
    @Type(() => Number)
    @IsNumber()
    @Min(1)
    quantity: number;

    @ApiProperty()
    @IsOptional()
    @Type(() => Number)
    @IsNumber()
    @Min(0)
    singlePrice: number;
}
