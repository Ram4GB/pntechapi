import { IsNotEmpty, IsNumber, IsUUID, Min } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { Type } from 'class-transformer';

export class OrderItemWhenAdminCreateDTO {
    @ApiProperty()
    @IsNotEmpty()
    @Type(() => Number)
    @IsNumber()
    @Min(1)
    quantity: number;

    @ApiProperty()
    @IsNotEmptyString({
        message: 'productId can not be empty',
    })
    @IsUUID()
    productId: string;
}
