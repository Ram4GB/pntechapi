import { IsEnum, IsOptional } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { OrderStatus } from 'orders/constants/order-status.enum';
import { PaginateQuery } from 'shared/helpers/pagination';
import { PriceCurrency } from 'shared/constants/price-currency';
import { Type } from 'class-transformer';

class OrderFilter {
    @IsOptional()
    totalPrice: number;

    @IsOptional()
    totalPriceAfterDiscount: number;

    @IsOptional()
    @IsEnum(OrderStatus)
    status: OrderStatus;

    @IsOptional()
    @IsEnum(PriceCurrency)
    priceCurrency: PriceCurrency;
}

export class OrderPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            priceCurrency: '=',
            status: '=',
            totalPrice: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            priceCurrency: PriceCurrency.VND,
            status: OrderStatus.Dealing,
            totalPrice: 10000000,
        },
    })
    @IsOptional()
    @Type(() => OrderFilter)
    filter: OrderFilter;
}
