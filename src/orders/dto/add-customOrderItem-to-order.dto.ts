import { IsNotEmpty, IsNumber, Min } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { Type } from 'class-transformer';

export class AddCustomOrderItemToOrderDTO {
    @ApiProperty()
    @IsNotEmpty()
    @Type(() => Number)
    @IsNumber()
    @Min(1)
    quantity: number;

    @ApiProperty()
    @IsNotEmpty()
    @Type(() => Number)
    @IsNumber()
    @Min(0)
    singlePrice: number;

    @ApiProperty()
    @IsNotEmptyString()
    name: string;

    @ApiProperty()
    @IsNotEmptyString()
    orderId: string;
}
