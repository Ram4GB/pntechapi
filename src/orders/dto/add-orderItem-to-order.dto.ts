import { IsNotEmpty, IsNumber, Min } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { Type } from 'class-transformer';

export class AddOrderItemToOrderDTO {
    @ApiProperty()
    @IsNotEmpty()
    @Type(() => Number)
    @IsNumber()
    @Min(1)
    quantity: number;

    @ApiProperty()
    @IsNotEmptyString()
    productId: string;

    @ApiProperty()
    @IsNotEmptyString()
    orderId: string;
}
