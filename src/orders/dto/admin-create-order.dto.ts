import { IsOptional, IsUUID, ValidateNested } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { CustomOrderItemWhenAdminCreateOrderDTO } from './custom-order-item-when-admin-create-order.dto';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { OrderItemWhenAdminCreateDTO } from './order-item-when-admin-create-order.dto';
import { Type } from 'class-transformer';

export class AdminCreateOrderDTO {
    @ApiProperty()
    @IsOptional()
    @ValidateNested({ each: true })
    @Type(() => OrderItemWhenAdminCreateDTO)
    orderItems: OrderItemWhenAdminCreateDTO[];

    @ApiProperty()
    @IsOptional()
    @ValidateNested({
        each: true,
    })
    @Type(() => CustomOrderItemWhenAdminCreateOrderDTO)
    customOrderItems: CustomOrderItemWhenAdminCreateOrderDTO[];

    @ApiProperty()
    @IsNotEmptyString()
    @IsUUID()
    customerId: string;
}
