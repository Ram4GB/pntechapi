import { IsNumber, IsOptional, Min } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { Type } from 'class-transformer';

export class CustomOrderItemUpdateDTO {
    @ApiProperty()
    @IsOptional()
    @Type(() => Number)
    @IsNumber()
    @Min(1)
    quantity: number;

    @ApiProperty()
    @IsOptional()
    @IsNotEmptyString()
    name: string;

    @ApiProperty()
    @IsOptional()
    @Type(() => Number)
    @IsNumber()
    @Min(0)
    singlePrice: number;
}
