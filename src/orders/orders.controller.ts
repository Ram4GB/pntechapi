import { ApiResponse, ApiTags } from '@nestjs/swagger';
import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Inject,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
    Req,
    UnauthorizedException,
    forwardRef,
} from '@nestjs/common';

import { AddCustomOrderItemToOrderDTO } from './dto/add-customOrderItem-to-order.dto';
import { AddOrderItemToOrderDTO } from './dto/add-orderItem-to-order.dto';
import { AdminCreateOrderDTO } from './dto/admin-create-order.dto';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { ChatsService } from 'chats/chats.services';
import { Connection } from 'typeorm';
import { CustomOrderItemUpdateDTO } from './dto/custom-order-item-update.dto';
import { CustomOrderItemsService } from './services/custom-order-items.service';
import { Order } from './entities/order.entity';
import { OrderItemUpdateDTO } from './dto/order-item-update.dto';
import { OrderItemsService } from './services/order-items.service';
import { OrderPaginateQuery } from './dto/order-query.dto';
import { OrdersService } from './services/orders.service';
import { PagedList } from 'shared/helpers/pagination';
import { ParseOrderCodeToIdPipe } from 'shared/pipes/parse-order-code-to-id.pipe';
import { Roles } from 'shared/constants/roles';
import { UsersService } from 'users/users.service';

@Controller('orders')
@ApiTags('Orders')
export class OrdersController {
    constructor(
        private connection: Connection,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        private readonly ordersService: OrdersService,
        private readonly orderItemsService: OrderItemsService,
        private readonly customOrderItemsService: CustomOrderItemsService,
        private readonly chatsService: ChatsService,
    ) {}

    @ApiResponse({
        status: 201,
        description: 'Response when add to cart successful',
        schema: {
            example: {
                user: {
                    id: '48532478-ffa4-429e-a573-cda1702ea6c0',
                    email: 'Dessie_Schowalter@gmail.com',
                    firstName: 'Dessie',
                    lastName: 'Schowalter',
                    phone: '0887162345',
                    status: 'active',
                    createdAt: '2020-08-12T08:10:43.901Z',
                    updatedAt: '2020-08-12T08:10:43.901Z',
                    role: '',
                },
                status: 'waiting-quotation',
                totalPrice: 28250000,
                totalPriceAfterDiscount: 28250000,
                priceCurrency: 'vnd',
                orderItems: [
                    {
                        quantity: 3,
                        singlePrice: 8750000,
                        totalPrice: 26250000,
                        discountPercentage: null,
                        totalPriceAfterDiscount: 26250000,
                        product: {
                            id: '75902f1f-06ae-4e4e-9e8b-dde8ac578a26',
                            name: 'product test 2',
                            generalDescription: null,
                            technicalDescription: null,
                            note: null,
                            price: 8750000,
                            quantity: 0,
                        },
                        id: '1694196e-77f4-43d0-934f-8f0497333242',
                        createdAt: '2020-08-12T08:29:44.316Z',
                        updatedAt: '2020-08-12T08:29:44.316Z',
                    },
                    {
                        quantity: 2,
                        singlePrice: 1000000,
                        totalPrice: 2000000,
                        discountPercentage: null,
                        totalPriceAfterDiscount: 2000000,
                        product: {
                            id: 'cf0bf4ff-5af1-41c2-81c1-555609754ce6',
                            name: 'product test 3',
                            generalDescription: null,
                            technicalDescription: null,
                            note: null,
                            price: 1000000,
                            quantity: 0,
                        },
                        id: 'b192345c-279f-4753-907c-dadfa1a7bd9e',
                        createdAt: '2020-08-12T08:29:44.324Z',
                        updatedAt: '2020-08-12T08:29:44.324Z',
                    },
                ],
                customOrderItems: [
                    {
                        name: 'Khong nho ten san pham',
                        status: 'custom-product',
                        quantity: 11,
                        singlePrice: null,
                        totalPrice: null,
                        discountPercentage: null,
                        totalPriceAfterDiscount: null,
                        id: 'e0512445-9346-4022-80b6-3d444b563e9c',
                        createdAt: '2020-08-12T08:29:44.331Z',
                        updatedAt: '2020-08-12T08:29:44.331Z',
                    },
                ],
                description: null,
                id: 1,
                createdAt: '2020-08-12T08:29:44.339Z',
                updatedAt: '2020-08-12T08:29:44.339Z',
            },
        },
    })
    @Authorize(Roles.Customer)
    @Post()
    @HttpCode(201)
    async create(@Req() req: any): Promise<Order> {
        /** Get user entity to attach to order */
        const user = await this.usersService.getOnlyUserInfoByEmail(
            req.user.email,
        );
        if (!user) {
            throw new UnauthorizedException();
        }

        const order = new Order({
            user,
        });

        /** If cart is empty then no order is created from it */
        const userCarts = await this.usersService.getAllProductInCartToMapToOrder(
            user.id,
        );
        this.ordersService.validateNotEmptyCartWhenCreateOrder(
            userCarts.carts,
            userCarts.customCartProducts,
        );

        /** Create order */
        const createdOrder = await this.ordersService.createOrderFromCart(
            order,
            userCarts.carts,
            userCarts.customCartProducts,
        );

        /** Clear cart */
        await this.usersService.clearUserCart(user.id);

        /** Create chat room */
        await this.chatsService.createChatRoom(
            createdOrder.id,
            createdOrder.userId,
        );
        return createdOrder;
    }

    @Authorize(Roles.Customer, Roles.Admin, Roles.Seller)
    @Get()
    @HttpCode(200)
    async getAll(
        @Req() req: any,
        @Query() query: OrderPaginateQuery,
    ): Promise<PagedList<Order>> {
        const user = await this.usersService.getActiveUserByEmail(
            req.user.email,
        );
        if (!user) {
            throw new UnauthorizedException();
        }

        const orders = await this.ordersService.listOrdersPaginated(
            query,
            user,
        );

        return orders;
    }

    @Authorize(Roles.Customer, Roles.Admin, Roles.Seller)
    @Get(':id')
    @HttpCode(200)
    async getById(
        @Req() req: any,
        @Param('id', ParseOrderCodeToIdPipe) id: any,
    ): Promise<Order> {
        const user = await this.usersService.getActiveUserByEmail(
            req.user.email,
        );
        if (!user) {
            throw new UnauthorizedException();
        }

        const order = await this.ordersService.findById(id, user);
        if (!order) {
            throw new NotFoundException();
        }

        return order;
    }

    @Authorize(Roles.Admin)
    @Put('orderItems/:id')
    @HttpCode(204)
    async updateOrderItem(
        @Param('id') id: string,
        @Body() updateInfo: OrderItemUpdateDTO,
    ): Promise<void> {
        await this.orderItemsService.update(id, updateInfo);
    }

    @Authorize(Roles.Admin)
    @Put('customOrderItems/:id')
    @HttpCode(204)
    async updateCustomOrderItem(
        @Param('id') id: string,
        @Body() updateInfo: CustomOrderItemUpdateDTO,
    ): Promise<void> {
        await this.customOrderItemsService.update(id, updateInfo);
    }

    @Authorize(Roles.Admin)
    @Post('orderItems')
    @HttpCode(201)
    async addOrderItemToOrder(
        @Body() addOrderItemInfo: AddOrderItemToOrderDTO,
        @Body('orderId', ParseOrderCodeToIdPipe) orderId: any,
    ): Promise<void> {
        await this.orderItemsService.addOrderItemToOrder(
            addOrderItemInfo,
            orderId,
        );
    }

    @Authorize(Roles.Admin)
    @Post('customOrderItems')
    @HttpCode(201)
    async addCustomOrderItemToOrder(
        @Body() addCustomOrderItemInfo: AddCustomOrderItemToOrderDTO,
        @Body('orderId', ParseOrderCodeToIdPipe) orderId: any,
    ): Promise<void> {
        await this.customOrderItemsService.addCustomOrderItemToOrder(
            addCustomOrderItemInfo,
            orderId,
        );
    }

    @Authorize(Roles.Admin)
    @Delete('orderItems/:id')
    @HttpCode(204)
    async deleteOrderItem(@Param('id') id: string): Promise<void> {
        await this.orderItemsService.deleteOrderItem(id);
    }

    @Authorize(Roles.Admin)
    @Delete('customOrderItems/:id')
    @HttpCode(204)
    async deleteCustomOrderItem(@Param('id') id: string): Promise<void> {
        await this.customOrderItemsService.deleteCustomOrderItem(id);
    }

    @Authorize(Roles.Admin)
    @Post('quotation/:id')
    @HttpCode(200)
    async sendQuotationToCustomerByMail(
        @Param('id', ParseOrderCodeToIdPipe) id: any,
    ): Promise<void> {
        await this.ordersService.sendQuotationToCustomerEmail(id);
    }

    @Authorize(Roles.Admin)
    @Post('admin')
    @HttpCode(201)
    async createOrderByAdmin(
        @Body() createInfo: AdminCreateOrderDTO,
    ): Promise<void> {
        await this.ordersService.createOrderByAdmin(createInfo);
    }

    @Authorize(Roles.Admin)
    @Put(':id/done')
    @HttpCode(204)
    async doneOrder(
        @Param('id', ParseOrderCodeToIdPipe) id: any,
    ): Promise<void> {
        await this.ordersService.changeOrderStatusToDone(id);
    }

    @Authorize(Roles.Admin)
    @Put(':id/cancel')
    @HttpCode(204)
    async cancelOrder(
        @Param('id', ParseOrderCodeToIdPipe) id: any,
    ): Promise<void> {
        await this.ordersService.changeOrderStatusToCancel(id);
    }
}
