import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    NotFoundException,
    Param,
    Post,
    Query,
    Req,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { PagedList } from 'shared/helpers/pagination';
import { ProductImport } from './product-import.entity';
import { ProductImportCreateDTO } from './dto/product-import-create.dto';
import { ProductImportPaginateQuery } from './dto/product-import.query';
import { ProductImportsService } from './product-imports.service';
import { Roles } from 'shared/constants/roles';

@Controller('productImports')
@ApiTags('ProductImports')
export class ProductImportsController {
    constructor(
        private readonly productImportsService: ProductImportsService,
    ) {}

    @Authorize(Roles.Admin)
    @Post()
    @HttpCode(201)
    async create(
        @Req() req: any,
        @Body() createInfo: ProductImportCreateDTO,
    ): Promise<ProductImport> {
        return await this.productImportsService.create(createInfo, req.user.id);
    }

    @Get()
    @Authorize(Roles.Admin)
    @HttpCode(200)
    async findAll(
        @Query() query: ProductImportPaginateQuery,
    ): Promise<PagedList<ProductImport>> {
        return await this.productImportsService.listProdcutImportsPaginated(
            query,
        );
    }

    @Get(':id')
    @Authorize(Roles.Admin)
    @HttpCode(200)
    async findById(@Param('id') id: string): Promise<ProductImport> {
        const productImport = await this.productImportsService.findById(id);
        if (!productImport) {
            throw new NotFoundException();
        }

        return productImport;
    }

    @Delete(':id')
    @Authorize(Roles.Admin)
    @HttpCode(204)
    async delete(@Param('id') id: string): Promise<void> {
        this.productImportsService.delete(id);
    }
}
