import { Module } from '@nestjs/common';
import { ProductImport } from './product-import.entity';
import { ProductImportDetail } from './product-import-detail.entity';
import { ProductImportsController } from './product-imports.controller';
import { ProductImportsService } from './product-imports.service';
import { ProductsModule } from 'products/products.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'users/users.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([ProductImport, ProductImportDetail]),
        UsersModule,
        ProductsModule,
    ],
    providers: [ProductImportsService],
    controllers: [ProductImportsController],
    exports: [ProductImportsService],
})
export class ProductImportsModule {}
