import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { ProductImportDetail } from './product-import-detail.entity';
import { User } from 'users/user.entity';

@Entity()
export class ProductImport {
    constructor(partial: Partial<ProductImport>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column({ type: 'text', nullable: true })
    description: string;

    @Column()
    totalPrice: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(
        type => User,
        createdBy => createdBy.productImports,
    )
    createdBy: User;

    @OneToMany(
        type => ProductImportDetail,
        productImportDetail => productImportDetail.productImport,
    )
    productImportDetails: ProductImportDetail[];
}
