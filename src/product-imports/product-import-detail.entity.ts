import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { Expose } from 'class-transformer';
import { Product } from 'products/products.entity';
import { ProductImport } from './product-import.entity';

@Entity()
export class ProductImportDetail {
    constructor(partial: Partial<ProductImportDetail>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    @Expose()
    quantity: number;

    @Column()
    @Expose()
    singlePrice: number;

    @Column()
    totalPrice: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(
        type => ProductImport,
        productImport => productImport.productImportDetails,
    )
    productImport: ProductImport;

    @ManyToOne(
        type => Product,
        product => product.productImportDetails,
    )
    product: Product;
}
