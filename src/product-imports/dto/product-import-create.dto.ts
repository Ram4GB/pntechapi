import { IsNotEmpty, IsOptional, ValidateNested } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { ProductImportDetailCreateDTO } from './product-import-detail-create.dto';
import { Type } from 'class-transformer';

export class ProductImportCreateDTO {
    @ApiProperty()
    @IsNotEmpty()
    @ValidateNested({
        each: true,
    })
    @Type(() => ProductImportDetailCreateDTO)
    productImportDetails: ProductImportDetailCreateDTO[];

    @ApiProperty()
    @IsOptional()
    description: string;
}
