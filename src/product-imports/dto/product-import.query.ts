import { ApiPropertyOptional } from '@nestjs/swagger';
import { CategoryStatus } from 'categories/constant/category.enum';
import { IsOptional } from 'class-validator';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class ProductImportFilter {
    @IsOptional()
    @Type(() => Number)
    totalPrice: number;
}

export class ProductImportPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            name: 'like',
            parentId: '=',
            status: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            name: 'category 1',
            status: CategoryStatus.Show,
        },
    })
    @IsOptional()
    @Type(() => ProductImportFilter)
    filter: ProductImportFilter;
}
