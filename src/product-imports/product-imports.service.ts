import * as _ from 'lodash';

import { Connection, Repository, SelectQueryBuilder } from 'typeorm';
import {
    Inject,
    Injectable,
    NotFoundException,
    forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';

import { Media } from 'media/media.entity';
import { MediaUsage } from 'media/media-usage.entity';
import { Product } from 'products/products.entity';
import { ProductImport } from './product-import.entity';
import { ProductImportCreateDTO } from './dto/product-import-create.dto';
import { ProductImportDetail } from './product-import-detail.entity';
import { ProductImportDetailCreateDTO } from './dto/product-import-detail-create.dto';
import { ProductImportPaginateQuery } from './dto/product-import.query';
import { ProductsService } from 'products/products.service';
import { User } from 'users/user.entity';
import { UsersService } from 'users/users.service';
import { paginate } from 'shared/helpers/pagination';

@Injectable()
export class ProductImportsService {
    constructor(
        @InjectRepository(ProductImport)
        private productImportsRepository: Repository<ProductImport>,
        @InjectConnection()
        private connection: Connection,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => ProductsService))
        private readonly productsService: ProductsService,
    ) {}

    async create(
        createInfo: ProductImportCreateDTO,
        adminId: string,
    ): Promise<ProductImport> {
        const admin = await this.usersService.getActiveAdminById(adminId);
        if (!admin) {
            throw new NotFoundException();
        }

        /** Remove duplicate product */
        const uniqueProductImportDetailsDto = _.uniqBy(
            createInfo.productImportDetails,
            'productId',
        );
        const products = await this.getValidProductsFromProductImportDetailsDto(
            uniqueProductImportDetailsDto,
        );

        const newProductImport = this.prepareNewProductImport(
            uniqueProductImportDetailsDto,
            createInfo.description,
            admin,
        );

        const newProductImportDetails = this.prepareNewProductImportDetails(
            uniqueProductImportDetailsDto,
            products,
        );

        let createdProductImport: ProductImport;
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const createdProductImportDetails = await queryRunner.manager.save(
                ProductImportDetail,
                newProductImportDetails,
            );

            newProductImport.productImportDetails = createdProductImportDetails;

            createdProductImport = await queryRunner.manager.save(
                ProductImport,
                newProductImport,
            );

            await queryRunner.manager.save(Product, products);

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }

        return createdProductImport;
    }

    async getValidProductsFromProductImportDetailsDto(
        productImportDetails: ProductImportDetailCreateDTO[],
    ): Promise<Product[]> {
        const productIds = productImportDetails.map(p => p.productId);

        const products = await this.productsService.findProductsByIds(
            productIds,
        );

        /** If list product get from db different from list product ids
         * from client => some products are not valid
         */
        if (products.length !== productIds.length) {
            throw new NotFoundException();
        }

        return products;
    }

    prepareNewProductImport(
        productImportDetails: ProductImportDetailCreateDTO[],
        description: string,
        admin: User,
    ): ProductImport {
        const productImportTotalPrice = productImportDetails.reduce(
            (
                total: number,
                productImportDetail: ProductImportDetailCreateDTO,
            ) => {
                return (total +=
                    productImportDetail.singlePrice *
                    productImportDetail.quantity);
            },
            0,
        );

        const newProductImport = new ProductImport({
            totalPrice: productImportTotalPrice,
            description: description || null,
            createdBy: admin,
        });

        return newProductImport;
    }

    prepareNewProductImportDetails(
        productImportDetails: ProductImportDetailCreateDTO[],
        products: Product[],
    ): ProductImportDetail[] {
        const newProductImportDetails = productImportDetails.map(
            (productImportDetailDto: ProductImportDetailCreateDTO) => {
                const productOfProductImportDetail = products.find(
                    p => p.id === productImportDetailDto.productId,
                );

                productOfProductImportDetail.quantity +=
                    productImportDetailDto.quantity;

                const newProductImportDetail = new ProductImportDetail({
                    product: productOfProductImportDetail,
                    quantity: productImportDetailDto.quantity,
                    singlePrice: productImportDetailDto.singlePrice,
                    totalPrice:
                        productImportDetailDto.singlePrice *
                        productImportDetailDto.quantity,
                });

                return newProductImportDetail;
            },
        );

        return newProductImportDetails;
    }

    async listProdcutImportsPaginated(query: ProductImportPaginateQuery) {
        return paginate<ProductImport>(
            this.productImportsRepository,
            query,
            (builder: SelectQueryBuilder<ProductImport>) => {
                builder.leftJoinAndSelect(
                    'ProductImport.createdBy',
                    'CreatedBy',
                );
            },
        );
    }

    async findById(id: string): Promise<ProductImport> {
        return this.productImportsRepository
            .createQueryBuilder('ProductImport')
            .leftJoinAndSelect(
                'ProductImport.productImportDetails',
                'ProductImportDetail',
            )
            .leftJoinAndSelect('ProductImportDetail.product', 'Product')
            .leftJoinAndSelect(
                MediaUsage,
                'MediaUsage',
                "Product.id = MediaUsage.usedById AND MediaUsage.type = 'product'",
            )
            .leftJoinAndMapOne(
                'Product.media',
                Media,
                'Media',
                'MediaUsage.mediaId = Media.id',
            )
            .andWhere('MediaUsage.weight = 1 OR MediaUsage.weight IS NULL')
            .where('ProductImport.id = :id', { id })
            .getOne();
    }

    async delete(id: string): Promise<void> {
        const deletedProductImport = await this.findById(id);
        if (!deletedProductImport) {
            throw new NotFoundException();
        }

        const deletedProductImportDetails =
            deletedProductImport.productImportDetails || [];

        const updatedProducts = deletedProductImportDetails.map(
            (productImportDetail: ProductImportDetail) => {
                const product = productImportDetail.product;
                /** Update quantity after product import detail is deleted
                 * Minus product detail quantity
                 */
                product.quantity -= productImportDetail.quantity;

                return product;
            },
        );

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            await queryRunner.manager.remove(
                ProductImportDetail,
                deletedProductImportDetails,
            );
            await queryRunner.manager.save(Product, updatedProducts);
            await queryRunner.manager.remove(
                ProductImport,
                deletedProductImport,
            );
            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }
}
