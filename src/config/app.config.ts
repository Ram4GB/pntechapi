export default () => ({
    name: require('../../package.json').name,
    title: require('../../package.json').title,
    version: require('../../package.json').version,
    port: parseInt(process.env.SERVER_PORT, 10) || 3000,
});
