export default () => ({
    host: process.env.MAIL_HOST || 'smtp.gmail.com',
    port: parseInt(process.env.MAIL_PORT, 10) || 587,
    from: process.env.MAIL_FROM || '"PnTech" <nam@pntech.vn>',
    username: process.env.MAIL_USERNAME,
    password: process.env.MAIL_PASSWORD,
});
