import {
    MulterModuleOptions,
    MulterOptionsFactory,
} from '@nestjs/platform-express';

import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
    constructor(private readonly configService: ConfigService) {}

    createMulterOptions(): MulterModuleOptions {
        return {
            dest: this.configService.get<string>('MULTER_DEST'),
        };
    }
}
