import AppConfig from './app.config';
import DatabaseConfig from './database.config';
import MailConfig from './mail.config';
import { Module } from '@nestjs/common';
import { MulterConfigService } from './multer-config.services';
import { ConfigModule as NestConfigModule } from '@nestjs/config';

@Module({
    imports: [
        NestConfigModule.forRoot({
            isGlobal: true,
            envFilePath: `.env${
                process.env.NODE_ENV ? `.${process.env.NODE_ENV}` : ''
            }`,
            load: [
                () => ({ app: AppConfig() }),
                () => ({ database: DatabaseConfig() }),
                () => ({ mail: MailConfig() }),
            ],
        }),
    ],
    providers: [MulterConfigService],
    exports: [MulterConfigService],
})
export class ConfigModule {}
