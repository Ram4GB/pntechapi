import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';

export default () =>
    ({
        type: process.env.DATABASE_DIALECT || 'mysql',
        host: process.env.DATABASE_HOST || 'localhost',
        port: parseInt(process.env.DATABASE_PORT, 10) || 3306,
        username: process.env.DATABASE_USER || 'root',
        password: process.env.DATABASE_PASSWORD || 'root',
        database: process.env.DATABASE_NAME || 'pntech',
        logging: process.env.DATABASE_LOGGING === 'true',
        entities: [join(__dirname, '../', '**/*.entity{.ts,.js}')],
        migrations: [join(__dirname, '../', 'database/migrations/**/*.ts')],
        synchronize: process.env.DATABASE_SYNCHRONIZE === 'true',
        dropSchema: process.env.DATABASE_DROP_SCHEMA === 'true',
        migrationsRun: process.env.DATABASE_MIGRATIONS_RUN === 'true',
    } as TypeOrmModuleOptions);
