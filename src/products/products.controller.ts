import { ApiBearerAuth, ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import {
    BadRequestException,
    Body,
    Controller,
    Get,
    HttpCode,
    Inject,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
    forwardRef,
} from '@nestjs/common';

import { Authorize } from 'shared/decorators/authorize.decorator';
import { CategoriesService } from 'categories/categories.service';
import { MediaService } from 'media/media.service';
import { PagedList } from 'shared/helpers/pagination';
import { Product } from './products.entity';
import { ProductCreateDTO } from './dto/product-create.dto';
import { ProductPaginateQuery } from './dto/product.query';
import { ProductUpdateDTO } from './dto/product-update.dto';
import { ProductsService } from './products.service';
import { ReqUser } from 'shared/decorators/request-user.decorator';
import { RequestUser } from 'auth/dto/req-user.interface';
import { Roles } from 'shared/constants/roles';
import { ShowProductPaginateQuery } from './dto/show-product.query';
import { User } from 'users/user.entity';

@Controller('products')
@ApiTags('Products')
export class ProductsController {
    constructor(
        private readonly productsService: ProductsService,
        @Inject(forwardRef(() => CategoriesService))
        private readonly categoriesService: CategoriesService,
        private readonly mediaService: MediaService,
    ) {}

    @ApiBearerAuth()
    @ApiBody({
        schema: {
            example: {
                name: 'product 8',
                price: '10000000',
                categoryId: '6449c616-cc9b-4836-b28e-78665f21e7fd',
                useMedia: [
                    {
                        id: 'de253f81-954b-4fd3-bd23-ccd27104169f',
                        weight: 1,
                    },
                    {
                        id: 'e54cab41-df91-447f-9d82-188f077c96db',
                        weight: 2,
                    },
                ],
            },
        },
    })
    @Authorize(Roles.Admin)
    @Post()
    @HttpCode(201)
    async create(
        @Body() productCreateInfo: ProductCreateDTO,
        @ReqUser() requestUser: RequestUser,
    ): Promise<void> {
        const category = await this.categoriesService.findOneById(
            productCreateInfo.categoryId,
        );
        if (!category) {
            throw new BadRequestException(
                'Danh mục cho sản phẩm không tồn tại',
            );
        }

        const product = new Product({
            ...productCreateInfo,
            createdBy: new User({ id: requestUser.id }),
            category,
        });
        const createdProduct = await this.productsService.create(product);

        /** Add images for product from media */
        if (
            productCreateInfo.useMedia &&
            productCreateInfo.useMedia.length > 0
        ) {
            await this.mediaService.useMultiMedia(
                'product',
                createdProduct.id,
                productCreateInfo.useMedia,
            );
        }
    }

    @ApiBearerAuth()
    @ApiBody({
        schema: {
            description: 'detail in ProductUpdateDTO',
            example: {
                name: 'product 8',
                price: '10000000',
                categoryId: '6449c616-cc9b-4836-b28e-78665f21e7fd',
                useMedia: [
                    {
                        id: 'de253f81-954b-4fd3-bd23-ccd27104169f',
                        weight: 1,
                    },
                    {
                        id: 'e54cab41-df91-447f-9d82-188f077c96db',
                        weight: 2,
                    },
                ],
            },
        },
    })
    @Authorize(Roles.Admin)
    @Put(':id')
    @HttpCode(201)
    async update(
        @Param('id') id: string,
        @Body() productUpdateInfo: ProductUpdateDTO,
    ): Promise<void> {
        const category = await this.categoriesService.findOneById(
            productUpdateInfo.categoryId,
        );
        if (!category) {
            throw new BadRequestException(
                'Danh mục cho sản phẩm không tồn tại',
            );
        }

        await this.productsService.update(id, productUpdateInfo);

        /** Update images for product from media */
        await this.mediaService.unUsedMediaAnymore('product', id);

        if (
            productUpdateInfo.useMedia &&
            productUpdateInfo.useMedia.length > 0
        ) {
            await this.mediaService.useMultiMedia(
                'product',
                id,
                productUpdateInfo.useMedia,
            );
        }
    }

    @Get()
    @Authorize(Roles.Anonymous)
    async listProducts(
        @ReqUser() user: RequestUser,
        @Query() query: ShowProductPaginateQuery,
        @Query() queryFull: ProductPaginateQuery,
    ): Promise<PagedList<Product>> {
        if (user.role === Roles.Admin) {
            return this.productsService.listFullProductsPaginated(queryFull);
        } else {
            const res = await this.productsService.listOnlyShowProductsPaginated(
                query,
            );
            res.data.forEach(product => (product.note = null));
            return res;
        }
    }

    @ApiResponse({
        status: 200,
        schema: {
            example: {
                id: '35a0caa0-34a3-4b7c-98b4-1cc88a25eb5b',
                name: 'Product 1',
                price: 200000,
                quantity: 0,
                generalDescription: null,
                technicalDescription: null,
                note: null,
                status: 'show',
                createdAt: '2020-08-12T19:42:47.607Z',
                updatedAt: '2020-08-12T19:42:47.607Z',
                categoryId: '6449c616-cc9b-4836-b28e-78665f21e7fd',
                category: {
                    id: '6449c616-cc9b-4836-b28e-78665f21e7fd',
                    name: 'Category 1',
                    parentId: '10d2833e-5030-4dab-93d8-4000a77f38a4',
                    status: 'show',
                    createdAt: '2020-08-12T19:42:00.428Z',
                    updatedAt: '2020-08-12T19:42:00.428Z',
                },
                media: [
                    {
                        weight: 1,
                        id: '00cac506-c024-4e55-8d49-4cd846af76f8',
                        name: 'illustration-wallpaper-1920x1080-wallpaper.jpg',
                        uri:
                            'uploads/media/4d4cb30b-99d7-479e-b4ef-21879ca61882.jpg',
                        filemine: 'image/jpeg',
                    },
                    {
                        weight: 2,
                        id: '42b8886e-3655-4d16-b12f-1666e3b239e5',
                        name: 'Screenshot from 2020-08-09 21-15-28.png',
                        uri:
                            'uploads/media/c387c8ef-740d-4415-b9b7-755e0609bf8f.png',
                        filemine: 'image/png',
                    },
                ],
            },
        },
    })
    @Authorize(Roles.Anonymous)
    @Get(':id')
    async findById(
        @Param('id') id: string,
        @ReqUser() user: RequestUser,
    ): Promise<Product> {
        const product = await this.productsService.findFullInfoById(id);
        if (!product) {
            throw new NotFoundException('Product not found');
        }
        if (user.role !== Roles.Admin) {
            product.note = null;
        }
        return product;
    }
}
