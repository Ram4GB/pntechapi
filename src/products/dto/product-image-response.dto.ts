import { Expose } from 'class-transformer';

export class ProductImageResponseDTO {
    @Expose()
    id: string;

    @Expose()
    createdAt: Date;

    @Expose()
    isProductAvatar: boolean;
}
