import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
    IsCurrency,
    IsEnum,
    IsNotEmpty,
    IsOptional,
    IsUUID,
    ValidateNested,
} from 'class-validator';

import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { ProductStatus } from 'products/constant/product-status.enum';
import { Type } from 'class-transformer';
import { UseMediaDTO } from 'media/dto/use-media.dto';

export class ProductCreateDTO {
    @ApiProperty()
    @IsNotEmptyString()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsCurrency()
    price: number;

    @ApiPropertyOptional()
    @IsOptional()
    generalDescription: string;

    @ApiPropertyOptional()
    @IsOptional()
    technicalDescription: string;

    @ApiPropertyOptional()
    @IsOptional()
    note: string;

    @ApiPropertyOptional({ default: ProductStatus.Show })
    @IsOptional()
    @IsEnum(ProductStatus)
    status: ProductStatus;

    @ApiProperty()
    @IsUUID()
    categoryId: string;

    @ApiPropertyOptional()
    @IsOptional()
    @ValidateNested()
    @Type(() => UseMediaDTO)
    useMedia: UseMediaDTO[];
}
