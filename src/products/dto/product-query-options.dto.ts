import { IsOptional } from 'class-validator';
import { QueryOptions } from 'shared/dtos/query-options.dto';

export class ProductQueryOptions extends QueryOptions {
    @IsOptional()
    name: string;
}
