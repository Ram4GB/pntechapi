import { Expose, Type } from 'class-transformer';

import { CategoryResponseDTO } from 'categories/dto/category-response.dto';
import { Media } from 'media/media.entity';
import { ProductImageResponseDTO } from './product-image-response.dto';

export class ProductResponseDTO {
    @Expose()
    id: string;

    @Expose()
    name: string;

    @Expose()
    generalDescription: string;

    @Expose()
    technicalDescription: string;

    @Expose()
    note: string;

    @Expose()
    price: number;

    @Expose()
    quantity: number;

    @Expose()
    createdAt: Date;

    @Expose()
    lastUpdatedAt: Date;

    @Expose()
    @Type(() => ProductImageResponseDTO)
    productImages: ProductImageResponseDTO[];

    @Expose()
    @Type(() => CategoryResponseDTO)
    category: CategoryResponseDTO;

    @Expose()
    @Type(() => CategoryResponseDTO)
    Media: Media;
}
