import { IsEnum, IsOptional, IsUUID } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { PaginateQuery } from 'shared/helpers/pagination';
import { ProductStatus } from 'products/constant/product-status.enum';
import { Type } from 'class-transformer';

class ProductFilter {
    @IsOptional()
    name: any;

    @IsOptional()
    price: any;

    @IsOptional()
    @IsEnum(ProductStatus)
    status: ProductStatus;

    @IsOptional()
    @IsUUID()
    categoryId: any;
}

export class ProductPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            name: 'like',
            price: '=',
            status: '=',
            categoryId: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            name: '',
            status: ProductStatus.Show,
            categoryId: 'fd9790bb-8660-4750-abce-57f981869598',
        },
    })
    @IsOptional()
    @Type(() => ProductFilter)
    filter: ProductFilter;
}
