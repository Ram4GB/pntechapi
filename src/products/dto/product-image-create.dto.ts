import { IsNotEmpty } from 'class-validator';

export class ProductImageCreateDTO {
    @IsNotEmpty({
        message: 'Vui lòng chọn sản phẩm Id cho hình ảnh',
    })
    productId: string;
}
