import { IsOptional, IsUUID } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class ShowProductFilter {
    @IsOptional()
    name: any;

    @IsOptional()
    @IsUUID()
    categoryId: any;
}

export class ShowProductPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            name: 'like',
            price: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            name: '',
        },
    })
    @IsOptional()
    @Type(() => ShowProductFilter)
    filter: ShowProductFilter;
}
