import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
    IsCurrency,
    IsEnum,
    IsNotEmpty,
    IsOptional,
    IsUUID,
    ValidateNested,
} from 'class-validator';

import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { ProductStatus } from 'products/constant/product-status.enum';
import { Type } from 'class-transformer';
import { UseMediaDTO } from 'media/dto/use-media.dto';

export class ProductUpdateDTO {
    @ApiPropertyOptional()
    @IsOptional()
    @IsNotEmptyString()
    name: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsNotEmptyString()
    generalDescription: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsNotEmptyString()
    technicalDescription: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsNotEmpty()
    note: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsCurrency()
    price: number;

    @ApiPropertyOptional()
    @IsOptional()
    @IsEnum(ProductStatus)
    status: ProductStatus;

    @ApiProperty()
    @IsUUID()
    categoryId: string;

    @ApiPropertyOptional()
    @IsOptional()
    @ValidateNested()
    @Type(() => UseMediaDTO)
    useMedia: UseMediaDTO[];
}
