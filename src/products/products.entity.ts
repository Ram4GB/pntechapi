import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { Cart } from 'carts/cart.entity';
import { Category } from 'categories/category.entity';
import { Expose } from 'class-transformer';
import { OrderItem } from 'orders/entities/order-item.entity';
import { ProductImportDetail } from 'product-imports/product-import-detail.entity';
import { ProductStatus } from './constant/product-status.enum';
import { User } from 'users/user.entity';

@Entity()
export class Product {
    constructor(partial: Partial<Product>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Expose()
    @Column()
    name: string;

    @Column()
    @Expose()
    price: number;

    @Column({ default: 0 })
    quantity: number;

    @Column({ type: 'text', nullable: true })
    generalDescription: string;

    @Column({ type: 'text', nullable: true })
    technicalDescription: string;

    @Column({ type: 'text', nullable: true })
    note: string;

    @Column({
        type: 'enum',
        enum: ProductStatus,
        default: ProductStatus.Show,
    })
    status: ProductStatus;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    @Expose()
    categoryId: string;

    media: any | any[];

    @OneToMany(
        type => Cart,
        cart => cart.product,
    )
    carts: Cart[];

    @ManyToOne(
        type => User,
        createdBy => createdBy.products,
    )
    createdBy: User;

    @ManyToOne(
        type => Category,
        category => category.products,
    )
    @Expose()
    category: Category;

    @OneToMany(
        type => OrderItem,
        orderItem => orderItem.product,
    )
    orderItems: OrderItem[];

    @OneToMany(
        type => ProductImportDetail,
        productImportDetail => productImportDetail.product,
    )
    productImportDetails: ProductImportDetail[];
}
