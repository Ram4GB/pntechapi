import { CategoriesModule } from 'categories/categories.module';
import { HelpersModule } from 'shared/helpers/helpers.module';
import { MediaModule } from 'media/media.module';
import { Module } from '@nestjs/common';
import { PaginationModule } from 'shared/pagination/pagination.module';
import { Product } from './products.entity';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'users/users.module';

@Module({
    providers: [ProductsService],
    controllers: [ProductsController],
    imports: [
        TypeOrmModule.forFeature([Product]),
        UsersModule,
        CategoriesModule,
        PaginationModule,
        HelpersModule,
        MediaModule,
    ],
    exports: [ProductsService],
})
export class ProductsModule {}
