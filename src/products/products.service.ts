import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository, SelectQueryBuilder } from 'typeorm';

import { InjectRepository } from '@nestjs/typeorm';
import { Media } from 'media/media.entity';
import { MediaService } from 'media/media.service';
import { MediaUsage } from 'media/media-usage.entity';
import { Product } from './products.entity';
import { ProductPaginateQuery } from './dto/product.query';
import { ProductStatus } from './constant/product-status.enum';
import { ProductUpdateDTO } from './dto/product-update.dto';
import { ShowProductPaginateQuery } from './dto/show-product.query';
import { paginate } from 'shared/helpers/pagination';

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(Product)
        private productsRepository: Repository<Product>,
        private readonly mediaService: MediaService,
    ) {}

    async listOnlyShowProductsPaginated(query: ShowProductPaginateQuery) {
        return paginate<Product>(
            this.productsRepository,
            query,
            (builder: SelectQueryBuilder<Product>) => {
                builder
                    .leftJoinAndSelect('Product.category', 'category')
                    .leftJoinAndSelect(
                        MediaUsage,
                        'MediaUsage',
                        "Product.id = MediaUsage.usedById AND MediaUsage.type = 'product'",
                    )
                    .leftJoinAndMapOne(
                        'Product.media',
                        Media,
                        'Media',
                        'MediaUsage.mediaId = Media.id',
                    )
                    .andWhere(
                        `Product.status = '${ProductStatus.Show}' AND (MediaUsage.weight = 1 OR MediaUsage.weight IS NULL)`,
                    );
            },
        );
    }

    async listFullProductsPaginated(query: ProductPaginateQuery) {
        return paginate<Product>(
            this.productsRepository,
            query,
            (builder: SelectQueryBuilder<Product>) => {
                builder
                    .leftJoinAndSelect('Product.category', 'category')
                    .leftJoinAndSelect(
                        MediaUsage,
                        'MediaUsage',
                        "Product.id = MediaUsage.usedById AND MediaUsage.type = 'product'",
                    )
                    .leftJoinAndMapOne(
                        'Product.media',
                        Media,
                        'Media',
                        'MediaUsage.mediaId = Media.id',
                    )
                    .andWhere(
                        'MediaUsage.weight = 1 OR MediaUsage.weight IS NULL',
                    );
            },
        );
    }

    async findById(id: string): Promise<Product> {
        return await this.productsRepository.findOne(id);
    }

    async findProductByIdToUpdateOrderItem(
        productId: string,
        userId: string,
    ): Promise<Product> {
        return await this.productsRepository
            .createQueryBuilder('Product')
            .leftJoinAndSelect('Product.category', 'Category')
            .leftJoinAndSelect(
                'Category.categoryDiscounts',
                'CategoryDiscount',
                'CategoryDiscount.userId = :userId',
                { userId },
            )
            .where('Product.id = :productId', { productId })
            .getOne();
    }

    async findFullInfoById(id: string): Promise<Product> {
        const product = await this.productsRepository
            .createQueryBuilder('Product')
            .leftJoinAndSelect('Product.category', 'category')
            .where('Product.id = :id', { id })
            .getOne();
        const mediaOfProduct = await this.mediaService.getAllMediaUsedByEntity(
            'product',
            id,
        );
        product.media = mediaOfProduct;
        return product;
    }

    async create(product: Product): Promise<Product> {
        return this.productsRepository.save(product);
    }

    async update(
        id: string,
        productUpdateInfo: ProductUpdateDTO,
    ): Promise<void> {
        const validUpdateInfo = { ...productUpdateInfo };
        delete validUpdateInfo.useMedia;

        const existingProduct = await this.productsRepository.findOne(id);
        if (!existingProduct) {
            throw new NotFoundException('Product not found');
        }

        await this.productsRepository.update(id, validUpdateInfo);
    }

    // async findFullInfoById(id: string): Promise<Product> {
    //     const product = await this.productsRepository
    //         .createQueryBuilder('Product')
    //         .leftJoinAndSelect('Product.category', 'category')
    //         .leftJoinAndMapMany(
    //             'Product.media',
    //             subQuery => {
    //                 const sub = subQuery
    //                     .subQuery()
    //                     .select('MediaUsage.usedById', 'Media_usedById')
    //                     .addSelect('MediaUsage.weight', 'Media_weight')
    //                     .from(MediaUsage, 'MediaUsage')
    //                     .leftJoinAndSelect(
    //                         Media,
    //                         'MediaSub',
    //                         'MediaUsage.mediaId = MediaSub.id',
    //                     )
    //                     .where(
    //                         "MediaUsage.usedById = :id And Type= 'product' ",
    //                         {
    //                             id,
    //                         },
    //                     )
    //                     .orderBy('MediaUsage.usedById', 'ASC')
    //                     .addOrderBy('MediaUsage.weight', 'ASC');
    //                 return sub;
    //             },
    //             'Media',
    //             'Product.id = Media.Media_usedById',
    //         )
    //         .where('Product.id = :id', { id })
    //         .getOne();

    //     return product;
    // }

    async findManyOrderItemProductsByIds(
        ids: string[],
        userId: string,
    ): Promise<Product[]> {
        return this.productsRepository
            .createQueryBuilder('Product')
            .leftJoinAndSelect('Product.category', 'Category')
            .leftJoinAndSelect(
                'Category.categoryDiscounts',
                'CategoryDiscount',
                'CategoryDiscount.userId = :userId',
                { userId },
            )
            .where('Product.id IN (:ids)', { ids })
            .getMany();
    }

    /**
     *
     * @param oldCategoryId
     * @param newCategoryId
     * @summary Move all product in old category to new category (must be validated that newCategoryId is exists before using this function)
     */
    async moveProductsToNewCategory(
        oldCategoryId: string,
        newCategoryId: string,
    ) {
        await this.productsRepository.update(
            { categoryId: oldCategoryId },
            { categoryId: newCategoryId },
        );
    }

    async findProductsByIds(ids: string[]): Promise<Product[] | undefined> {
        return this.productsRepository
            .createQueryBuilder('Product')
            .where('Product.id IN (:ids)', { ids })
            .getMany();
    }
}
