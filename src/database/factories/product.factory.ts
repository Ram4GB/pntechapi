import * as Faker from 'faker';

import { Product } from 'products/products.entity';
import { define } from 'typeorm-seeding';

define(Product, (faker: typeof Faker) => {
    faker.locale = 'en';
    const product = new Product({
        name: faker.commerce.productName(),
        price: Number(faker.commerce.price(100000, 500000)),
        generalDescription: faker.lorem.paragraph(),
        technicalDescription: faker.lorem.paragraph(),
        note: faker.lorem.sentence(),
    });

    return product;
});
