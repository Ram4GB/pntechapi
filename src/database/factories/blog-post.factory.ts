import * as Faker from 'faker';

import { BlogPost } from 'blogs/blog-post.entity';
import { BlogPostStatus } from 'blogs/constant/blog-post-status.enum';
import { define } from 'typeorm-seeding';

define(BlogPost, (faker: typeof Faker) => {
    faker.locale = 'en';
    const blogPost = new BlogPost({
        title: faker.name.title(),
        content: faker.lorem.paragraphs(5),
        summary: faker.lorem.words(100),
        status: BlogPostStatus.Show,
    });

    return blogPost;
});
