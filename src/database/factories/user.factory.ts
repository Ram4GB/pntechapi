import * as Faker from 'faker';

import { User } from 'users/user.entity';
import { define } from 'typeorm-seeding';

define(User, (faker: typeof Faker) => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const email = faker.internet.email(firstName, lastName);
    const phone = faker.phone.phoneNumber('0#########');
    //password is 12345678
    const passwordHash =
        '$2a$12$4Qp6QUSJEmEl4C9D28M8bOnoSTV94mx7P1OBOYPyQFrJB6UYNmuSi';

    const user = new User({
        email,
        firstName,
        lastName,
        phone,
        passwordHash,
    });

    return user;
});
