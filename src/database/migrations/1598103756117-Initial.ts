import { MigrationInterface, QueryRunner } from 'typeorm';

export class Initial1598103756117 implements MigrationInterface {
    name = 'Initial1598103756117';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            "CREATE TABLE `banner` (`id` varchar(36) NOT NULL, `title` varchar(255) NULL, `description` varchar(255) NULL, `buttonTitle` varchar(255) NULL, `link` varchar(255) NOT NULL DEFAULT '#', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            'CREATE TABLE `custom_cart_product` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `quantity` int NOT NULL, `createdAt` datetime NOT NULL, `lastUpdatedAt` datetime NOT NULL, `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `custom_order_item` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `quantity` int NOT NULL, `singlePrice` int NOT NULL, `totalPrice` int NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `orderId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `order_item` (`id` varchar(36) NOT NULL, `quantity` int NOT NULL, `singlePrice` int NOT NULL, `totalPrice` int NOT NULL, `discountPercentage` int NULL, `totalPriceAfterDiscount` int NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `orderId` int NULL, `productId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            "CREATE TABLE `order` (`id` int NOT NULL AUTO_INCREMENT, `status` enum ('canceled', 'dealing', 'done', 'exported') NOT NULL DEFAULT 'dealing', `description` text NULL, `totalPrice` int NOT NULL, `totalPriceAfterDiscount` int NOT NULL, `priceCurrency` enum ('vnd', 'usd') NOT NULL DEFAULT 'vnd', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            'CREATE TABLE `chat_message` (`id` varchar(36) NOT NULL, `content` text NOT NULL, `messageType` varchar(255) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `senderId` varchar(255) NOT NULL, `roomId` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            "CREATE TABLE `chat_room` (`id` varchar(36) NOT NULL, `status` enum ('opening', 'closed') NOT NULL DEFAULT 'opening', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `orderId` int NOT NULL, UNIQUE INDEX `IDX_51f43d1a9d120348a2dcc14336` (`orderId`), UNIQUE INDEX `REL_51f43d1a9d120348a2dcc14336` (`orderId`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            'CREATE TABLE `participants` (`userId` varchar(255) NOT NULL, `chatRoomId` varchar(255) NOT NULL, `unreadMessageCount` int NOT NULL DEFAULT 0, `muted` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (`userId`, `chatRoomId`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `role` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_ae4578dcaed5adff96595e6166` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `user_claim` (`id` varchar(36) NOT NULL, `claimType` varchar(255) NOT NULL, `claimValue` varchar(255) NOT NULL, `userId` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            "CREATE TABLE `user` (`id` varchar(36) NOT NULL, `email` varchar(255) NOT NULL, `passwordHash` varchar(255) NOT NULL, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `phone` varchar(255) NOT NULL, `status` enum ('inactive', 'active', 'banned', 'deleted') NOT NULL DEFAULT 'inactive', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            'CREATE TABLE `category_discount` (`id` varchar(36) NOT NULL, `discountPercentage` int NOT NULL, `categoryId` varchar(36) NULL, `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            "CREATE TABLE `category` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `parentId` varchar(255) NULL, `status` enum ('hide', 'show') NOT NULL DEFAULT 'show', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `createdById` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            "CREATE TABLE `product` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `price` int NOT NULL, `quantity` int NOT NULL DEFAULT 0, `generalDescription` text NULL, `technicalDescription` text NULL, `note` text NULL, `status` enum ('hide', 'show') NOT NULL DEFAULT 'show', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `categoryId` varchar(255) NOT NULL, `createdById` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            'CREATE TABLE `cart` (`id` varchar(36) NOT NULL, `quantity` int NOT NULL, `createdAt` datetime NOT NULL, `lastUpdatedAt` datetime NOT NULL, `userId` varchar(36) NULL, `productId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `media` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `uri` varchar(255) NOT NULL, `filemine` varchar(255) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `media_usage` (`mediaId` varchar(255) NOT NULL, `usedById` varchar(255) NOT NULL, `type` varchar(255) NOT NULL, `weight` int NOT NULL DEFAULT 1, PRIMARY KEY (`mediaId`, `usedById`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `user_role` (`userId` varchar(36) NOT NULL, `roleId` varchar(36) NOT NULL, INDEX `IDX_ab40a6f0cd7d3ebfcce082131f` (`userId`), INDEX `IDX_dba55ed826ef26b5b22bd39409` (`roleId`), PRIMARY KEY (`userId`, `roleId`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'ALTER TABLE `custom_cart_product` ADD CONSTRAINT `FK_ef37378f4dce40b9c50d49c8360` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `custom_order_item` ADD CONSTRAINT `FK_9f4534ff71cb5b255bd83fea997` FOREIGN KEY (`orderId`) REFERENCES `order`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `order_item` ADD CONSTRAINT `FK_646bf9ece6f45dbe41c203e06e0` FOREIGN KEY (`orderId`) REFERENCES `order`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `order_item` ADD CONSTRAINT `FK_904370c093ceea4369659a3c810` FOREIGN KEY (`productId`) REFERENCES `product`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `order` ADD CONSTRAINT `FK_caabe91507b3379c7ba73637b84` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `chat_message` ADD CONSTRAINT `FK_a2be22c99b34156574f4e02d0a0` FOREIGN KEY (`senderId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `chat_message` ADD CONSTRAINT `FK_55dfd6d1589749727a7ef2d121f` FOREIGN KEY (`roomId`) REFERENCES `chat_room`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `chat_room` ADD CONSTRAINT `FK_51f43d1a9d120348a2dcc143361` FOREIGN KEY (`orderId`) REFERENCES `order`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `participants` ADD CONSTRAINT `FK_5fc9cddc801b973cd9edcdda42a` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `participants` ADD CONSTRAINT `FK_877279155365e420eae9b92589f` FOREIGN KEY (`chatRoomId`) REFERENCES `chat_room`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `user_claim` ADD CONSTRAINT `FK_de29471e37715dadf6eadd312c3` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `category_discount` ADD CONSTRAINT `FK_20968a86003b8d10c7a1343d100` FOREIGN KEY (`categoryId`) REFERENCES `category`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `category_discount` ADD CONSTRAINT `FK_fc56661c6c1f6eac5ed33b83f99` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `category` ADD CONSTRAINT `FK_d5456fd7e4c4866fec8ada1fa10` FOREIGN KEY (`parentId`) REFERENCES `category`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `category` ADD CONSTRAINT `FK_50c69cdc9b3e7494784a2fa2db4` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `product` ADD CONSTRAINT `FK_806302f2d4da2a0c27eedbf34fe` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `product` ADD CONSTRAINT `FK_ff0c0301a95e517153df97f6812` FOREIGN KEY (`categoryId`) REFERENCES `category`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `cart` ADD CONSTRAINT `FK_756f53ab9466eb52a52619ee019` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `cart` ADD CONSTRAINT `FK_371eb56ecc4104c2644711fa85f` FOREIGN KEY (`productId`) REFERENCES `product`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `media_usage` ADD CONSTRAINT `FK_0f0ba7f38431196d984e332773f` FOREIGN KEY (`mediaId`) REFERENCES `media`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `user_role` ADD CONSTRAINT `FK_ab40a6f0cd7d3ebfcce082131fd` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `user_role` ADD CONSTRAINT `FK_dba55ed826ef26b5b22bd39409b` FOREIGN KEY (`roleId`) REFERENCES `role`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'ALTER TABLE `user_role` DROP FOREIGN KEY `FK_dba55ed826ef26b5b22bd39409b`',
        );
        await queryRunner.query(
            'ALTER TABLE `user_role` DROP FOREIGN KEY `FK_ab40a6f0cd7d3ebfcce082131fd`',
        );
        await queryRunner.query(
            'ALTER TABLE `media_usage` DROP FOREIGN KEY `FK_0f0ba7f38431196d984e332773f`',
        );
        await queryRunner.query(
            'ALTER TABLE `cart` DROP FOREIGN KEY `FK_371eb56ecc4104c2644711fa85f`',
        );
        await queryRunner.query(
            'ALTER TABLE `cart` DROP FOREIGN KEY `FK_756f53ab9466eb52a52619ee019`',
        );
        await queryRunner.query(
            'ALTER TABLE `product` DROP FOREIGN KEY `FK_ff0c0301a95e517153df97f6812`',
        );
        await queryRunner.query(
            'ALTER TABLE `product` DROP FOREIGN KEY `FK_806302f2d4da2a0c27eedbf34fe`',
        );
        await queryRunner.query(
            'ALTER TABLE `category` DROP FOREIGN KEY `FK_50c69cdc9b3e7494784a2fa2db4`',
        );
        await queryRunner.query(
            'ALTER TABLE `category` DROP FOREIGN KEY `FK_d5456fd7e4c4866fec8ada1fa10`',
        );
        await queryRunner.query(
            'ALTER TABLE `category_discount` DROP FOREIGN KEY `FK_fc56661c6c1f6eac5ed33b83f99`',
        );
        await queryRunner.query(
            'ALTER TABLE `category_discount` DROP FOREIGN KEY `FK_20968a86003b8d10c7a1343d100`',
        );
        await queryRunner.query(
            'ALTER TABLE `user_claim` DROP FOREIGN KEY `FK_de29471e37715dadf6eadd312c3`',
        );
        await queryRunner.query(
            'ALTER TABLE `participants` DROP FOREIGN KEY `FK_877279155365e420eae9b92589f`',
        );
        await queryRunner.query(
            'ALTER TABLE `participants` DROP FOREIGN KEY `FK_5fc9cddc801b973cd9edcdda42a`',
        );
        await queryRunner.query(
            'ALTER TABLE `chat_room` DROP FOREIGN KEY `FK_51f43d1a9d120348a2dcc143361`',
        );
        await queryRunner.query(
            'ALTER TABLE `chat_message` DROP FOREIGN KEY `FK_55dfd6d1589749727a7ef2d121f`',
        );
        await queryRunner.query(
            'ALTER TABLE `chat_message` DROP FOREIGN KEY `FK_a2be22c99b34156574f4e02d0a0`',
        );
        await queryRunner.query(
            'ALTER TABLE `order` DROP FOREIGN KEY `FK_caabe91507b3379c7ba73637b84`',
        );
        await queryRunner.query(
            'ALTER TABLE `order_item` DROP FOREIGN KEY `FK_904370c093ceea4369659a3c810`',
        );
        await queryRunner.query(
            'ALTER TABLE `order_item` DROP FOREIGN KEY `FK_646bf9ece6f45dbe41c203e06e0`',
        );
        await queryRunner.query(
            'ALTER TABLE `custom_order_item` DROP FOREIGN KEY `FK_9f4534ff71cb5b255bd83fea997`',
        );
        await queryRunner.query(
            'ALTER TABLE `custom_cart_product` DROP FOREIGN KEY `FK_ef37378f4dce40b9c50d49c8360`',
        );
        await queryRunner.query(
            'DROP INDEX `IDX_dba55ed826ef26b5b22bd39409` ON `user_role`',
        );
        await queryRunner.query(
            'DROP INDEX `IDX_ab40a6f0cd7d3ebfcce082131f` ON `user_role`',
        );
        await queryRunner.query('DROP TABLE `user_role`');
        await queryRunner.query('DROP TABLE `media_usage`');
        await queryRunner.query('DROP TABLE `media`');
        await queryRunner.query('DROP TABLE `cart`');
        await queryRunner.query('DROP TABLE `product`');
        await queryRunner.query('DROP TABLE `category`');
        await queryRunner.query('DROP TABLE `category_discount`');
        await queryRunner.query(
            'DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`',
        );
        await queryRunner.query('DROP TABLE `user`');
        await queryRunner.query('DROP TABLE `user_claim`');
        await queryRunner.query(
            'DROP INDEX `IDX_ae4578dcaed5adff96595e6166` ON `role`',
        );
        await queryRunner.query('DROP TABLE `role`');
        await queryRunner.query('DROP TABLE `participants`');
        await queryRunner.query(
            'DROP INDEX `REL_51f43d1a9d120348a2dcc14336` ON `chat_room`',
        );
        await queryRunner.query(
            'DROP INDEX `IDX_51f43d1a9d120348a2dcc14336` ON `chat_room`',
        );
        await queryRunner.query('DROP TABLE `chat_room`');
        await queryRunner.query('DROP TABLE `chat_message`');
        await queryRunner.query('DROP TABLE `order`');
        await queryRunner.query('DROP TABLE `order_item`');
        await queryRunner.query('DROP TABLE `custom_order_item`');
        await queryRunner.query('DROP TABLE `custom_cart_product`');
        await queryRunner.query('DROP TABLE `banner`');
    }
}
