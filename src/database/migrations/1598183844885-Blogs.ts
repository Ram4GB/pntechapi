import { MigrationInterface, QueryRunner } from 'typeorm';

export class Blogs1598183844885 implements MigrationInterface {
    name = 'Blogs1598183844885';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            "CREATE TABLE `blog_post` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `thumbnail` varchar(255) NULL, `content` text NOT NULL, `status` enum ('hide', 'show') NOT NULL DEFAULT 'show', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `categoryId` varchar(255) NOT NULL, `createdById` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            "CREATE TABLE `blog_category` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `status` enum ('hide', 'show') NOT NULL DEFAULT 'show', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB",
        );
        await queryRunner.query(
            'ALTER TABLE `blog_post` ADD CONSTRAINT `FK_a1ba67336686c3de21bbed2d7cc` FOREIGN KEY (`categoryId`) REFERENCES `blog_category`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `blog_post` ADD CONSTRAINT `FK_9cd75ef9a7cab7d9fcfdf9077cd` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'ALTER TABLE `blog_post` DROP FOREIGN KEY `FK_9cd75ef9a7cab7d9fcfdf9077cd`',
        );
        await queryRunner.query(
            'ALTER TABLE `blog_post` DROP FOREIGN KEY `FK_a1ba67336686c3de21bbed2d7cc`',
        );
        await queryRunner.query('DROP TABLE `blog_category`');
        await queryRunner.query('DROP TABLE `blog_post`');
    }
}
