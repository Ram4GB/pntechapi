import { MigrationInterface, QueryRunner } from 'typeorm';

export class BlogSummary1598256991807 implements MigrationInterface {
    name = 'BlogSummary1598256991807';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'ALTER TABLE `blog_post` ADD `summary` varchar(255) NULL',
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'ALTER TABLE `blog_post` DROP COLUMN `summary`',
        );
    }
}
