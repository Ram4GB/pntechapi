import { MigrationInterface, QueryRunner } from 'typeorm';

export class productImports1598207856286 implements MigrationInterface {
    name = 'productImports1598207856286';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'CREATE TABLE `product_import` (`id` varchar(36) NOT NULL, `description` text NULL, `totalPrice` int NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `createdById` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'CREATE TABLE `product_import_detail` (`id` varchar(36) NOT NULL, `quantity` int NOT NULL, `singlePrice` int NOT NULL, `totalPrice` int NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `productImportId` varchar(36) NULL, `productId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
        );
        await queryRunner.query(
            'ALTER TABLE `product_import` ADD CONSTRAINT `FK_6078b9debde752dfcebb1189abe` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `product_import_detail` ADD CONSTRAINT `FK_ca2c851ea9fc670b2a7560b16f9` FOREIGN KEY (`productImportId`) REFERENCES `product_import`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
        await queryRunner.query(
            'ALTER TABLE `product_import_detail` ADD CONSTRAINT `FK_cdc6b58b90c5048cae51f2b2ce0` FOREIGN KEY (`productId`) REFERENCES `product`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'ALTER TABLE `product_import_detail` DROP FOREIGN KEY `FK_cdc6b58b90c5048cae51f2b2ce0`',
        );
        await queryRunner.query(
            'ALTER TABLE `product_import_detail` DROP FOREIGN KEY `FK_ca2c851ea9fc670b2a7560b16f9`',
        );
        await queryRunner.query(
            'ALTER TABLE `product_import` DROP FOREIGN KEY `FK_6078b9debde752dfcebb1189abe`',
        );
        await queryRunner.query('DROP TABLE `product_import_detail`');
        await queryRunner.query('DROP TABLE `product_import`');
    }
}
