import { Factory, Seeder } from 'typeorm-seeding';

import { Category } from 'categories/category.entity';
import { Connection } from 'typeorm';
import { User } from 'users/user.entity';
import { plainToClass } from 'class-transformer';

export default class CreateCategories implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        // get user to link to who created category
        const categoriesRepository = connection.getRepository(Category);
        const usersRepository = connection.getRepository(User);
        const yungAdminUser = await usersRepository.findOne({
            email: 'yungadmin@gmail.com',
        });

        // Seed paremt categories
        const categoryNames = [
            { name: 'Cảm biến ánh sáng' },
            { name: 'Cảm biến chất khí' },
            { name: 'Cảm biến dòng điện' },
            { name: 'Cảm biến khói' },
            { name: 'Cảm biến không dây' },
            { name: 'Cảm biến áp suất' },
            { name: 'Cảm biến mức' },
            { name: 'Cảm biến nhiệt độ' },
            { name: 'Cảm biến độ ẩm' },
            { name: 'Cảm biến nhiệt độ, độ ẩm' },
        ];

        const categoryEntities = plainToClass(Category, categoryNames);

        // attach user to category before save
        categoryEntities.forEach(category => {
            category.createdBy = yungAdminUser;
        });

        await categoriesRepository.save(categoryEntities);

        // Seed child categories
        const categoriesPromise = [
            categoriesRepository.findOne({
                name: 'Cảm biến dòng điện',
            }),
            categoriesRepository.findOne({
                name: 'Cảm biến nhiệt độ',
            }),
        ];

        const categories = await Promise.all(categoriesPromise);

        const childEntities = [
            new Category({
                name: 'Cảm biến dòng điện 1 chiều',
                createdBy: yungAdminUser,
                parent: categories[0],
            }),
            new Category({
                name: 'Cảm biến dòng điện 2 chiều',
                createdBy: yungAdminUser,
                parent: categories[0],
            }),
            new Category({
                name: 'Cảm biến nhiệt độ 1',
                createdBy: yungAdminUser,
                parent: categories[1],
            }),
            new Category({
                name: 'Cảm biến nhiệt độ 2',
                createdBy: yungAdminUser,
                parent: categories[1],
            }),
        ];

        await categoriesRepository.save(childEntities);
    }
}
