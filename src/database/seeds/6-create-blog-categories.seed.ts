import { Factory, Seeder } from 'typeorm-seeding';

import { BlogCategory } from 'blogs/blog-category.entity';
import { Connection } from 'typeorm';
import { plainToClass } from 'class-transformer';

export default class CreateBlogCategories implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const categoriesRepository = connection.getRepository(BlogCategory);

        // Seed paremt categories
        const categoryNames = [
            { name: 'Danh mục bài viết 1' },
            { name: 'Danh mục bài viết 2' },
            { name: 'Danh mục bài viết 3' },
            { name: 'Danh mục bài viết 4' },
            { name: 'Danh mục bài viết 5' },
        ];

        const categoryEntities = plainToClass(BlogCategory, categoryNames);

        await categoriesRepository.save(categoryEntities);
    }
}
