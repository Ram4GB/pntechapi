import { Factory, Seeder } from 'typeorm-seeding';

import { BlogCategory } from 'blogs/blog-category.entity';
import { BlogPost } from 'blogs/blog-post.entity';
import { Connection } from 'typeorm';
import { User } from 'users/user.entity';

export default class CreateBlogPosts implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const usersRepository = connection.getRepository(User);
        const categoriesRepository = connection.getRepository(BlogCategory);

        const yungAdminUser = await usersRepository.findOne({
            email: 'yungadmin@gmail.com',
        });

        const tonyAdminUser = await usersRepository.findOne({
            email: 'tonyadmin@gmail.com',
        });

        const categoriesPromise = [
            categoriesRepository.findOne({
                name: 'Danh mục bài viết 1',
            }),
            categoriesRepository.findOne({
                name: 'Danh mục bài viết 2',
            }),
            categoriesRepository.findOne({
                name: 'Danh mục bài viết 3',
            }),
            categoriesRepository.findOne({
                name: 'Danh mục bài viết 4',
            }),
            categoriesRepository.findOne({
                name: 'Danh mục bài viết 5',
            }),
        ];

        const categories = await Promise.all(categoriesPromise);

        // Create posts in category 'Danh mục bài viết 1'
        await factory(BlogPost)().createMany(10, {
            createdBy: yungAdminUser,
            category: categories[0],
        });

        // Create posts in category 'Danh mục bài viết 2'
        await factory(BlogPost)().createMany(10, {
            createdBy: yungAdminUser,
            category: categories[1],
        });

        // Create posts in category 'Danh mục bài viết 3'
        await factory(BlogPost)().createMany(10, {
            createdBy: yungAdminUser,
            category: categories[2],
        });

        // Create posts in category 'Danh mục bài viết 4'
        await factory(BlogPost)().createMany(10, {
            createdBy: tonyAdminUser,
            category: categories[3],
        });

        // Create posts in category 'Danh mục bài viết 5'
        await factory(BlogPost)().createMany(10, {
            createdBy: tonyAdminUser,
            category: categories[4],
        });
    }
}
