import { Factory, Seeder } from 'typeorm-seeding';

import { Connection } from 'typeorm';
import { Role } from 'users/role.entity';
import { Roles } from 'shared/constants/roles';
import { User } from 'users/user.entity';
import { UserStatus } from 'users/constant/userstatus';

export default class CreateUsers implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        // Get role from database
        const rolesRepository = connection.getRepository(Role);
        const superAdminRole = await rolesRepository.findOne({
            name: Roles.SuperAdmin,
        });
        const adminRole = await rolesRepository.findOne({
            name: Roles.Admin,
        });
        const sellerRole = await rolesRepository.findOne({
            name: Roles.Seller,
        });
        const customerRole = await rolesRepository.findOne({
            name: Roles.Customer,
        });

        // Create supper admin
        await factory(User)().create({
            firstName: 'Super Admin',
            lastName: 'Lil',
            email: 'lilsuperadmin@gmail.com',
            status: UserStatus.Active,
            roles: [superAdminRole],
        });

        // Create admin
        await factory(User)().create({
            firstName: 'Admin',
            lastName: 'Yung',
            email: 'yungadmin@gmail.com',
            status: UserStatus.Active,
            roles: [adminRole],
        });

        await factory(User)().create({
            firstName: 'Admin',
            lastName: 'Tony',
            email: 'tonyadmin@gmail.com',
            status: UserStatus.Active,
            roles: [adminRole],
        });

        // Create seller
        await factory(User)().create({
            firstName: 'Seller',
            lastName: 'Big',
            email: 'bigseller@gmail.com',
            status: UserStatus.Active,
            roles: [sellerRole],
        });
        await factory(User)().create({
            firstName: 'Seller',
            lastName: 'Lil',
            email: 'lilseller@gmail.com',
            status: UserStatus.Active,
            roles: [sellerRole],
        });

        // Create Customer
        await factory(User)().create({
            firstName: 'Unique',
            lastName: 'West',
            email: 'Unique_West68@gmail.com',
            status: UserStatus.Active,
            roles: [customerRole],
        });

        await factory(User)().createMany(20, {
            status: UserStatus.Active,
            roles: [customerRole],
        });
    }
}
