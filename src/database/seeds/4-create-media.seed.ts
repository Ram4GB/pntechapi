import * as fs from 'fs';
import * as http from 'http';
import * as path from 'path';

import { Factory, Seeder } from 'typeorm-seeding';

import { Connection } from 'typeorm';
import { Media } from 'media/media.entity';

export default class CreateMedia implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        // get user to link to who created category
        const mediaRepository = connection.getRepository(Media);

        const dest = path.join(
            __dirname,
            '..',
            '..',
            '..',
            'uploads',
            'media',
            'seed.jpg',
        );
        const imageIsExisted = fs.existsSync(dest);
        if (!imageIsExisted) {
            const file = fs.createWriteStream(dest);
            http.get(
                'http://pntech.vn/images/stories/virtuemart/product/CR-DC-5A_01.jpg',
                function(response) {
                    response.pipe(file);
                    file.on('finish', function() {
                        file.close();
                    });
                },
            ).on('error', function() {
                // Handle errors
                fs.unlinkSync(dest); // Delete the file
            });
        }

        await mediaRepository.save(
            new Media({
                name: 'seed.jpg',
                uri: 'uploads/media/seed.jpg',
                filemine: 'image/jpeg',
            }),
        );
    }
}
