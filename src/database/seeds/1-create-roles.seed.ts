import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Role } from 'users/role.entity';
import { Roles } from 'shared/constants/roles';

export default class CreateRoles implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const superAdminRole = new Role();
        superAdminRole.name = Roles.SuperAdmin;

        const adminRole = new Role();
        adminRole.name = Roles.Admin;

        const sellerRole = new Role();
        sellerRole.name = Roles.Seller;

        const customerRole = new Role();
        customerRole.name = Roles.Customer;

        const rolesRepository = connection.getRepository(Role);
        await rolesRepository.save([
            superAdminRole,
            adminRole,
            sellerRole,
            customerRole,
        ]);
    }
}
