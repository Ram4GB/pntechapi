import { Factory, Seeder } from 'typeorm-seeding';

import { Category } from 'categories/category.entity';
import { Connection } from 'typeorm';
import { Media } from 'media/media.entity';
import { MediaUsage } from 'media/media-usage.entity';
import { Product } from 'products/products.entity';
import { User } from 'users/user.entity';

export default class CreateProducts implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const usersRepository = connection.getRepository(User);
        const categoriesRepository = connection.getRepository(Category);
        const mediaRepository = connection.getRepository(Media);
        const mediaUsageRepository = connection.getRepository(MediaUsage);

        const yungAdminUser = await usersRepository.findOne({
            email: 'yungadmin@gmail.com',
        });

        const categoriesPromise = [
            categoriesRepository.findOne({
                name: 'Cảm biến ánh sáng',
            }),
            categoriesRepository.findOne({
                name: 'Cảm biến chất khí',
            }),
            categoriesRepository.findOne({
                name: 'Cảm biến dòng điện 1 chiều',
            }),
            categoriesRepository.findOne({
                name: 'Cảm biến dòng điện 2 chiều',
            }),
            categoriesRepository.findOne({
                name: 'Cảm biến nhiệt độ 1',
            }),
            categoriesRepository.findOne({
                name: 'Cảm biến nhiệt độ 2',
            }),
        ];

        const categories = await Promise.all(categoriesPromise);

        // Create product in category 'Cảm biến ánh sáng'
        const products1 = await factory(Product)().createMany(20, {
            createdBy: yungAdminUser,
            category: categories[0],
        });

        // Create product in category 'Cảm biến chất khí'
        const products2 = await factory(Product)().createMany(20, {
            createdBy: yungAdminUser,
            category: categories[1],
        });

        // Create product in category 'Cảm biến dòng điện 1 chiều'
        const products3 = await factory(Product)().createMany(20, {
            createdBy: yungAdminUser,
            category: categories[2],
        });

        // Create product in category 'Cảm biến dòng điện 2 chiều'
        const products4 = await factory(Product)().createMany(20, {
            createdBy: yungAdminUser,
            category: categories[3],
        });

        // Create product in category 'Cảm biến nhiệt độ 1'
        const products5 = await factory(Product)().createMany(20, {
            createdBy: yungAdminUser,
            category: categories[4],
        });

        // Create product in category 'Cảm biến nhiệt độ 2'
        const products6 = await factory(Product)().createMany(20, {
            createdBy: yungAdminUser,
            category: categories[5],
        });

        // Seed product Image
        const media = await mediaRepository.findOne({
            uri: 'uploads/media/seed.jpg',
        });

        const mediaUsage = [];

        products1.forEach(product => {
            mediaUsage.push(
                new MediaUsage({
                    usedById: product.id,
                    mediaId: media.id,
                    type: 'product',
                    weight: 1,
                }),
            );
        });

        products2.forEach(product => {
            mediaUsage.push(
                new MediaUsage({
                    usedById: product.id,
                    mediaId: media.id,
                    type: 'product',
                    weight: 1,
                }),
            );
        });

        products3.forEach(product => {
            mediaUsage.push(
                new MediaUsage({
                    usedById: product.id,
                    mediaId: media.id,
                    type: 'product',
                    weight: 1,
                }),
            );
        });

        products4.forEach(product => {
            mediaUsage.push(
                new MediaUsage({
                    usedById: product.id,
                    mediaId: media.id,
                    type: 'product',
                    weight: 1,
                }),
            );
        });

        products5.forEach(product => {
            mediaUsage.push(
                new MediaUsage({
                    usedById: product.id,
                    mediaId: media.id,
                    type: 'product',
                    weight: 1,
                }),
            );
        });

        products6.forEach(product => {
            mediaUsage.push(
                new MediaUsage({
                    usedById: product.id,
                    mediaId: media.id,
                    type: 'product',
                    weight: 1,
                }),
            );
        });

        await mediaUsageRepository.save(mediaUsage);
    }
}
