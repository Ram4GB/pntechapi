import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Inject,
    Param,
    Post,
    Put,
    Query,
    forwardRef,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { CategoriesService } from './categories.service';
import { Category } from './category.entity';
import { CategoryCreateDTO } from './dto/category-create.dto';
import { CategoryDiscountCreateDTO } from './dto/category-discount-create.dto';
import { CategoryDiscountPaginateQuery } from './dto/category-discount-query.dto';
import { CategoryDiscountUpateDTO } from './dto/category-discount-update.dto';
import { CategoryDiscountsService } from './category-discounts.service';
import { CategoryPaginateQuery } from './dto/category.query';
import { CategoryUpdateDTO } from './dto/category-update.dto';
import { MediaService } from 'media/media.service';
import { PagedList } from 'shared/helpers/pagination';
import { ReqUser } from 'shared/decorators/request-user.decorator';
import { RequestUser } from 'auth/dto/req-user.interface';
import { Roles } from 'shared/constants/roles';
import { ShowCategoryPaginateQuery } from './dto/show-category.query';
import { User } from 'users/user.entity';

@Controller('categories')
@ApiTags('Categories')
export class CategoriesController {
    constructor(
        private readonly categoriesService: CategoriesService,
        private readonly mediaService: MediaService,
        @Inject(forwardRef(() => CategoryDiscountsService))
        private readonly categoryDiscountsService: CategoryDiscountsService,
    ) {}

    @Authorize(Roles.Admin)
    @Post()
    @HttpCode(201)
    async create(
        @Body() categoryInfo: CategoryCreateDTO,
        @ReqUser() requestUser: RequestUser,
    ): Promise<void> {
        const category: Category = new Category({
            ...categoryInfo,
            createdBy: new User({ id: requestUser.id }),
        });

        await this.categoriesService.create(category);
    }

    @Get(':id')
    async findById(@Param('id') id: string): Promise<Category> {
        return this.categoriesService.findFullInfoById(id);
    }

    @Authorize(Roles.Anonymous)
    @Get()
    async findAll(
        @Query() queryFull: CategoryPaginateQuery,
        @Query() query: ShowCategoryPaginateQuery,
        @ReqUser() user: RequestUser,
    ): Promise<PagedList<Category>> {
        if (user.role === Roles.Admin) {
            return this.categoriesService.listCategoriesPaginated(queryFull);
        }
        return this.categoriesService.listOnlyShowCategoriesPaginated(query);
    }

    @Authorize(Roles.Admin)
    @Put(':id')
    @HttpCode(200)
    async update(
        @Param('id') id: string,
        @Body() updateInfo: CategoryUpdateDTO,
    ): Promise<void> {
        await this.categoriesService.update(id, updateInfo);
    }

    @Authorize(Roles.Admin)
    @Delete(':id')
    @HttpCode(200)
    async delete(@Param('id') id: string): Promise<void> {
        await this.categoriesService.delete(id);
    }

    @Authorize(Roles.Admin)
    @Post('categoryDiscounts')
    @HttpCode(201)
    async createCategoryDiscount(
        @Body() createInfo: CategoryDiscountCreateDTO,
    ): Promise<void> {
        await this.categoryDiscountsService.createCategoryDiscountAndUpdateAllRelatedOrder(
            createInfo,
        );
    }

    @Authorize(Roles.Admin)
    @Get('categoryDiscounts/users')
    @HttpCode(200)
    async getCategoryDiscountOfUser(
        @Query() query: CategoryDiscountPaginateQuery,
    ): Promise<PagedList<Category>> {
        return await this.categoriesService.listCategoryDiscountOfUser(query);
    }

    @Authorize(Roles.Admin)
    @Put('categoryDiscounts/:id')
    @HttpCode(201)
    async updateCategoryDiscount(
        @Param('id') id: string,
        @Body() updateInfo: CategoryDiscountUpateDTO,
    ): Promise<void> {
        await this.categoryDiscountsService.updateCategoryDiscountAndUpdateAllRelatedOrder(
            id,
            updateInfo,
        );
    }
}
