import * as _ from 'lodash';

import {
    BadRequestException,
    Inject,
    Injectable,
    NotFoundException,
    forwardRef,
} from '@nestjs/common';
import { Connection, QueryRunner, Repository } from 'typeorm';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';

import { CategoriesService } from './categories.service';
import { Category } from './category.entity';
import { CategoryDiscount } from 'categorydiscounts/categorydiscount.entity';
import { CategoryDiscountCreateDTO } from './dto/category-discount-create.dto';
import { CategoryDiscountUpateDTO } from './dto/category-discount-update.dto';
import { Order } from 'orders/entities/order.entity';
import { OrderItem } from 'orders/entities/order-item.entity';
import { OrderItemsService } from 'orders/services/order-items.service';
import { OrdersService } from 'orders/services/orders.service';
import { PriceCalculateHelper } from 'orders/helpers/price-calculate.helper';
import { Roles } from 'shared/constants/roles';
import { User } from 'users/user.entity';
import { UsersService } from 'users/users.service';

@Injectable()
export class CategoryDiscountsService {
    constructor(
        @InjectRepository(CategoryDiscount)
        private categoryDiscountsRepository: Repository<CategoryDiscount>,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => CategoriesService))
        private readonly categoriesService: CategoriesService,
        @Inject(forwardRef(() => OrdersService))
        private readonly ordersService: OrdersService,
        @Inject(forwardRef(() => OrderItemsService))
        private readonly orderItemsService: OrderItemsService,
        private readonly priceCalculateHelper: PriceCalculateHelper,
        @InjectConnection()
        private connection: Connection,
    ) {}

    /* -------------------------------------
          CREATE CATEGORY DISCOUNT
      ------------------------------------- */
    async createCategoryDiscountAndUpdateAllRelatedOrder(
        createInfo: CategoryDiscountCreateDTO,
    ): Promise<void> {
        await this.validateExistedCategoryDiscount(
            createInfo.userId,
            createInfo.categoryId,
        );

        const customerUser = await this.getValidateCustomerWhenCreateCategoryDiscount(
            createInfo.userId,
        );

        const category = await this.getValidateCategoryWhenCreateCategoryDiscount(
            createInfo.categoryId,
        );

        const categoryDiscount = new CategoryDiscount({
            discountPercentage: createInfo.discountPercentage,
            category,
            user: customerUser,
        });

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            await queryRunner.manager.save(CategoryDiscount, categoryDiscount);
            await this.updateRelatedOrderWhenCategoryDiscountChange(
                queryRunner,
                category.id,
                customerUser.id,
                categoryDiscount.discountPercentage,
            );
            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async findCategoryDiscountByUserIdAndCategoryId(
        userId: string,
        categoryId: string,
    ) {
        return this.categoryDiscountsRepository
            .createQueryBuilder('CategoryDiscount')
            .where('CategoryDiscount.userId = :userId', { userId })
            .where('CategoryDiscount.categoryId = :categoryId', { categoryId })
            .getOne();
    }

    async validateExistedCategoryDiscount(userId: string, categoryId: string) {
        /** Check existed category discount */
        const existedCategoryDiscount = await this.findCategoryDiscountByUserIdAndCategoryId(
            userId,
            categoryId,
        );
        if (existedCategoryDiscount) {
            throw new BadRequestException(
                'Danh mục này của người dùng đã có chiết khấu',
            );
        }
    }

    async getValidateCustomerWhenCreateCategoryDiscount(
        userId: string,
    ): Promise<User> {
        const customerUser = await this.usersService.findById(userId);
        if (!customerUser) {
            throw new NotFoundException();
        }
        if (customerUser.roles[0].name !== Roles.Customer) {
            throw new BadRequestException('User must be customer');
        }

        return customerUser;
    }

    async getValidateCategoryWhenCreateCategoryDiscount(
        categoryId: string,
    ): Promise<Category> {
        const category = await this.categoriesService.findOneById(categoryId);
        if (!category) {
            throw new NotFoundException();
        }

        return category;
    }

    async updateRelatedOrderWhenCategoryDiscountChange(
        queryRunner: QueryRunner,
        categoryId: string,
        customerUserId: string,
        discountPercentage: number,
    ): Promise<void> {
        let orders = await this.ordersService.getAllDealingOrderOfUserToWhenCategoryDiscountChange(
            customerUserId,
            categoryId,
        );

        /** Update discount percentage and price of order items in each order */
        orders = this.updateOrderItemsOfOrderWhenCategoryDiscountChange(
            orders,
            discountPercentage,
        );

        /** derive order items from order to save to db */
        let orderItems: OrderItem[] = [];
        orders.forEach(order => {
            orderItems = [...orderItems, ...order.orderItems];
            delete order.orderItems;
        });

        /** Save to db */
        if (!!orders && orders.length > 0) {
            await queryRunner.manager.save(Order, orders);
        }
        if (!!orderItems && orderItems.length > 0) {
            await queryRunner.manager.save(OrderItem, orderItems);
        }
    }

    updateOrderItemsOfOrderWhenCategoryDiscountChange(
        orders: Order[],
        discountPercentage: number,
    ): Order[] {
        /** Update discount percentage and price of order items in each order */
        orders = orders.map(order => {
            /** Save old version of order items to update order price */
            const oldOrderItems = _.cloneDeep(order.orderItems);

            /** Update disocunt percentage of each order item */
            order.orderItems = order.orderItems.map(orderItem => {
                orderItem.discountPercentage = discountPercentage;

                return orderItem;
            });
            order.orderItems = this.priceCalculateHelper.calculatePriceOfOrderItemList(
                order.orderItems,
            );

            /** re-calculate order price after all order items are changed */
            order = this.priceCalculateHelper.calculateOrderPriceBaseOnChangedOrderItems(
                order,
                oldOrderItems,
                order.orderItems,
            );

            return order;
        });

        return orders;
    }

    /* -------------------------------------
          UPDATE CATEGORY DISCOUNT
      ------------------------------------- */
    async updateCategoryDiscountAndUpdateAllRelatedOrder(
        id: string,
        updateInfo: CategoryDiscountUpateDTO,
    ): Promise<void> {
        const categoryDiscount = await this.findById(id);
        if (!categoryDiscount) {
            throw new NotFoundException();
        }

        categoryDiscount.discountPercentage = updateInfo.discountPercentage;

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            await queryRunner.manager.save(CategoryDiscount, categoryDiscount);
            await this.updateRelatedOrderWhenCategoryDiscountChange(
                queryRunner,
                categoryDiscount.category.id,
                categoryDiscount.user.id,
                categoryDiscount.discountPercentage,
            );
            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    async findById(id: string): Promise<CategoryDiscount> {
        return this.categoryDiscountsRepository.findOne({
            where: {
                id,
            },
            relations: ['user', 'category'],
        });
    }
}
