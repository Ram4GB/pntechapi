import { Module, forwardRef } from '@nestjs/common';

import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { Category } from './category.entity';
import { CategoryDiscount } from 'categorydiscounts/categorydiscount.entity';
import { CategoryDiscountsService } from './category-discounts.service';
import { MediaModule } from 'media/media.module';
import { OrdersModule } from 'orders/orders.module';
import { PaginationModule } from 'shared/pagination/pagination.module';
import { ProductsModule } from 'products/products.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'users/users.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([Category, CategoryDiscount]),
        UsersModule,
        PaginationModule,
        forwardRef(() => ProductsModule),
        forwardRef(() => OrdersModule),
        MediaModule,
    ],
    providers: [CategoriesService, CategoryDiscountsService],
    controllers: [CategoriesController],
    exports: [CategoriesService, CategoryDiscountsService],
})
export class CategoriesModule {}
