import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { CategoryDiscount } from 'categorydiscounts/categorydiscount.entity';
import { CategoryStatus } from './constant/category.enum';
import { Expose } from 'class-transformer';
import { Product } from 'products/products.entity';
import { User } from 'users/user.entity';

@Entity()
export class Category {
    constructor(partial: Partial<Category>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Column()
    name: string;

    @Column({ nullable: true })
    parentId: string;

    @Column({
        type: 'enum',
        enum: CategoryStatus,
        default: CategoryStatus.Show,
    })
    status: CategoryStatus;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(
        type => Category,
        category => category.children,
    )
    parent: Category;

    @OneToMany(
        type => Category,
        category => category.parent,
    )
    children: Category[];

    @ManyToOne(
        type => User,
        user => user.categories,
    )
    createdBy: User;

    @Expose()
    @OneToMany(
        type => CategoryDiscount,
        categoryDiscount => categoryDiscount.category,
    )
    categoryDiscounts: CategoryDiscount[];

    @OneToMany(
        type => Product,
        product => product.category,
    )
    products: Product[];
}
