import {
    BadRequestException,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { Brackets, Repository, SelectQueryBuilder } from 'typeorm';

import { Category } from './category.entity';
import { CategoryDiscountPaginateQuery } from './dto/category-discount-query.dto';
import { CategoryPaginateQuery } from './dto/category.query';
import { CategoryStatus } from './constant/category.enum';
import { CategoryUpdateDTO } from './dto/category-update.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductsService } from 'products/products.service';
import { ShowCategoryPaginateQuery } from './dto/show-category.query';
import { paginate } from 'shared/helpers/pagination';

@Injectable()
export class CategoriesService {
    constructor(
        @InjectRepository(Category)
        private categoriesRepository: Repository<Category>,
        private productsService: ProductsService,
    ) {}

    async listOnlyShowCategoriesPaginated(query: ShowCategoryPaginateQuery) {
        // Custom filter
        return paginate<Category>(
            this.categoriesRepository,
            query,
            (builder: SelectQueryBuilder<Category>) => {
                builder
                    .leftJoinAndSelect('Category.children', 'children')
                    .where(
                        `Category.parentId IS NULL AND Category.status = '${CategoryStatus.Show}'`,
                    );

                if (query && query.filter && query.filter.name) {
                    builder.andWhere(
                        new Brackets(qb =>
                            qb
                                .where(`Category.name like :name`, {
                                    name: `%${query.filter.name}%`,
                                })
                                .orWhere(`children.name like :childrenName`, {
                                    childrenName: `%${query.filter.name}%`,
                                }),
                        ),
                    );
                }
            },
        );
    }

    async listCategoriesPaginated(query: CategoryPaginateQuery) {
        // Custom filter
        return paginate<Category>(
            this.categoriesRepository,
            query,
            (builder: SelectQueryBuilder<Category>) => {
                builder
                    .leftJoinAndSelect('Category.children', 'children')
                    .where(`Category.parentId IS NULL `);

                if (query && query.filter && query.filter.status) {
                    builder.andWhere(`Category.status = :status`, {
                        status: `%${query.filter.status}%`,
                    });
                }
                if (query && query.filter && query.filter.name) {
                    builder.andWhere(
                        new Brackets(qb =>
                            qb
                                .where(`Category.name like :name`, {
                                    name: `%${query.filter.name}%`,
                                })
                                .orWhere(`children.name like :childrenName`, {
                                    childrenName: `%${query.filter.name}%`,
                                }),
                        ),
                    );
                }
            },
        );
    }

    async findFullInfoById(id: string): Promise<Category> {
        const category = await this.categoriesRepository
            .createQueryBuilder('Category')
            .leftJoinAndSelect('Category.children', 'children')
            .where('Category.id = :id', { id })
            .getOne();

        return category;
    }

    async findOneById(id: string): Promise<Category> {
        return this.categoriesRepository.findOne(id, {
            relations: ['children'],
        });
    }

    private async validateParentId(parentId: string) {
        const parentCategory = await this.categoriesRepository.findOne(
            parentId,
        );

        // Phải là category cha (category chỉ 2 cấp)
        if (!parentCategory || parentCategory.parentId) {
            throw new NotFoundException('Parent category not found');
        }
    }

    private async moveProductOfParentIfExist(
        parentId: string,
        childrenId: string,
    ) {
        const parentCategory = await this.categoriesRepository.findOne({
            relations: ['products'],
            where: {
                id: parentId,
            },
        });

        // Nếu parent category chứ product, dời qua category con
        if (
            parentCategory &&
            parentCategory.products &&
            parentCategory.products.length
        ) {
            await this.productsService.moveProductsToNewCategory(
                parentId,
                childrenId,
            );
            console.log(
                'HQ: CategoriesService -> parentCategory.products.',
                parentCategory.products,
            );
        }
    }

    async create(category: Category): Promise<Category> {
        if (category.parentId) {
            await this.validateParentId(category.parentId);
        }

        const created = await this.categoriesRepository.save(category);

        if (category.parentId) {
            await this.moveProductOfParentIfExist(
                category.parentId,
                created.id,
            );
        }

        return created;
    }

    async update(id: string, updateInfo: CategoryUpdateDTO): Promise<void> {
        const category = await this.categoriesRepository.findOne(id);

        if (!category) {
            throw new NotFoundException('Category not found');
        }

        await this.categoriesRepository.update(category.id, {
            ...updateInfo,
        });
    }

    async delete(id: string): Promise<void> {
        const category = await this.categoriesRepository.findOne(id, {
            relations: ['products', 'children'],
        });

        if (!category) {
            throw new NotFoundException('Category not found');
        }
        if (category.children && category.children.length > 0) {
            throw new BadRequestException(
                'Bạn không thể xóa, vì danh mục này đang tồn danh mục con',
            );
        }
        if (category.products && category.products.length > 0) {
            throw new BadRequestException(
                'Bạn không thể xóa, vì danh mục này đang tồn tại sản phẩm',
            );
        }

        await this.categoriesRepository.delete(category.id);
    }

    async listCategoryDiscountOfUser(query: CategoryDiscountPaginateQuery) {
        if (!query.filter || !query.filter.categoryDiscountUserId) {
            throw new BadRequestException(
                'Please provide categoryDiscountUserId and categoryDiscountUserId must be an UUID',
            );
        }

        const categoryDiscountUserId = query.filter.categoryDiscountUserId;
        delete query.filter.categoryDiscountUserId;

        return paginate<Category>(
            this.categoriesRepository,
            query,
            (builder: SelectQueryBuilder<Category>) => {
                builder
                    .leftJoinAndSelect('Category.children', 'children')
                    .leftJoinAndSelect(
                        'children.categoryDiscounts',
                        'CategoryDiscount',
                        'CategoryDiscount.userId = :userId',
                        { userId: categoryDiscountUserId },
                    )
                    .andWhere(
                        `Category.parentId IS NULL AND Category.status = '${CategoryStatus.Show}'`,
                    );
            },
        );
    }
}
