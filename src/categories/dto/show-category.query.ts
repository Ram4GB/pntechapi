import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { IsOptional } from 'class-validator';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class ShowCategoryFilter {
    @IsOptional()
    @IsNotEmptyString()
    name: string;
}

export class ShowCategoryPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            name: 'like',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            name: 'category 1',
        },
    })
    @IsOptional()
    @Type(() => ShowCategoryFilter)
    filter: ShowCategoryFilter;
}
