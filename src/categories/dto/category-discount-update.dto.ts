import { IsNotEmpty, Max, Min } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class CategoryDiscountUpateDTO {
    @ApiProperty()
    @IsNotEmpty()
    @Type(() => Number)
    @Min(0)
    @Max(100)
    discountPercentage: number;
}
