import { IsNotEmpty, IsUUID, Max, Min } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { Type } from 'class-transformer';

export class CategoryDiscountCreateDTO {
    @ApiProperty()
    @IsNotEmpty()
    @Type(() => Number)
    @Min(0)
    @Max(100)
    discountPercentage: number;

    @ApiProperty()
    @IsNotEmptyString()
    @IsUUID()
    userId: string;

    @ApiProperty()
    @IsNotEmptyString()
    @IsUUID()
    categoryId: string;
}
