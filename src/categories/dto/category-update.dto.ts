import { IsEnum, IsOptional } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { CategoryStatus } from 'categories/constant/category.enum';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class CategoryUpdateDTO {
    @ApiPropertyOptional({
        description: 'Category name',
    })
    @IsOptional()
    @IsNotEmptyString()
    name: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsEnum(CategoryStatus)
    status: CategoryStatus;
}
