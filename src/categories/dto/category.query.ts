import { IsEnum, IsOptional } from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { CategoryStatus } from 'categories/constant/category.enum';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { PaginateQuery } from 'shared/helpers/pagination';
import { Type } from 'class-transformer';

class CategoryFilter {
    @IsOptional()
    @IsNotEmptyString()
    name: string;

    @IsOptional()
    @IsEnum(CategoryStatus)
    status: CategoryStatus;
}

export class CategoryPaginateQuery extends PaginateQuery {
    @ApiPropertyOptional({
        description: 'Filter conditions for each field, default is equal " = "',
        example: {
            name: 'like',
            status: '=',
        },
    })
    @IsOptional()
    filterCondition: any;

    @ApiPropertyOptional({
        description: 'Filter value',
        type: 'object',
        example: {
            name: 'category 1',
            status: CategoryStatus.Show,
        },
    })
    @IsOptional()
    @Type(() => CategoryFilter)
    filter: CategoryFilter;
}
