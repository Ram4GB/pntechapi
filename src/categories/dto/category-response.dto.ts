import { Expose } from 'class-transformer';

export class CategoryResponseDTO {
    @Expose()
    id: string;
    @Expose()
    name: string;
    @Expose()
    createdAt: Date;
    @Expose()
    lastUpdatedAt: Date;
}
