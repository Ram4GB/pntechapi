import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class CategoryCreateDTO {
    @ApiProperty({
        description: 'Category name',
    })
    @IsNotEmptyString()
    name: string;

    @ApiPropertyOptional({
        default: null,
        description: 'Parent category id',
    })
    @IsOptional()
    @IsUUID()
    parentId: string;
}
