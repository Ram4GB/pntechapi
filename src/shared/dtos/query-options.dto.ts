import { IsOptional } from 'class-validator';

export class QueryOptions {
    // page?: number = PaginationConfig.PageDefault;
    // limit?: number = PaginationConfig.LimitDefault;
    @IsOptional()
    page: number;
    @IsOptional()
    limit: number;
}
