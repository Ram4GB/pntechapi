export interface PaginationResultInterface<T> {
    results: T[];
    page: number;
    pageSize: number;
    totalRecord: number;
    next?: string;
    previous?: string;
}
export class Pagination<T> {
    results: T[];
    page: number;
    pageSize: number;
    totalRecord: number;
    totalPage: number;
    next?: string;
    previous?: string;

    constructor(paginationResults: PaginationResultInterface<T>) {
        this.results = paginationResults.results;
        this.page = paginationResults.page;
        this.totalRecord = paginationResults.totalRecord;
        this.pageSize = paginationResults.pageSize;
        this.totalPage = Math.ceil(
            paginationResults.totalRecord / paginationResults.pageSize,
        );
    }
}
