export class CheckValidResultDTO {
    isSuccess: boolean;
    errors: Array<string>;
    constructor(isSuccess: boolean, errors: Array<string>) {
        this.isSuccess = isSuccess;
        this.errors = errors;
    }
}
