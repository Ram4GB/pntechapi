import { Injectable, NotFoundException, PipeTransform } from '@nestjs/common';

import { parseOrderCodeToOrderId } from 'orders/helpers/parse-order-id.helper';

@Injectable()
export class ParseOrderCodeToIdPipe implements PipeTransform {
    transform(value: any) {
        /** Sample valid order code from client: DH-0000001 */
        const regexString = `${process.env.ORDER_ID_PREFIX}\\d{${process.env.ORDER_ID_NUM_AFTER_PREFIX}}`;
        /** Using RegExp class because need to concat string from env */
        const regexp = new RegExp(regexString, 'gi');

        if (!regexp.test(value)) {
            throw new NotFoundException();
        }

        const id: number = parseOrderCodeToOrderId(value);

        return id;
    }
}
