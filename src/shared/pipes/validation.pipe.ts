import {
    ArgumentMetadata,
    BadRequestException,
    Injectable,
    PipeTransform,
} from '@nestjs/common';
import { ValidationError, validate } from 'class-validator';

import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
    async transform(value: any, { metatype }: ArgumentMetadata): Promise<any> {
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }

        const object = plainToClass(metatype, value);

        const errors = await validate(object, {
            whitelist: true,
            validationError: {
                target: false,
                value: false,
            },
        });
        if (errors.length > 0) {
            throw new BadRequestException(this.formatErrors(errors));
        }

        return object;
    }

    private toValidate(metatype: Function): boolean {
        const types: Function[] = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }

    /**
     * Format error response to client.
     *
     * @param  {ValidationError[]} errors
     * @returns A error formatted
     */
    private formatErrors(errors: ValidationError[]) {
        const formatted = {};

        // Format the errors do not contain children errors
        errors.forEach(error => {
            if (error.property !== 'filter') {
                formatted[error.property] =
                    error.constraints &&
                    !!Object.values(error.constraints).length
                        ? Object.values(error.constraints).join('\n')
                        : '';
                // Handle children error
                // if (!!error.children && error.children.length > 0) {

                // }
            }
        });

        // Find the nested error of filter field
        const nestedErrorOfFilter: ValidationError = errors.find(
            item => item.property === 'filter',
        );

        // Format the children error of filter field if exist
        if (nestedErrorOfFilter) {
            nestedErrorOfFilter.children.forEach(nested => {
                formatted[`filter.${nested.property}`] =
                    nested.constraints &&
                    !!Object.values(nested.constraints).length
                        ? Object.values(nested.constraints).join('\n')
                        : '';
            });
        }

        //Todo
        //Validate filter conditions

        return formatted;
    }
}
