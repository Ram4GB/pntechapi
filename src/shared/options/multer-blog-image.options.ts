import * as path from 'path';

import { BadRequestException } from '@nestjs/common';
import { ImageFileMime } from '../constants/image-filemime';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';

export const multerBlogImageOptions: MulterOptions = {
    storage: diskStorage({
        destination: './uploads/blog-images',
        filename: (req, file, _cb) => {
            _cb(null, `${uuidv4()}${path.extname(file.originalname)}`);
        },
    }),
    fileFilter: (_req, file, callback) => {
        if (!ImageFileMime.includes(file.mimetype)) {
            return callback(
                new BadRequestException('Chỉ được phép đăng tải hình ảnh'),
                false,
            );
        } else {
            callback(null, true);
        }
    },
    limits: { fileSize: 10485760 },
};
