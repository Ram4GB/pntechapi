import { Injectable } from '@nestjs/common';
import { PaginationConfig } from '../constants/pagination-config';
import { QueryOptions } from '../dtos/query-options.dto';

@Injectable()
export class PaginationService {
    convertValidQueryOption(query: any): any {
        const validQuery = {
            ...query,
            page: isNaN(query.page) ? PaginationConfig.PageDefault : query.page,
            limit: isNaN(query.limit)
                ? PaginationConfig.LimitDefault
                : query.limit,
        };

        return validQuery;
        // const validQuery: QueryOptions = {
        //     page: isNaN(query.page) ? PaginationConfig.PageDefault : query.page,
        //     limit: isNaN(query.limit)
        //         ? PaginationConfig.LimitDefault
        //         : query.limit,
        // };

        // return validQuery;
    }

    convertValidPageAndLimit(page: number, limit: number): QueryOptions {
        const validPage = isNaN(page) ? PaginationConfig.PageDefault : page;
        const validLimit = isNaN(limit) ? PaginationConfig.LimitDefault : limit;

        return {
            page: validPage,
            limit: validLimit,
        };
    }
}
