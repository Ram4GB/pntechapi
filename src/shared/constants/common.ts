export enum CommonConfig {
    SALT_ROUNDS = 12,
    TimeZoneMoment = 'Asia/Ho_Chi_Minh',
    DEFAULT_PAGE = 1,
    DEFAULT_LIMIT = 10,
}
