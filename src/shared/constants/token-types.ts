export enum TokenTypes {
    AccessToken = 'accessToken',
    VerifyEmailToken = 'verifyEmailToken',
    ResetPasswordToken = 'ResetPasswordToken',
}
