export enum Roles {
    Anonymous = 'anonymous',
    Admin = 'admin',
    Seller = 'seller',
    Customer = 'customer',
    SuperAdmin = 'superadmin',
}
