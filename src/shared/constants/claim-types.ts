export enum ClaimTypes {
    DateOfBirth = 'dateofbirth',
    Gender = 'gender',
    SellerGroupId = 'sellerGroupId',
}
