export enum PaginationConfig {
    LimitDefault = 10,
    PageDefault = 1,
}
