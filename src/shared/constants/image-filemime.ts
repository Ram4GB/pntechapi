export const ImageFileMime = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
];
