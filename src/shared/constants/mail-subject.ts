export enum MailSubject {
    VERIFICATION_MAIL = 'Xác nhận email',
    ANNOUNCE_QUOTATION_SUCCEEDED = 'Announce quotatio succeeded',
    QUOTATION = 'Quotation your order',
    RESET_PASSWORD = 'Khôi phục mật khẩu',
    USER_ORDER_IS_CREATED = 'Đơn hàng của bạn vừa được khởi tạo',
    CONFIRM_USER_ORDER = 'Xác nhận đơn hàng mới của bạn',
    CREATED_ADMIN = 'Bạn đã được tạo một tài khoản Admin từ PnTech',
    CREATED_SELLER = 'Bạn đã được tạo một tài khoản Seller từ PnTech',
}
