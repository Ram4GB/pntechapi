import { ValidationOptions, registerDecorator } from 'class-validator';

export function IsNotEmptyString(validationOptions?: ValidationOptions) {
    return function(object: Object, propertyName: string) {
        registerDecorator({
            name: 'isNotEmptyString',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: {
                message: '$property should not be empty',
                ...validationOptions,
            },
            validator: {
                validate(value: any) {
                    const notUndefined = typeof value !== 'undefined';
                    const notNull = value !== null;
                    return (
                        notUndefined &&
                        notNull &&
                        typeof value === 'string' &&
                        value.trim().length > 0
                    );
                },
            },
        });
    };
}
