import { SetMetadata, UseGuards, applyDecorators } from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from 'auth/jwt-auth.guard';
import { Roles } from 'shared/constants/roles';
import { RolesGuard } from 'auth/roles.guard';

export function Authorize(...roles: Roles[]) {
    if (roles && roles.includes(Roles.Anonymous)) {
        return applyDecorators(
            SetMetadata('roles', roles),
            UseGuards(AuthGuard(['jwt', 'anonymous']), RolesGuard),
        );
    }
    return applyDecorators(
        SetMetadata('roles', roles),
        UseGuards(JwtAuthGuard, RolesGuard),
    );
}
