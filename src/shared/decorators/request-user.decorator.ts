import { ExecutionContext, createParamDecorator } from '@nestjs/common';

import { RequestUser } from 'auth/dto/req-user.interface';

export const ReqUser = createParamDecorator(
    (data: unknown, ctx: ExecutionContext): RequestUser => {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    },
);
