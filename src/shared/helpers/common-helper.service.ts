import * as util from 'util';

export class CommonHelperService {
    consoleLogObjectDepth(obj: any) {
        console.log(util.inspect(obj, false, null, true /* enable colors */));
    }

    generateRandomPassword() {
        return Math.random()
            .toString(36)
            .slice(-8);
    }

    findBlogThumbnail(content: string) {
        let thumbnail = null;
        if (content && content.trim().length) {
            const patt = /<img.+?src=[\'"]([^\'"]+)[\'"].*?>/i;
            const matchs = patt.exec(content);
            thumbnail = matchs && matchs[1] ? matchs[1] : null;
        }
        return thumbnail;
    }

    findBlogSummary(content: string) {
        let summary = null;
        if (content && content.trim().length) {
            const patt = /<p>\s*(.+?)\s*<\/p>/i;
            const matchs = patt.exec(content);
            if (matchs && matchs[1] && matchs[1].length) {
                summary =
                    matchs[1].length >= 100
                        ? matchs[1].substring(0, 100)
                        : matchs[1];
            }
        }
        return summary;
    }
}
