import { CommonHelperService } from './common-helper.service';
import { Module } from '@nestjs/common';
import { UpdateHelperService } from './update-helper.service';

@Module({
    providers: [UpdateHelperService, CommonHelperService],
    exports: [UpdateHelperService, CommonHelperService],
})
export class HelpersModule {}
