export class UpdateHelperService<T> {
    createUpdateEntityBaseUpdateDto(updateDto: T, entity: T): any {
        Object.keys(updateDto).forEach(key => {
            entity[key] = updateDto[key];
        });

        return entity;
    }
}
