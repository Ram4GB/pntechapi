export class ApiError {
    messages: Object = {};

    add(field: string, message: string) {
        this.messages[field] = message;
    }
}
