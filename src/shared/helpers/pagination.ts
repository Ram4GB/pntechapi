import { IsInt, IsOptional, Min, ValidateNested } from 'class-validator';
import { Repository, SelectQueryBuilder } from 'typeorm';

import { ApiError } from './api.error';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { BadRequestException } from '@nestjs/common';
import { CommonConfig } from 'shared/constants/common';
import { Type } from 'class-transformer';

export class PaginateQuery {
    @ApiPropertyOptional({ default: CommonConfig.DEFAULT_PAGE })
    @IsOptional()
    @Type(() => Number)
    @IsInt()
    @Min(1)
    page: number = CommonConfig.DEFAULT_PAGE;

    @ApiPropertyOptional({ default: CommonConfig.DEFAULT_LIMIT })
    @IsOptional()
    @Type(() => Number)
    @IsInt()
    @Min(1)
    limit: number = CommonConfig.DEFAULT_LIMIT;

    @ApiPropertyOptional({
        description: 'Support sorting by many fields',
        default: {
            createdAt: 'DESC',
        },
    })
    @IsOptional()
    order: object;

    filterCondition: any;

    @ValidateNested()
    filter: any;
}

export class PagedList<T> {
    data: T[];
    count: number;
    totalItems: number;
    currentPage: number;
    totalPages: number;
    pageSize: number;

    constructor(data: Array<T>, totalItems, currentPage, pageSize) {
        this.data = data;
        this.count = data.length;
        this.currentPage = currentPage;
        this.totalItems = totalItems;
        this.pageSize = pageSize;
        this.totalPages = Math.ceil(totalItems / pageSize);
    }
}

export async function paginate<T>(
    repository: Repository<T>,
    query: PaginateQuery,
    fn?: (builder: SelectQueryBuilder<T>) => void,
) {
    const { page, limit } = query;
    const alias = repository.metadata.name;
    let builder = repository.createQueryBuilder(alias).where('1=1');
    builder = buildQuery<T>(alias, builder, query);
    builder = buildSort<T>(alias, builder, query);

    builder.skip(limit * (page - 1)).take(limit);
    !!fn && fn(builder);
    const [data, total] = await builder.getManyAndCount();
    return new PagedList(data, total, page, limit);
}

function buildSort<T>(
    alias: string,
    builder: SelectQueryBuilder<T>,
    query: PaginateQuery,
) {
    const { order } = query;

    if (!order || Object.keys(order).length === 0) {
        builder.orderBy(`${alias}.createdAt`, 'DESC');
        return builder;
    }

    let index = 0;
    Object.keys(order).forEach(key => {
        if (!['ASC', 'DESC'].includes(order[key])) {
            return; //skip
        }
        if (index === 0) {
            builder.orderBy(`${alias}.${key}`, order[key]);
        } else {
            builder.addOrderBy(`${alias}.${key}`, order[key]);
        }
        index++;
    });

    return builder;
}

function buildQuery<T>(
    alias: string,
    builder: SelectQueryBuilder<T>,
    query: PaginateQuery,
) {
    const { filter, filterCondition } = query;
    if (!filter || Object.keys(filter).length === 0) {
        return builder;
    }
    const error = new ApiError();
    Object.keys(filter).forEach(key => {
        const condition =
            filterCondition && filterCondition[key]
                ? filterCondition[key]
                : undefined;
        switch (condition) {
            case 'not':
                builder.andWhere(`${alias}.${key} != :${key}`, {
                    [key]: filter[key],
                });
                break;
            case 'like':
                builder.andWhere(`${alias}.${key} like :${key}`, {
                    [key]: `%${filter[key]}%`,
                });
                break;
            case '>':
                builder.andWhere(`${alias}.${key} > :${key}`, {
                    [key]: filter[key],
                });
                break;
            case '<':
                builder.andWhere(`${alias}.${key} < :${key}`, {
                    [key]: filter[key],
                });
                break;
            case '>=':
                builder.andWhere(`${alias}.${key} >= :${key}`, {
                    [key]: filter[key],
                });
                break;
            case '<=':
                builder.andWhere(`${alias}.${key} <= :${key}`, {
                    [key]: filter[key],
                });
                break;
            case 'in':
                if (Array.isArray(filter[key]) && filter[key].length > 0) {
                    builder.andWhere(`${alias}.${key} in (:${key})`, {
                        [key]: filter[key],
                    });
                } else {
                    error.add(
                        key,
                        'When filtering in, the value must be an array of lenght > 0',
                    );
                    throw new BadRequestException(error.messages);
                }
                break;
            case 'between':
                if (Array.isArray(filter[key]) && filter[key].length === 2) {
                    builder.andWhere(
                        `${alias}.${key} between :${key}start AND :${key}end`,
                        {
                            [key + 'start']: filter[key][0],
                            [key + 'end']: filter[key][1],
                        },
                    );
                } else {
                    error.add(
                        key,
                        'When filtering between, the value must be an array of lenght = 2',
                    );
                    throw new BadRequestException(error.messages);
                }
                break;
            default:
                builder.andWhere(`${alias}.${key} = :${key}`, {
                    [key]: filter[key],
                });
                break;
        }
    });

    return builder;
}
