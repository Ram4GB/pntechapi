import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    HttpException,
    HttpStatus,
    Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';

import { unlink } from 'fs';

// This filter catch all unhandled exception
@Catch()
export class AllExceptionFilter implements ExceptionFilter {
    catch(exception: Error, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest<Request>();
        const response = ctx.getResponse<Response>();
        const status =
            exception instanceof HttpException
                ? exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;

        const errorResponse = {
            statusCode: status,
            message:
                exception instanceof HttpException
                    ? exception.getResponse()
                    : 'Internal Server Error',
        };

        // response exception.message when exception.message is string
        // response exception.getResponse() when exception is object
        // when exception.getResponse() is object, then exception.message contain 'Exception' at the end
        if (!exception.message.includes('Exception') && status != 500) {
            errorResponse.message = exception.message;
        }

        const file = request.file;
        const files = request.files;

        if (file) {
            unlink(file.path, () => {});
        }
        if (files && files.length > 0) {
            for (let i = 0; i < files.length; i++) {
                unlink(files[i].path, () => {});
            }
        }

        if (process.env.NODE_ENV !== 'test') {
            Logger.error(
                `${request.method} ${request.url}`,
                JSON.stringify(errorResponse),
                'ExceptionFilter',
            );

            if (process.env.SERVER_LOG_STACK === 'true') {
                Logger.error(`Trace`, exception.stack, 'ExceptionFilter');
            }
        }

        response.status(status).json(errorResponse);
    }
}
