import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository, SelectQueryBuilder } from 'typeorm';

import { Banner } from './banner.entity';
import { BannerPaginateQuery } from './dto/banner.query';
import { InjectRepository } from '@nestjs/typeorm';
import { Media } from 'media/media.entity';
import { MediaService } from 'media/media.service';
import { MediaUsage } from 'media/media-usage.entity';
import { UpdateBannerDTO } from './dto/update-banner.dto';
import { paginate } from 'shared/helpers/pagination';

@Injectable()
export class BannersService {
    constructor(
        @InjectRepository(Banner)
        private readonly bannersRepository: Repository<Banner>,
        private readonly mediaService: MediaService,
    ) {}

    async getAll(query: BannerPaginateQuery) {
        return paginate<Banner>(
            this.bannersRepository,
            query,
            (builder: SelectQueryBuilder<Banner>) => {
                builder
                    .innerJoinAndMapOne(
                        'Banner.usage',
                        MediaUsage,
                        'MediaUsage',
                        "Banner.id = MediaUsage.usedById AND MediaUsage.type = 'banner'",
                    )
                    .innerJoinAndMapOne(
                        'Banner.media',
                        Media,
                        'Media',
                        'MediaUsage.mediaId = Media.id',
                    );
            },
        );
    }

    async findById(id: string): Promise<Banner> {
        const banner = await this.bannersRepository.findOne(id);

        const mediaOfBanner = await this.mediaService.getOneMediaUsedByEntity(
            'banner',
            id,
        );
        banner.media = mediaOfBanner;
        banner.weight = mediaOfBanner.weight;
        return banner;
    }

    async create(banner: Banner): Promise<Banner> {
        return this.bannersRepository.save(banner);
    }

    async update(id: string, updateInfo: UpdateBannerDTO): Promise<void> {
        const existingBanner = await this.bannersRepository.findOne(id);
        if (!existingBanner) {
            throw new NotFoundException('Banner not found');
        }

        await this.bannersRepository.update(id, { ...updateInfo });
    }

    async delete(id: string): Promise<void> {
        const banner = await this.bannersRepository.findOne(id);
        if (!banner) {
            throw new NotFoundException('Banner not found');
        }
        await this.bannersRepository.delete(id);
    }
}
