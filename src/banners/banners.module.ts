import { Banner } from './banner.entity';
import { BannersController } from './banners.controller';
import { BannersService } from './banners.service';
import { MediaModule } from 'media/media.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([Banner]), MediaModule],
    providers: [BannersService],
    controllers: [BannersController],
})
export class BannersModule {}
