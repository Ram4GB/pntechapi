import { IsNotEmptyObject, IsOptional, ValidateNested } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { UseMediaDTO } from 'media/dto/use-media.dto';

export class UpdateMediaBannerDTO {
    @ApiProperty({
        description: 'Media',
        example: {
            id: '3e6afe3f-2dc7-44fc-9e12-801d55fa3893',
            weight: 2,
        },
    })
    @IsOptional()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => UseMediaDTO)
    media: UseMediaDTO;
}
