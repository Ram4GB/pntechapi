import { IsEmpty } from 'class-validator';
import { PaginateQuery } from 'shared/helpers/pagination';

export class BannerPaginateQuery extends PaginateQuery {
    @IsEmpty()
    filterCondition: any;

    @IsEmpty()
    filter: any;
}
