import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateBannerDTO {
    @ApiPropertyOptional({
        description: 'Banner link',
        example: 'https://pntech.vn',
    })
    @IsOptional()
    link: string;

    @ApiPropertyOptional()
    @IsOptional()
    title: string;

    @ApiPropertyOptional()
    @IsOptional()
    description: string;

    @ApiPropertyOptional()
    @IsOptional()
    buttonTitle: string;
}
