import { ApiBody, ApiTags } from '@nestjs/swagger';
import {
    Body,
    Controller,
    Delete,
    Get,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
} from '@nestjs/common';

import { Authorize } from 'shared/decorators/authorize.decorator';
import { Banner } from './banner.entity';
import { BannerPaginateQuery } from './dto/banner.query';
import { BannersService } from './banners.service';
import { CreateBannerDTO } from './dto/create-banner.dto';
import { CreateMediaBannerDTO } from './dto/create-media-banner.dto';
import { MediaService } from 'media/media.service';
import { PagedList } from 'shared/helpers/pagination';
import { Roles } from 'shared/constants/roles';
import { UpdateBannerDTO } from './dto/update-banner.dto';
import { UpdateMediaBannerDTO } from './dto/update-media-banner.dto';

@Controller('banners')
@ApiTags('Banners')
export class BannersController {
    constructor(
        private readonly mediaService: MediaService,
        private readonly bannersService: BannersService,
    ) {}

    @Get()
    async getAllBanners(
        @Query() query: BannerPaginateQuery,
    ): Promise<PagedList<Banner>> {
        const banners = await this.bannersService.getAll(query);
        banners.data.forEach(banner => {
            banner.weight = banner.usage.weight;
        });
        return banners;
    }

    @Get(':id')
    async getBannerById(@Param('id') id: string): Promise<Banner> {
        const banner = await this.bannersService.findById(id);
        if (!banner) {
            throw new NotFoundException('Banner not found');
        }
        return banner;
    }

    @ApiBody({
        schema: {
            example: {
                link: 'https://pntech.vn (Optional)',
                title: 'string (Optional)',
                description: 'string (Optional)',
                buttonTitle: 'string (Optional)',
                media: {
                    id: 'id (Required)',
                    weight: '1 (Required)',
                },
            },
        },
    })
    @Post()
    @Authorize(Roles.Admin)
    async addBanner(
        @Body() bannerInfo: CreateBannerDTO,
        @Body() mediaBanner: CreateMediaBannerDTO,
    ): Promise<void> {
        const banner = new Banner({
            ...bannerInfo,
        });

        const createdBanner = await this.bannersService.create(banner);

        /** Add images for banner from media */
        await this.mediaService.useMedia(
            'banner',
            createdBanner.id,
            mediaBanner.media.id,
            mediaBanner.media.weight,
        );
    }

    @ApiBody({
        schema: {
            example: {
                link: 'https://pntech.vn (Optional)',
                title: 'string (Optional)',
                description: 'string (Optional)',
                buttonTitle: 'string (Optional)',
                media: {
                    id: 'id (Optional)',
                    weight: '1 (Optional)',
                },
            },
        },
    })
    @Put(':id')
    @Authorize(Roles.Admin)
    async updateBanner(
        @Param('id') id: string,
        @Body() updateInfo: UpdateBannerDTO,
        @Body() mediaBanner: UpdateMediaBannerDTO,
    ): Promise<void> {
        await this.bannersService.update(id, updateInfo);

        /** Update images for banner from media */
        if (mediaBanner.media && mediaBanner.media.id) {
            await this.mediaService.unUsedMediaAnymore('banner', id);
            await this.mediaService.useMedia(
                'banner',
                id,
                mediaBanner.media.id,
                mediaBanner.media.weight,
            );
        }
    }

    @Delete(':id')
    @Authorize(Roles.Admin)
    async deleteBanner(@Param('id') id: string): Promise<void> {
        await this.bannersService.delete(id);
        await this.mediaService.unUsedMediaAnymore('banner', id);
    }
}
