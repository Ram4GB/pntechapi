import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

@Entity()
export class Banner {
    constructor(partial: Partial<Banner>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Column({ nullable: true })
    @Expose()
    title: string;

    @Column({ nullable: true })
    @Expose()
    description: string;

    @Column({ nullable: true })
    @Expose()
    buttonTitle: string;

    @Column({ default: '#' })
    @Expose()
    link: string;

    media: any;
    weight: any;
    @Exclude()
    usage: any;

    @CreateDateColumn()
    createdAt: Date;

    @CreateDateColumn()
    updatedAt: Date;
}
