import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Expose } from 'class-transformer';
import { User } from './user.entity';

@Entity()
export class UserClaim {
    constructor(partial: Partial<UserClaim>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Expose()
    claimType: string;

    @Column()
    @Expose()
    claimValue: string;

    /** Reference column */
    @Column()
    userId: string;

    /** References */
    @ManyToOne(
        type => User,
        user => user.userClaims,
    )
    user: User;
}
