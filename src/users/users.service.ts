import * as bcrypt from 'bcrypt';

import {
    Connection,
    EntityManager,
    In,
    Not,
    Repository,
    SelectQueryBuilder,
    UpdateResult,
} from 'typeorm';
import {
    Inject,
    Injectable,
    Logger,
    NotFoundException,
    forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { PaginateQuery, paginate } from 'shared/helpers/pagination';

import { AuthService } from 'auth/auth.service';
import { CartsService } from 'carts/carts.service';
import { CheckValidResultDTO } from 'shared/dtos/check-valid-result.dto';
import { ClaimTypes } from 'shared/constants/claim-types';
import { CommonConfig } from 'shared/constants/common';
import { CommonHelperService } from 'shared/helpers/common-helper.service';
import { ConfigService } from '@nestjs/config';
import { CustomCartProductsService } from 'carts/custom-cart-product.service';
import { MailSubject } from 'shared/constants/mail-subject';
import { MailerService } from '@nestjs-modules/mailer';
import { Media } from 'media/media.entity';
import { MediaUsage } from 'media/media-usage.entity';
import { Role } from './role.entity';
import { Roles } from 'shared/constants/roles';
import { User } from './user.entity';
import { UserClaim } from './userclaim.entity';
import { UserPaginateQuery } from './dto/user.query';
import { UserStatus } from './constant/userstatus';
import { UserUpdateDTO } from './dto/user-update.dto';

@Injectable()
export class UsersService {
    constructor(
        @InjectConnection()
        private connection: Connection,
        @InjectRepository(User)
        private usersRepository: Repository<User>,
        @InjectRepository(UserClaim)
        private userClaimsRepository: Repository<UserClaim>,
        @InjectRepository(Role)
        private rolesRepository: Repository<Role>,
        @Inject(forwardRef(() => AuthService))
        private readonly authService: AuthService,
        @Inject(forwardRef(() => CartsService))
        private readonly cartsService: CartsService,
        @Inject(forwardRef(() => CustomCartProductsService))
        private readonly customCartProductsService: CustomCartProductsService,
        private readonly commonHelperService: CommonHelperService,
        private readonly mailerService: MailerService,
        private readonly configService: ConfigService,
    ) {}

    async listAdminsPaginated(query: UserPaginateQuery) {
        return paginate<User>(
            this.usersRepository,
            query,
            (builder: SelectQueryBuilder<User>) => {
                builder
                    .leftJoinAndSelect('User.roles', 'roles')
                    .andWhere(`roles.name = '${Roles.Admin}'`);
            },
        );
    }

    async listCustomersPaginated(query: UserPaginateQuery) {
        return paginate<User>(
            this.usersRepository,
            query,
            (builder: SelectQueryBuilder<User>) => {
                builder
                    .leftJoinAndSelect('User.roles', 'roles')
                    .andWhere(`roles.name = '${Roles.Customer}'`);
            },
        );
    }

    async listCustomersInGroupPaginated(
        query: UserPaginateQuery,
        sellerId: string,
    ) {
        return paginate<User>(
            this.usersRepository,
            query,
            (builder: SelectQueryBuilder<User>) => {
                builder
                    .leftJoinAndSelect('User.roles', 'roles')
                    .andWhere(`roles.name = '${Roles.Customer}'`)
                    .andWhere(qb => {
                        const subQuery = qb
                            .subQuery()
                            .select('claimType')
                            .from(UserClaim, 'UserClaim')
                            .where('UserClaim.userId = User.id')
                            .andWhere(
                                `UserClaim.claimType = '${ClaimTypes.SellerGroupId}'`,
                            )
                            .andWhere(`UserClaim.claimValue = '${sellerId}'`)
                            .getQuery();
                        return 'EXISTS ' + subQuery;
                    });
            },
        );
    }

    async listCustomersNotInGroupPaginated(query: PaginateQuery) {
        return paginate<User>(
            this.usersRepository,
            query,
            (builder: SelectQueryBuilder<User>) => {
                builder
                    .leftJoinAndSelect('User.roles', 'roles')
                    .andWhere(`roles.name = '${Roles.Customer}'`)
                    .andWhere(qb => {
                        const subQuery = qb
                            .subQuery()
                            .select('claimType')
                            .from(UserClaim, 'UserClaim')
                            .where('UserClaim.userId = User.id')
                            .andWhere(
                                `UserClaim.claimType = '${ClaimTypes.SellerGroupId}'`,
                            )
                            .getQuery();
                        return 'NOT EXISTS ' + subQuery;
                    });
            },
        );
    }

    async listSellersPaginated(query: PaginateQuery) {
        return paginate<User>(
            this.usersRepository,
            query,
            (builder: SelectQueryBuilder<User>) => {
                builder
                    .leftJoinAndSelect('User.roles', 'roles')
                    .andWhere(`roles.name = '${Roles.Seller}'`);
            },
        );
    }

    async getActiveAdmins(): Promise<User[] | undefined> {
        return this.usersRepository
            .createQueryBuilder()
            .leftJoinAndSelect('User.roles', 'roles')
            .where(`roles.name = '${Roles.Admin}'`)
            .andWhere(`User.status = '${UserStatus.Active}'`)
            .getMany();
    }

    async getActiveAdminById(id: string): Promise<User | undefined> {
        return this.usersRepository
            .createQueryBuilder('User')
            .leftJoinAndSelect('User.roles', 'roles')
            .where(`roles.name = '${Roles.Admin}'`)
            .andWhere(`User.status = '${UserStatus.Active}'`)
            .andWhere('User.id = :id', { id })
            .getOne();
    }

    async getActiveUserByEmail(email: string): Promise<User | undefined> {
        return this.usersRepository.findOne({
            where: {
                email,
                status: UserStatus.Active,
            },
            relations: ['roles'],
        });
    }

    async getOnlyUserInfoByEmail(email: string): Promise<User> {
        return await this.usersRepository.findOne({
            where: {
                email,
            },
        });
    }

    async getOnlyUserInfoById(id: string): Promise<User> {
        return await this.usersRepository.findOne({
            where: {
                id,
            },
        });
    }

    async findById(id: string): Promise<User> {
        return await this.usersRepository.findOne({
            where: {
                id,
            },
            relations: ['roles'],
        });
    }

    async getUserClaims(user: User): Promise<UserClaim> {
        return this.userClaimsRepository.findOne({
            user,
        });
    }

    async findOneByEmailOrPhone(
        searchEmail: string,
        searchPhone: string,
    ): Promise<User | undefined | null> {
        return await this.usersRepository.findOne({
            where: [
                {
                    email: searchEmail,
                },
                {
                    phone: searchPhone,
                },
            ],
        });
    }

    async findUserByEmail(email: string): Promise<User> {
        return this.usersRepository.findOne({
            where: {
                email,
            },
            relations: ['roles'],
        });
    }

    async createUser(user: User, manager: EntityManager): Promise<User> {
        /** Prepare full user info */
        const passwordHashed = await bcrypt.hash(
            user.passwordHash,
            CommonConfig.SALT_ROUNDS,
        );
        user.passwordHash = passwordHashed;

        const createdUser = await manager.save(user);

        return createdUser;
    }

    async update(user: User): Promise<UpdateResult> {
        return await this.usersRepository.update(user.id, user);
    }

    async validateUserBeforeUpdate(
        userDto: UserUpdateDTO,
        user: User,
    ): Promise<CheckValidResultDTO> {
        /** If user update phone then make sure it not duplicate */

        if (userDto.phone) {
            const existedUserWithPhone = await this.usersRepository.findOne({
                where: {
                    id: Not(user.id),
                    phone: userDto.phone,
                },
            });

            if (existedUserWithPhone) {
                return new CheckValidResultDTO(false, [
                    'Số điện thoại đã có người sử dụng',
                ]);
            }
        }

        /** If user update password then validate
         * password and confirm password are matched
         * and old password has to be corrected*/
        if (userDto.password) {
            // validate the old password has to be corrected
            const isOldPasswordCorrect = await bcrypt.compare(
                userDto.oldPassword,
                user.passwordHash,
            );
            if (!isOldPasswordCorrect) {
                return new CheckValidResultDTO(false, [
                    'Mật khẩu cũ không chính xác',
                ]);
            }

            // validate password and confirm password are matched
            if (userDto.password !== userDto.confirmPassword) {
                return new CheckValidResultDTO(false, [
                    'Mật khẩu mới và xác nhận mật khẩu mới không khớp',
                ]);
            }
        }

        return new CheckValidResultDTO(true, []);
    }

    async prepareUserUpdateEntity(
        userUpdateInfo: UserUpdateDTO,
        user: User,
    ): Promise<User> {
        /** If password is updated then hash it before update */
        if (userUpdateInfo.password) {
            userUpdateInfo.password = await bcrypt.hash(
                userUpdateInfo.password,
                CommonConfig.SALT_ROUNDS,
            );

            user.passwordHash = userUpdateInfo.password;

            delete userUpdateInfo.password;
        }

        /** Remove unnecessary fields before map dto to entity*/
        delete userUpdateInfo.confirmPassword;
        delete userUpdateInfo.oldPassword;

        Object.keys(userUpdateInfo).forEach(key => {
            user[key] = userUpdateInfo[key];
        });

        return user;
    }

    async addCustomersToGroup(
        customerIds: string[],
        sellerId: string,
    ): Promise<void> {
        await this.userClaimsRepository.delete({
            userId: In(customerIds),
            claimType: ClaimTypes.SellerGroupId,
        });

        const userClaim = customerIds.map(
            customerId =>
                new UserClaim({
                    userId: customerId,
                    claimType: ClaimTypes.SellerGroupId,
                    claimValue: sellerId,
                }),
        );

        await this.userClaimsRepository.save(userClaim);
    }

    async addToRole(
        user: User,
        roleName: string,
        manager: EntityManager,
    ): Promise<any> {
        const role = await this.rolesRepository.findOne({ name: roleName });
        if (!role) {
            throw new NotFoundException('Role not found');
        }
        user.roles = [role];
        await manager.save(user);
    }

    async isInRole(user: User, roleName: string): Promise<boolean> {
        if (!user.roles || !user.roles.length) {
            const role = await this.rolesRepository
                .createQueryBuilder()
                .leftJoin('Role.users', 'Users')
                .where('Role.name = :roleName', { roleName: roleName })
                .andWhere('Users.Id = :userId', { userId: user.id })
                .getOne();
            return !!role;
        }
        const isInRole = user.roles.some(role => role.name === roleName);
        return isInRole;
    }

    async createAdmin(user: User): Promise<User> {
        await this.authService.validateValidNewUser(user);

        const randomPassword = this.commonHelperService.generateRandomPassword();
        user.passwordHash = randomPassword;
        user.status = UserStatus.Active;

        let createdUser;
        await this.connection.transaction(async manager => {
            createdUser = await this.createUser(user, manager);
            await this.addToRole(createdUser, Roles.Admin, manager);
        });

        const mailContext = {
            name: `${user.lastName} ${user.firstName}`,
            email: user.email,
            role: Roles.Admin.toUpperCase(),
            password: randomPassword,
            linkLogin: this.configService.get<string>('DASHBOARD_URL'),
        };

        this.mailerService
            .sendMail({
                to: user.email,
                subject: MailSubject.CREATED_ADMIN,
                template: 'created-seller-or-admin',
                context: mailContext,
            })
            .catch(error => {
                Logger.error(error);
            });

        return createdUser;
    }

    async createSeller(user: User): Promise<User> {
        await this.authService.validateValidNewUser(user);

        const randomPassword = this.commonHelperService.generateRandomPassword();
        user.passwordHash = randomPassword;
        user.status = UserStatus.Active;

        let createdUser;
        await this.connection.transaction(async manager => {
            createdUser = await this.createUser(user, manager);
            await this.addToRole(createdUser, Roles.Seller, manager);
        });

        const mailContext = {
            name: `${user.lastName} ${user.firstName}`,
            email: user.email,
            role: Roles.Seller.toUpperCase(),
            password: randomPassword,
            linkLogin: this.configService.get<string>('DASHBOARD_URL'),
        };

        this.mailerService
            .sendMail({
                to: user.email,
                subject: MailSubject.CREATED_SELLER,
                template: 'created-seller-or-admin',
                context: mailContext,
            })
            .catch(error => {
                Logger.error(error);
            });

        return createdUser;
    }

    async getAllUserCartsAndCustomProductsInCart(id: string): Promise<User> {
        return await this.usersRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.customCartProducts', 'customCartProducts')
            .leftJoinAndSelect('user.carts', 'carts')
            .leftJoinAndSelect('carts.product', 'Product')
            .leftJoinAndSelect(
                MediaUsage,
                'MediaUsage',
                "Product.id = MediaUsage.usedById AND MediaUsage.type = 'product'",
            )
            .leftJoinAndMapOne(
                'Product.media',
                Media,
                'Media',
                'MediaUsage.mediaId = Media.id',
            )
            .andWhere('MediaUsage.weight = 1 OR MediaUsage.weight IS NULL')
            .leftJoinAndSelect('Product.category', 'category')
            .where('user.id = :id', { id })
            .getOne();
    }

    async getAllProductInCartToMapToOrder(id: string): Promise<User> {
        return await this.usersRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.customCartProducts', 'customCartProducts')
            .leftJoinAndSelect('user.carts', 'carts')
            .leftJoinAndSelect('carts.product', 'product')
            .leftJoinAndSelect('product.category', 'category')
            .leftJoinAndSelect(
                'category.categoryDiscounts',
                'categoryDiscount',
                'categoryDiscount.userId = user.id',
            )
            .where('user.id = :id', { id })
            .getOne();
    }

    async clearUserCart(id: string): Promise<void> {
        await this.customCartProductsService.clearAllCustomCartProductsInCart(
            id,
        );
        await this.cartsService.clearAllProductsInCarts(id);
    }

    async findByIds(ids: string[]) {
        return this.usersRepository.findByIds(ids);
    }

    async usersIsInRole(userIds: string[], roleName: string) {
        const result = await this.usersRepository
            .createQueryBuilder()
            .leftJoin('User.roles', 'Role')
            .where('Role.name = :roleName', { roleName: roleName })
            .andWhere('User.Id IN(:userIds)', { userIds })
            .getCount();
        console.log('count', userIds.length);
        console.log('count', result);

        return userIds.length === result;
    }
}
