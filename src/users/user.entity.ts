import {
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

import { Cart } from 'carts/cart.entity';
import { Category } from 'categories/category.entity';
import { CategoryDiscount } from 'categorydiscounts/categorydiscount.entity';
import { CustomCartProduct } from 'carts/custom-cart-product.entity';
import { Order } from 'orders/entities/order.entity';
import { Participants } from 'chats/participants.entity';
import { Product } from 'products/products.entity';
import { ProductImport } from 'product-imports/product-import.entity';
import { Role } from './role.entity';
import { UserClaim } from './userclaim.entity';
import { UserStatus } from 'users/constant/userstatus';

@Entity()
export class User {
    constructor(partial: Partial<User>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ unique: true })
    @Expose()
    email: string;

    @Exclude()
    @Column()
    passwordHash: string;

    @Column()
    @Expose()
    firstName: string;

    @Column()
    @Expose()
    lastName: string;

    @Column()
    @Expose()
    phone: string;

    @Column({ type: 'enum', enum: UserStatus, default: UserStatus.Inactive })
    status: UserStatus;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(
        type => UserClaim,
        userClaim => userClaim.user,
    )
    userClaims: UserClaim[];

    /* Serialization array of role to the main role
       Because the current business is one user will have only one role
    */
    @Exclude()
    @ManyToMany(
        type => Role,
        role => role.users,
    )
    @JoinTable({ name: 'user_role' })
    roles: Role[];

    @Expose()
    get role(): string {
        return this.roles && this.roles.length ? this.roles[0].name : '';
    }

    @OneToMany(
        type => Product,
        product => product.createdBy,
    )
    products: Product[];

    @OneToMany(
        type => Category,
        category => category.createdBy,
    )
    categories: Category[];

    @OneToMany(
        type => CategoryDiscount,
        categoryDiscount => categoryDiscount.user,
    )
    categoryDiscounts: CategoryDiscount[];

    @OneToMany(
        type => Cart,
        cart => cart.user,
    )
    carts: Cart[];

    @OneToMany(
        type => CustomCartProduct,
        customCartProduct => customCartProduct.user,
    )
    customCartProducts: CustomCartProduct[];

    @OneToMany(
        type => Order,
        oder => oder.user,
    )
    orders: Order[];

    @OneToMany(
        type => Participants,
        participants => participants.user,
    )
    participants: Participants[];

    @OneToMany(
        type => ProductImport,
        prductImport => prductImport.createdBy,
    )
    productImports: ProductImport[];
}
