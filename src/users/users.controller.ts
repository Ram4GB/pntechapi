import { ApiBearerAuth, ApiNotFoundResponse, ApiTags } from '@nestjs/swagger';
import {
    BadRequestException,
    Body,
    Controller,
    Get,
    HttpCode,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
    Req,
} from '@nestjs/common';

import { AddUserToGroupDTO } from './dto/add-user-to-group.dto';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { CommonHelperService } from 'shared/helpers/common-helper.service';
import { CreateAdminDTO } from './dto/create-admin.dto';
import { CreateSellerDTO } from './dto/create-seller.dto';
import { PagedList } from 'shared/helpers/pagination';
import { Roles } from 'shared/constants/roles';
import { UpdateResult } from 'typeorm';
import { UpdateUserStatusDTO } from './dto/update-user-status.dto';
import { User } from './user.entity';
import { UserPaginateQuery } from './dto/user.query';
import { UserUpdateDTO } from './dto/user-update.dto';
import { UsersService } from './users.service';

@Controller('users')
@ApiTags('Users')
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
        private readonly commonHelperService: CommonHelperService,
    ) {}

    @Get('admins')
    @ApiBearerAuth()
    @Authorize(Roles.SuperAdmin)
    async getAdmins(
        @Query() query: UserPaginateQuery,
    ): Promise<PagedList<User>> {
        const customers = await this.usersService.listAdminsPaginated(query);
        return customers;
    }

    @Get('sellers')
    @ApiBearerAuth()
    @Authorize(Roles.Admin)
    async getSellers(
        @Query() query: UserPaginateQuery,
    ): Promise<PagedList<User>> {
        const customers = await this.usersService.listSellersPaginated(query);
        return customers;
    }

    @Get('customers')
    @ApiBearerAuth()
    @Authorize(Roles.Admin, Roles.Seller)
    async getCustomers(
        @Req() req: any,
        @Query() query: UserPaginateQuery,
    ): Promise<PagedList<User>> {
        if (req.user.role === Roles.Seller) {
            return this.usersService.listCustomersInGroupPaginated(
                query,
                req.user.id,
            );
        } else {
            return this.usersService.listCustomersPaginated(query);
        }
    }

    @Get('customers-not-in-group')
    @ApiBearerAuth()
    @Authorize(Roles.Admin)
    async getCustomersNotInGroup(
        @Query() query: UserPaginateQuery,
    ): Promise<PagedList<User>> {
        const customers = await this.usersService.listCustomersNotInGroupPaginated(
            query,
        );
        return customers;
    }

    @Authorize()
    @HttpCode(200)
    @Put('profile')
    async update(
        @Req() req: any,
        @Body() userUpdateInfo: UserUpdateDTO,
    ): Promise<UpdateResult> {
        const userEntity = await this.usersService.getOnlyUserInfoByEmail(
            req.user.email,
        );

        if (!userEntity) {
            throw new NotFoundException();
        }

        const validateResult = await this.usersService.validateUserBeforeUpdate(
            userUpdateInfo,
            userEntity,
        );

        if (!validateResult.isSuccess) {
            throw new BadRequestException(validateResult.errors);
        }

        const userUpdate = await this.usersService.prepareUserUpdateEntity(
            userUpdateInfo,
            userEntity,
        );

        this.commonHelperService.consoleLogObjectDepth(userUpdate);

        return await this.usersService.update(userUpdate);
    }

    @Post('admins')
    @Authorize(Roles.SuperAdmin)
    async createAdmin(@Body() createAminInfo: CreateAdminDTO): Promise<User> {
        const adminUser = new User({
            ...createAminInfo,
        });

        return this.usersService.createAdmin(adminUser);
    }

    @Post('sellers')
    @Authorize(Roles.Admin)
    async createSeller(
        @Body() createSellerInfo: CreateSellerDTO,
    ): Promise<User> {
        const sellerUser = new User({
            ...createSellerInfo,
        });

        return this.usersService.createSeller(sellerUser);
    }

    @Put('admins/status/:id')
    @HttpCode(204)
    @Authorize(Roles.SuperAdmin)
    async changeAdminStatus(
        @Param('id') id: string,
        @Body() updateStatusInfo: UpdateUserStatusDTO,
    ): Promise<any> {
        const user = await this.usersService.findById(id);

        if (!user) {
            throw new NotFoundException();
        }

        const isAdmin = user.roles.find(
            val => val.name === Roles.Admin.toString(),
        );
        if (!isAdmin) {
            throw new BadRequestException(
                'Bạn chỉ được phép chỉnh sửa trạng thái của account admin',
            );
        }

        /** Remove roles field to use only user entity to update */
        delete user.roles;

        /** Change user status */
        user.status = updateStatusInfo.status;

        const result = await this.usersService.update(user);

        return result;
    }

    @Put('sellers/status/:id')
    @HttpCode(204)
    @Authorize(Roles.Admin)
    async changeSellerStatus(
        @Param('id') id: string,
        @Body() updateStatusInfo: UpdateUserStatusDTO,
    ): Promise<any> {
        const user = await this.usersService.findById(id);

        if (!user) {
            throw new NotFoundException();
        }

        const isSeller = user.roles.find(
            val => val.name === Roles.Seller.toString(),
        );
        if (!isSeller) {
            throw new BadRequestException(
                'Bạn chỉ được phép chỉnh sửa trạng thái của account seller',
            );
        }

        /** Remove roles field to use only user entity to update */
        delete user.roles;

        /** Change user status */
        user.status = updateStatusInfo.status;

        const result = await this.usersService.update(user);

        return result;
    }

    @Put('customers/status/:id')
    @HttpCode(204)
    @Authorize(Roles.Admin)
    async changeCustomerStatus(
        @Param('id') id: string,
        @Body() updateStatusInfo: UpdateUserStatusDTO,
    ): Promise<any> {
        const user = await this.usersService.findById(id);

        if (!user) {
            throw new NotFoundException();
        }

        const isCustomer = user.roles.find(
            val => val.name === Roles.Customer.toString(),
        );
        if (!isCustomer) {
            throw new BadRequestException(
                'Bạn chỉ được phép chỉnh sửa trạng thái của account khách hàng',
            );
        }

        /** Remove roles field to use only user entity to update */
        delete user.roles;

        /** Change user status */
        user.status = updateStatusInfo.status;

        const result = await this.usersService.update(user);

        return result;
    }

    /**
     * @description Add a customer to group manage by seller
     */
    @ApiBearerAuth()
    @ApiNotFoundResponse({
        description: 'Response when user not exist',
        schema: {
            example: {
                statusCode: 404,
                message: 'Customer not found',
            },
        },
    })
    @Post('customers/group')
    @Authorize(Roles.Admin)
    async addCustomersToGroup(
        @Body() addUserToGroupDTO: AddUserToGroupDTO,
    ): Promise<void> {
        const seller = await this.usersService.findById(
            addUserToGroupDTO.sellerId,
        );

        const isCustomer = await this.usersService.usersIsInRole(
            addUserToGroupDTO.customerIds,
            Roles.Customer,
        );

        const isSeller =
            !!seller &&
            (await this.usersService.isInRole(seller, Roles.Seller));

        if (!isCustomer) {
            throw new NotFoundException('Customer not found');
        }
        if (!isSeller) {
            throw new NotFoundException('Seller not found');
        }

        await this.usersService.addCustomersToGroup(
            addUserToGroupDTO.customerIds,
            addUserToGroupDTO.sellerId,
        );
    }
}
