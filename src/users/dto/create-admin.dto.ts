import { IsEmail, IsNotEmpty, IsNumberString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class CreateAdminDTO {
    @ApiProperty()
    @IsEmail()
    @IsNotEmpty({
        message: 'Email không được để trống',
    })
    readonly email: string;

    @ApiProperty()
    @IsNotEmpty({
        message: 'Tên không được để trống',
    })
    readonly firstName: string;

    @ApiProperty()
    @IsNotEmpty({
        message: 'Họ không được để trống',
    })
    readonly lastName: string;

    @ApiProperty()
    @IsNotEmptyString({
        message: 'Số điện thoại không được để trống',
    })
    @IsNumberString()
    readonly phone: string;
}
