import {
    IsNotEmpty,
    IsNumberString,
    IsOptional,
    ValidateIf,
} from 'class-validator';
export class UserUpdateDTO {
    @IsOptional()
    firstName: string;

    @IsOptional()
    lastName: string;

    @IsOptional()
    @IsNumberString()
    phone: string;

    @IsOptional() password: string;

    @ValidateIf(
        o =>
            o.password !== '' &&
            o.password !== null &&
            o.password !== undefined,
    )
    @IsNotEmpty({
        message: 'Xin hãy nhập xác nhận mật khẩu mới',
    })
    confirmPassword: string;

    @ValidateIf(
        o =>
            o.password !== '' &&
            o.password !== null &&
            o.password !== undefined,
    )
    @IsNotEmpty({
        message: 'Xin hãy nhập mật khẩu cũ của bạn',
    })
    oldPassword: string;
}
