import { ApiProperty } from '@nestjs/swagger';
import { IsIn } from 'class-validator';
import { UserStatus } from 'users/constant/userstatus';

export class UpdateUserStatusDTO {
    @IsIn([UserStatus.Banned, UserStatus.Active], {
        message: 'Trạng thái thay đổi không hợp lệ',
    })
    @ApiProperty()
    status: UserStatus;
}
