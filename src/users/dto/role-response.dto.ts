import { Expose } from 'class-transformer';
export class RoleResponseDTO {
    @Expose() readonly id: string;
    @Expose() readonly name: string;
}
