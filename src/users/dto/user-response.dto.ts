import { Expose, Type } from 'class-transformer';
import { RoleResponseDTO } from './role-response.dto';
export class UserResponseDTO {
    @Expose() readonly id: string;
    @Expose() readonly email: string;
    @Expose() readonly firstName: string;
    @Expose() readonly lastName: string;
    @Expose() readonly phone: string;
    @Expose() @Type(() => RoleResponseDTO) readonly roles: RoleResponseDTO[];
}
