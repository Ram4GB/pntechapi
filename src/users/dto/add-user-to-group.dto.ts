import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class AddUserToGroupDTO {
    @ApiProperty({
        example: [
            '0d53d33b-f023-426a-a116-1277ae0c585e',
            '7afbb2c9-26d1-4e24-a1f1-1a37fbb7fc4b',
            '7e4d0dba-443d-474a-a49a-9b5abb5c89a3',
        ],
    })
    @IsUUID('4', {
        each: true,
    })
    customerIds: string[];

    @ApiProperty({
        example: '817d1f84-dcd4-4713-9da8-f8e931c33440',
    })
    @IsUUID()
    sellerId: string;
}
