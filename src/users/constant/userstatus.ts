export enum UserStatus {
    Inactive = 'inactive',
    Active = 'active',
    Banned = 'banned',
    Deleted = 'deleted',
}
