import { Module, forwardRef } from '@nestjs/common';

import { AuthModule } from 'auth/auth.module';
import { CartsModule } from 'carts/carts.module';
import { HelpersModule } from 'shared/helpers/helpers.module';
import { Role } from './role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserClaim } from './userclaim.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
@Module({
    imports: [
        TypeOrmModule.forFeature([User, UserClaim, Role]),
        HelpersModule,
        forwardRef(() => AuthModule),
        forwardRef(() => CartsModule),
    ],
    controllers: [UsersController],
    providers: [UsersService],
    exports: [UsersService],
})
export class UsersModule {}
