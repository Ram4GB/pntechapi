import { CategoryDiscount } from './categorydiscount.entity';
import { CategorydiscountsService } from './categorydiscounts.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    providers: [CategorydiscountsService],
    imports: [TypeOrmModule.forFeature([CategoryDiscount])],
})
export class CategorydiscountsModule {}
