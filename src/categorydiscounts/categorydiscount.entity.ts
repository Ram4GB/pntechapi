import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Category } from 'categories/category.entity';
import { Expose } from 'class-transformer';
import { User } from 'users/user.entity';

@Entity()
export class CategoryDiscount {
    constructor(partial: Partial<CategoryDiscount>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    @Expose()
    id: string;

    @Column()
    @Expose()
    discountPercentage: number;

    @ManyToOne(
        type => Category,
        category => category.categoryDiscounts,
    )
    category: Category;

    @ManyToOne(
        type => User,
        user => user.categoryDiscounts,
    )
    user: User;
}
