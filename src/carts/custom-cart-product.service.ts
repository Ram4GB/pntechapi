import * as momentTimezone from 'moment-timezone';

import { DeleteResult, Repository } from 'typeorm';

import { CommonConfig } from 'shared/constants/common';
import { CustomCartProduct } from './custom-cart-product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { ProductsService } from 'products/products.service';

@Injectable()
export class CustomCartProductsService {
    constructor(
        @InjectRepository(CustomCartProduct)
        private customCartProductsRepository: Repository<CustomCartProduct>,
        private readonly productsService: ProductsService,
    ) {}

    async addCustomProductToCart(
        customCartProduct: CustomCartProduct,
    ): Promise<CustomCartProduct> {
        const customProductInCart = await this.getCustomCartProductInCartByName(
            customCartProduct.name,
        );

        if (customProductInCart) {
            /** Existed in cart => update quantity */
            customProductInCart.quantity += customCartProduct.quantity;
            customProductInCart.lastUpdatedAt = momentTimezone()
                .tz(CommonConfig.TimeZoneMoment)
                .toDate();
            return await this.update(customProductInCart);
        } else {
            /** NOT existed in cart => create new */
            customCartProduct.createdAt = momentTimezone()
                .tz(CommonConfig.TimeZoneMoment)
                .toDate();
            customCartProduct.lastUpdatedAt = momentTimezone()
                .tz(CommonConfig.TimeZoneMoment)
                .toDate();
            return await this.create(customCartProduct);
        }
    }

    async findById(id: string): Promise<CustomCartProduct> {
        return await this.customCartProductsRepository.findOne({
            where: { id },
        });
    }

    async create(
        customCartProduct: CustomCartProduct,
    ): Promise<CustomCartProduct> {
        return await this.customCartProductsRepository.save(customCartProduct);
    }

    async update(
        customCartProduct: CustomCartProduct,
    ): Promise<CustomCartProduct> {
        return await this.customCartProductsRepository.save(customCartProduct);
    }

    async getCustomCartProductInCartByName(
        name: string,
    ): Promise<CustomCartProduct> {
        return await this.customCartProductsRepository
            .createQueryBuilder('customCartProduct')
            .where('name = :name', {
                name,
            })
            .getOne();
    }

    async delete(customCartProduct: CustomCartProduct): Promise<DeleteResult> {
        return await this.customCartProductsRepository.delete(
            customCartProduct.id,
        );
    }

    async clearAllCustomCartProductsInCart(userId: string): Promise<void> {
        await this.customCartProductsRepository
            .createQueryBuilder()
            .delete()
            .where('userId = :userId', {
                userId,
            })
            .execute();
    }
}
