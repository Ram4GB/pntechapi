import * as momentTimezone from 'moment-timezone';

import { DeleteResult, Repository } from 'typeorm';

import { Cart } from './cart.entity';
import { CommonConfig } from 'shared/constants/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Media } from 'media/media.entity';
import { MediaUsage } from 'media/media-usage.entity';

@Injectable()
export class CartsService {
    constructor(
        @InjectRepository(Cart)
        private cartsRepository: Repository<Cart>,
    ) {}

    async addToCart(cart: Cart): Promise<Cart> {
        const productInCart = await this.getProductInCart(
            cart.product.id,
            cart.user.id,
        );

        if (productInCart) {
            /** Existed in cart => update quantity */
            productInCart.quantity += cart.quantity;
            productInCart.lastUpdatedAt = momentTimezone()
                .tz(CommonConfig.TimeZoneMoment)
                .toDate();
            const updatedCart = await this.update(productInCart);
            return await this.findById(updatedCart.id);
        } else {
            /** NOT existed in cart => create new */
            cart.createdAt = momentTimezone()
                .tz(CommonConfig.TimeZoneMoment)
                .toDate();
            cart.lastUpdatedAt = momentTimezone()
                .tz(CommonConfig.TimeZoneMoment)
                .toDate();
            const result = await this.create(cart);
            return await this.findById(result.id);
        }
    }

    async findById(id: string): Promise<Cart> {
        const result = await this.cartsRepository
            .createQueryBuilder('Cart')
            .leftJoinAndSelect('Cart.user', 'User')
            .leftJoin('Cart.product', 'Product')
            .addSelect([
                'Product.id',
                'Product.name',
                'Product.generalDescription',
                'Product.technicalDescription',
                'Product.category',
            ])
            .leftJoinAndSelect('Product.category', 'Category')
            .leftJoinAndSelect(
                MediaUsage,
                'MediaUsage',
                "Product.id = MediaUsage.usedById AND MediaUsage.type = 'product'",
            )
            .leftJoinAndMapOne(
                'Product.media',
                Media,
                'Media',
                'MediaUsage.mediaId = Media.id',
            )
            .andWhere('MediaUsage.weight = 1 OR MediaUsage.weight IS NULL')
            .where('Cart.id = :id', { id })
            .getOne();
        return result;
    }

    async create(cart: Cart): Promise<Cart> {
        return await this.cartsRepository.save(cart);
    }

    async update(cart: Cart): Promise<Cart> {
        return await this.cartsRepository.save(cart);
    }

    async isProductExistedIncart(
        productId: string,
        userId: string,
    ): Promise<boolean> {
        const cartDetail = await this.cartsRepository
            .createQueryBuilder('cart')
            .where('productId = :productId', {
                productId,
            })
            .andWhere('userId = :userId', {
                userId,
            })
            .getOne();

        if (!cartDetail) {
            return false;
        }

        return true;
    }

    async getProductInCart(productId: string, userId: string): Promise<Cart> {
        return await this.cartsRepository
            .createQueryBuilder('cart')
            .where('productId = :productId', {
                productId,
            })
            .andWhere('userId = :userId', {
                userId,
            })
            .getOne();
    }

    async delete(cart: Cart): Promise<DeleteResult> {
        return await this.cartsRepository.delete(cart.id);
    }

    async clearAllProductsInCarts(userId: string): Promise<void> {
        await this.cartsRepository
            .createQueryBuilder()
            .delete()
            .where('userId = :userId', {
                userId,
            })
            .execute();
    }
}
