import { Module, forwardRef } from '@nestjs/common';

import { Cart } from './cart.entity';
import { CartsController } from './carts.controller';
import { CartsService } from './carts.service';
import { CustomCartProduct } from './custom-cart-product.entity';
import { CustomCartProductsService } from './custom-cart-product.service';
import { ProductsModule } from 'products/products.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'users/users.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([Cart, CustomCartProduct]),
        forwardRef(() => ProductsModule),
        forwardRef(() => UsersModule),
    ],
    controllers: [CartsController],
    providers: [CartsService, CustomCartProductsService],
    exports: [CartsService, CustomCartProductsService],
})
export class CartsModule {}
