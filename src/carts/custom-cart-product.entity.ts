import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Expose } from 'class-transformer';
import { User } from 'users/user.entity';
@Entity()
export class CustomCartProduct {
    constructor(partial: Partial<CustomCartProduct>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Expose()
    name: string;

    @Column()
    @Expose()
    quantity: number;

    @Column()
    createdAt: Date;

    @Column()
    lastUpdatedAt: Date;

    @ManyToOne(
        type => User,
        user => user.customCartProducts,
    )
    user: User;
}
