import { IsInt, IsOptional, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class UpdateCustomProductDTO {
    @ApiProperty()
    @IsOptional()
    @Transform(quantity => parseInt(quantity), { toClassOnly: true })
    @IsInt({
        message: 'Số lượng thêm vào giỏ phải là số nguyên',
    })
    @Min(1, {
        message: 'Số lượng thêm vào giỏ không được bé hơn 1',
    })
    quantity: number;

    @ApiProperty()
    @IsOptional()
    name: string;
}
