import { Expose, Type } from 'class-transformer';
import { ProductResponseDTO } from 'products/dto/product-response.dto';
import { UserResponseDTO } from 'users/dto/user-response.dto';

export class CartResponseDTO {
    @Expose()
    id: string;

    @Expose()
    quantity: number;

    @Expose()
    createdAt: Date;

    @Expose()
    lastUpdatedAt: Date;

    @Expose()
    @Type(() => ProductResponseDTO)
    product: ProductResponseDTO;

    @Expose()
    @Type(() => UserResponseDTO)
    user: UserResponseDTO;
}
