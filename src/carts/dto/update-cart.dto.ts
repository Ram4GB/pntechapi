import { IsInt, IsNotEmpty, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class UpdateCartDTO {
    @ApiProperty()
    @IsNotEmpty({
        message: 'Xin vui lòng nhập số lượng muốn thêm vào giỏ',
    })
    @Transform(quantity => parseInt(quantity), { toClassOnly: true })
    @IsInt({
        message: 'Số lượng thêm vào giỏ phải là số nguyên',
    })
    @Min(1, {
        message: 'Số lượng thêm vào giỏ không được bé hơn 1',
    })
    readonly quantity: number;
}
