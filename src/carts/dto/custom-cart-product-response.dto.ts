import { Expose } from 'class-transformer';

export class CustomCartProductResponseDTO {
    @Expose()
    id: string;

    @Expose()
    quantity: number;

    @Expose()
    name: string;

    @Expose()
    createdAt: Date;

    @Expose()
    lastUpdatedAt: Date;
}
