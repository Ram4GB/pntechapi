import { Expose, Type } from 'class-transformer';
import { CartResponseDTO } from './cart-response.dto';
import { CustomCartProductResponseDTO } from './custom-cart-product-response.dto';

export class GetUserCartResponseDTO {
    @Expose()
    @Type(() => CartResponseDTO)
    carts: CartResponseDTO[];

    @Expose()
    @Type(() => CustomCartProductResponseDTO)
    customCartProducts: CustomCartProductResponseDTO[];
}
