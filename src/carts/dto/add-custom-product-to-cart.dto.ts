import { IsInt, IsNotEmpty, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class AddCustomProductToCartDTO {
    @ApiProperty()
    @IsNotEmpty({
        message: 'Tên sản phẩm thêm vào giỏ không được để trống',
    })
    readonly name: string;

    @ApiProperty()
    @IsNotEmpty({
        message: 'Xin vui lòng nhập số lượng muốn thêm vào giỏ',
    })
    @Type(() => Number)
    @IsInt({
        message: 'Số lượng thêm vào giỏ phải là số nguyên',
    })
    @Min(1, {
        message: 'Số lượng thêm vào giỏ không được bé hơn 1',
    })
    readonly quantity: number;
}
