import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Expose } from 'class-transformer';
import { Product } from 'products/products.entity';
import { User } from 'users/user.entity';

@Entity()
export class Cart {
    constructor(partial: Partial<Cart>) {
        Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Expose()
    quantity: number;

    @Column()
    createdAt: Date;

    @Column()
    lastUpdatedAt: Date;

    @ManyToOne(
        _ => User,
        user => user.carts,
    )
    user: User;

    @ManyToOne(
        type => Product,
        product => product.carts,
    )
    product: Product;
}
