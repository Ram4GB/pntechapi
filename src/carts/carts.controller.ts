import { ApiResponse, ApiTags } from '@nestjs/swagger';
import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Inject,
    NotFoundException,
    Param,
    Post,
    Put,
    Req,
    UnauthorizedException,
    forwardRef,
} from '@nestjs/common';
import { plainToClass, plainToClassFromExist } from 'class-transformer';

import { AddCustomProductToCartDTO } from './dto/add-custom-product-to-cart.dto';
import { AddToCartDTO } from './dto/add-to-cart.dto';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { Cart } from './cart.entity';
import { CartResponseDTO } from './dto/cart-response.dto';
import { CartsService } from './carts.service';
import { CustomCartProduct } from './custom-cart-product.entity';
import { CustomCartProductResponseDTO } from './dto/custom-cart-product-response.dto';
import { CustomCartProductsService } from './custom-cart-product.service';
import { DeleteResult } from 'typeorm';
import { ProductsService } from 'products/products.service';
import { Roles } from 'shared/constants/roles';
import { UpdateCartDTO } from './dto/update-cart.dto';
import { UpdateCustomProductDTO } from './dto/update-custom-product.dto';
import { User } from 'users/user.entity';
import { UsersService } from 'users/users.service';

@Controller('carts')
@ApiTags('Carts')
export class CartsController {
    constructor(
        private readonly cartsService: CartsService,
        private readonly customCartProductsService: CustomCartProductsService,
        private readonly productsService: ProductsService,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
    ) {}

    @Authorize(Roles.Customer)
    @Post()
    @HttpCode(201)
    @ApiResponse({
        status: 400,
        description: 'Response when validation request body failed',
        schema: {
            example: {
                statusCode: 400,
                message: {
                    quantity:
                        'Xin vui lòng nhập số lượng muốn thêm vào giỏ\nSố lượng thêm vào giỏ phải là số nguyên\nSố lượng thêm vào giỏ không được bé hơn 1',
                    productId:
                        'Xin vui lòng nhập  id của sản phẩm cần thêm vào giỏ hàng',
                },
            },
        },
    })
    @ApiResponse({
        status: 201,
        description: 'Response when add to cart successful',
        schema: {
            example: {
                id: 'b4fb4cf7-78d4-4295-97bc-e08497fd0370',
                quantity: 8,
                createdAt: '2020-08-07T16:55:11.294Z',
                lastUpdatedAt: '2020-08-07T16:55:11.318Z',
                product: {
                    id: '030e245a-b32a-4d63-93ae-86241e3aa01b',
                    name: 'product testing 1',
                    generalDescription: 'Nothing long',
                    technicalDescription: 'Nothing long',
                    note: 'This is note',
                    price: 3000000,
                    quantity: 0,
                    createdAt: '2020-08-07T09:48:26.920Z',
                    productImages: [],
                    category: {
                        id: '1d0f1c16-dca8-4742-830d-98c216da9c21',
                        name: 'category test 1',
                        createdAt: '2020-08-07T09:48:14.323Z',
                    },
                },
                user: {
                    id: '913767e6-c22c-41c9-9c0a-f2006f74a16c',
                    email: 'johndoe@gmail.com',
                    firstName: 'John',
                    lastName: 'Doe',
                    phone: '0987654321',
                },
            },
        },
    })
    async addToCart(
        @Body() addToCartInfo: AddToCartDTO,
        @Req() req: any,
    ): Promise<Cart> {
        /** Get userto check valid and get cart of user too */
        const user = await this.usersService.getOnlyUserInfoByEmail(
            req.user.email,
        );
        if (!user) {
            throw new UnauthorizedException();
        }

        /** Check valid product */
        const product = await this.productsService.findById(
            addToCartInfo.productId,
        );
        if (!product) {
            throw new NotFoundException();
        }

        const cart = new Cart({
            quantity: addToCartInfo.quantity,
            product,
            user,
        });

        const addedCart = await this.cartsService.addToCart(cart);

        return addedCart;
    }

    @Authorize(Roles.Customer)
    @Post('customProducts')
    @HttpCode(201)
    async addCustomProductToCart(
        @Body() addCustomProductToCartInfo: AddCustomProductToCartDTO,
        @Req() req: any,
    ): Promise<CustomCartProductResponseDTO> {
        /** Get userto check valid and get cart of user too */
        const user = await this.usersService.getOnlyUserInfoByEmail(
            req.user.email,
        );
        if (!user) {
            throw new UnauthorizedException();
        }

        const customCartProduct = new CustomCartProduct({
            quantity: addCustomProductToCartInfo.quantity,
            name: addCustomProductToCartInfo.name,
            user,
        });

        const addedCustomCartProduct = await this.customCartProductsService.addCustomProductToCart(
            customCartProduct,
        );
        const customCartProductResponse = plainToClass(
            CustomCartProductResponseDTO,
            addedCustomCartProduct,
            {
                excludeExtraneousValues: true,
            },
        );

        return customCartProductResponse;
    }

    @Authorize(Roles.Customer)
    @Get()
    @HttpCode(200)
    async getAll(@Req() req: any): Promise<User> {
        const user = await this.usersService.getOnlyUserInfoById(req.user.id);
        if (!user) {
            throw new UnauthorizedException();
        }

        const result = await this.usersService.getAllUserCartsAndCustomProductsInCart(
            user.id,
        );

        return result;
    }

    @Authorize(Roles.Customer)
    @Put(':id')
    @HttpCode(204)
    async updateCart(
        @Param('id') id: string,
        @Body() updateInfo: UpdateCartDTO,
        @Req() req: any,
    ): Promise<CartResponseDTO> {
        /** Get userto check valid */
        const user = await this.usersService.getOnlyUserInfoByEmail(
            req.user.email,
        );
        if (!user) {
            throw new UnauthorizedException();
        }

        /** Get and check cart */
        const cart = await this.cartsService.findById(id);
        if (!cart) {
            throw new NotFoundException();
        }
        // Update quantity
        cart.quantity = updateInfo.quantity;

        const updatedCart = this.cartsService.update(cart);
        const cartResponse = plainToClass(CartResponseDTO, updatedCart, {
            excludeExtraneousValues: true,
        });

        return cartResponse;
    }

    @Authorize(Roles.Customer)
    @Put('customProducts/:id')
    @HttpCode(204)
    async updateCustomProduct(
        @Param('id') id: string,
        @Body() updateInfo: UpdateCustomProductDTO,
        @Req() req: any,
    ): Promise<any> {
        /** Get user to check valid */
        const user = await this.usersService.getOnlyUserInfoByEmail(
            req.user.email,
        );
        if (!user) {
            throw new UnauthorizedException();
        }

        /** Get and check custom product */
        const customProduct = await this.customCartProductsService.findById(id);
        if (!customProduct) {
            throw new NotFoundException();
        }

        const newCustomProduct = plainToClassFromExist(
            customProduct,
            updateInfo,
        );

        const updatedCustomProduct = this.customCartProductsService.update(
            newCustomProduct,
        );
        const customCartProductResponse = plainToClass(
            CustomCartProductResponseDTO,
            updatedCustomProduct,
            {
                excludeExtraneousValues: true,
            },
        );

        return customCartProductResponse;
    }

    @Authorize(Roles.Customer)
    @Delete(':id')
    @HttpCode(204)
    async deleteCart(@Param('id') id: string): Promise<DeleteResult> {
        /** Get and check cart */
        const cart = await this.cartsService.findById(id);
        if (!cart) {
            throw new NotFoundException();
        }

        const result = this.cartsService.delete(cart);

        return result;
    }

    @Authorize(Roles.Customer)
    @Delete('customProducts/:id')
    @HttpCode(204)
    async deleteCustomProduct(@Param('id') id: string): Promise<DeleteResult> {
        /** Get and check cart */
        const customProduct = await this.customCartProductsService.findById(id);
        if (!customProduct) {
            throw new NotFoundException();
        }

        const result = this.customCartProductsService.delete(customProduct);

        return result;
    }
}
