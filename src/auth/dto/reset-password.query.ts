import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class ResetPasswordQuery {
    @ApiProperty({
        description: 'Token in email',
        example: '',
    })
    @IsNotEmptyString()
    token: string;
}
