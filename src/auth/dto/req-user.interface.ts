import { UserStatus } from 'users/constant/userstatus';

export interface RequestUser {
    id: string;
    email: string;
    role: string;
    status: UserStatus;
}
