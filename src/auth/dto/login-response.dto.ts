import { Expose } from 'class-transformer';

export class LoginResponseDTO {
    @Expose({ name: 'expires_in' })
    expiresIn: string;

    @Expose({ name: 'access_token' })
    accessToken: string;
}
