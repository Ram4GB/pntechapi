import { IsEmail, IsNumberString, Length } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class RegisterDTO {
    @ApiProperty({
        required: true,
        example: 'tonystark09@gmail.com',
    })
    @IsEmail()
    @IsNotEmptyString()
    readonly email: string;

    @ApiProperty({
        required: true,
        example: 'Tony',
    })
    @IsNotEmptyString()
    readonly firstName: string;

    @ApiProperty({
        required: true,
        example: 'Stark',
    })
    @IsNotEmptyString()
    readonly lastName: string;

    @ApiProperty({
        required: true,
        example: '0909090909',
    })
    @IsNotEmptyString()
    @IsNumberString()
    readonly phone: string;

    @ApiProperty({
        required: true,
        example: '12345678',
    })
    @IsNotEmptyString()
    @Length(8)
    readonly password: string;

    @ApiProperty({
        description: 'Verify url after register',
        example: 'http://localhost:5000/verify-email',
    })
    @IsNotEmptyString()
    redirectUrl: string;
}
