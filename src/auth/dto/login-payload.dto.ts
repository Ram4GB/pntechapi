import { IsEmail, Length } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class LoginPayloadDTO {
    @ApiProperty({
        required: true,
        example: 'tonystark09@gmail.com',
    })
    @IsEmail()
    @IsNotEmptyString()
    readonly email: string;

    @ApiProperty({
        required: true,
        example: 'tonystark123',
    })
    @IsNotEmptyString()
    @Length(8)
    readonly password: string;
}
