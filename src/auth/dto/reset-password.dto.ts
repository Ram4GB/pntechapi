import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';
import { Length } from 'class-validator';

export class ResetPasswordDTO {
    @ApiProperty({
        description: 'New password',
        example: 'thisisanewpass',
    })
    @IsNotEmptyString()
    @Length(8)
    newPassword: string;
}
