import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';
import { IsNotEmptyString } from 'shared/decorators/is-not-empty-string.decorator';

export class RequestResetPasswordDTO {
    @ApiProperty({
        required: true,
        example: 'tonystark09@gmail.com',
    })
    @IsEmail()
    @IsNotEmptyString()
    readonly email: string;

    @ApiProperty({
        description: 'Verify url after register',
        example: 'http://localhost:5000/reset-password',
    })
    @IsNotEmptyString()
    redirectUrl: string;
}
