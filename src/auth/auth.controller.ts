import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiCreatedResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
    Body,
    Controller,
    Get,
    HttpCode,
    NotFoundException,
    Post,
    Query,
    Request,
} from '@nestjs/common';

import { AuthService } from './auth.service';
import { Authorize } from 'shared/decorators/authorize.decorator';
import { LoginPayloadDTO } from './dto/login-payload.dto';
import { RegisterDTO } from './dto/register.dto';
import { RequestResetPasswordDTO } from './dto/request-reset-password.dto';
import { ResetPasswordDTO } from './dto/reset-password.dto';
import { ResetPasswordQuery } from './dto/reset-password.query';
import { TokenTypes } from 'shared/constants/token-types';
import { User } from 'users/user.entity';
import { UserStatus } from 'users/constant/userstatus';
import { UsersService } from 'users/users.service';
import { VerifyDTO } from './dto/verify.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
    constructor(
        private authService: AuthService,
        private usersService: UsersService,
    ) {}

    @ApiUnauthorizedResponse({
        status: 401,
        description: 'Response when email or password is incorrect',
        schema: {
            example: {
                statusCode: 401,
                message:
                    'Vui lòng kiểm tra email để kích hoạt tài khoản của bạn',
            },
        },
    })
    @ApiOkResponse({
        description: 'Response when login successfully',
        schema: {
            example: {
                expiresIn: '7d',
                accessToken:
                    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImY0YWNiNGRhLTM1NjAtNDkxMi05NzkwLTc3ZjA4YjBiZTU1OSIsImVtYWlsIjoibGlsc3VwZXJhZG1pbkBnbWFpbC5jb20iLCJzdGF0dXMiOiJhY3RpdmUiLCJyb2xlIjoic3VwZXJhZG1pbiIsImlhdCI6MTU5Njk5Mjk1NywiZXhwIjoxNTk3NTk3NzU3fQ.Pa_USU05JAoAqfpQY8lwXfchAQMtsseEzHYg1hY96sM',
            },
        },
    })
    @Post('login')
    async login(@Body() loginPayload: LoginPayloadDTO) {
        return this.authService.login(loginPayload);
    }

    @Post('register')
    @HttpCode(201)
    @ApiBadRequestResponse({
        description: 'Response when validation request body failed',
        schema: {
            example: {
                statusCode: 400,
                message: {
                    email: 'email should not be empty\nemail must be an email',
                    firstName: 'firstName should not be empty',
                    lastName: 'lastName should not be empty',
                    phone: 'phone should not be empty',
                    password: 'password should not be empty',
                },
            },
        },
    })
    @ApiCreatedResponse({
        description: 'Response when successful registration',
        schema: {
            example: {
                id: '530160ea-5693-4a42-b9df-f5cd93e96d4d',
                email: 'huyquyen3t@gmail.com',
                firstName: 'quyen',
                lastName: 'tran',
                phone: '0969442751',
                status: 'active',
                createdAt: '2020-08-07T03:24:55.681Z',
                updatedAt: '2020-08-07T03:24:55.681Z',
                role: 'customer',
            },
        },
    })
    async register(@Body() registerInfo: RegisterDTO): Promise<User> {
        const user = new User({
            ...registerInfo,
            passwordHash: registerInfo.password,
        });

        const newUser = await this.authService.register(
            user,
            registerInfo.redirectUrl,
        );

        return newUser;
    }

    @ApiBearerAuth()
    @ApiOkResponse({
        schema: {
            example: {
                id: 'f4acb4da-3560-4912-9790-77f08b0be559',
                email: 'lilsuperadmin@gmail.com',
                firstName: 'Super Admin',
                lastName: 'Lil',
                phone: '0111111111',
                status: 'active',
                createdAt: '2020-08-08T00:04:50.648Z',
                updatedAt: '2020-08-08T00:04:50.648Z',
                role: 'superadmin',
            },
        },
    })
    @Get('profile')
    @Authorize()
    async getProfile(@Request() req): Promise<User> {
        const user = await this.usersService.findUserByEmail(req.user.email);

        if (!user) {
            throw new NotFoundException();
        }

        return user;
    }

    @ApiOkResponse({
        schema: {
            example: {
                id: 'f4acb4da-3560-4912-9790-77f08b0be559',
                email: 'lilsuperadmin@gmail.com',
                firstName: 'Super Admin',
                lastName: 'Lil',
                phone: '0111111111',
                status: 'active',
                createdAt: '2020-08-08T00:04:50.648Z',
                updatedAt: '2020-08-08T00:04:50.648Z',
                role: 'superadmin',
            },
        },
    })
    @ApiBadRequestResponse({
        description: 'Response when token is invalid',
        schema: {
            example: {
                statusCode: 400,
                message: 'Email xác thực đã hết hạn',
            },
        },
    })
    @Get('verify')
    async verify(@Query() verifyDTO: VerifyDTO): Promise<User> {
        const userInfo = await this.authService.verifyToken(
            verifyDTO.token,
            TokenTypes.VerifyEmailToken,
        );

        const user = await this.usersService.findById(userInfo.id);
        if (!user) {
            throw new NotFoundException();
        }

        /** Remove roles field to use only user entity to update */
        delete user.roles;

        user.status = UserStatus.Active;
        await this.usersService.update(user);

        return user;
    }

    @ApiNotFoundResponse({
        description: 'Response when email not exist',
        schema: {
            example: {
                statusCode: 404,
                message: 'Email không tồn tại trong hệ thống',
            },
        },
    })
    @Post('request-reset-password')
    async requestResetPassword(
        @Body() resetPasswordDTO: RequestResetPasswordDTO,
    ): Promise<void> {
        const user = await this.usersService.findUserByEmail(
            resetPasswordDTO.email,
        );
        if (!user) {
            throw new NotFoundException('Email không tồn tại trong hệ thống');
        }

        this.authService.requestResetPassword(
            user,
            resetPasswordDTO.redirectUrl,
        );
    }

    @Post('reset-password')
    async resetPassword(
        @Query() resetPasswordQuery: ResetPasswordQuery,
        @Body() resetPasswordDTO: ResetPasswordDTO,
    ): Promise<void> {
        const userInfo = await this.authService.verifyToken(
            resetPasswordQuery.token,
            TokenTypes.ResetPasswordToken,
        );

        const user = await this.usersService.findById(userInfo.id);
        if (!user) {
            throw new NotFoundException();
        }

        /** Remove roles field to use only user entity to update */
        delete user.roles;

        await this.authService.resetPassword(
            user,
            resetPasswordDTO.newPassword,
        );
    }
}
