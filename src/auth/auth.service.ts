import * as bcrypt from 'bcrypt';

import {
    BadRequestException,
    Inject,
    Injectable,
    Logger,
    UnauthorizedException,
    forwardRef,
} from '@nestjs/common';

import { ApiError } from 'shared/helpers/api.error';
import { CommonConfig } from 'shared/constants/common';
import { ConfigService } from '@nestjs/config';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { LoginPayloadDTO } from './dto/login-payload.dto';
import { LoginResponseDTO } from './dto/login-response.dto';
import { MailSubject } from 'shared/constants/mail-subject';
import { MailerService } from '@nestjs-modules/mailer';
import { RequestUser } from './dto/req-user.interface';
import { Roles } from 'shared/constants/roles';
import { TokenTypes } from 'shared/constants/token-types';
import { User } from 'users/user.entity';
import { UserStatus } from 'users/constant/userstatus';
import { UsersService } from 'users/users.service';

@Injectable()
export class AuthService {
    constructor(
        @InjectConnection()
        private connection: Connection,
        private readonly configService: ConfigService,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
        private readonly mailerService: MailerService,
    ) {}

    async hashPassword(password: string): Promise<string> {
        return await bcrypt.hash(password, CommonConfig.SALT_ROUNDS);
    }

    generateAccessToken(user: User): LoginResponseDTO {
        const payload: RequestUser = {
            id: user.id,
            email: user.email,
            status: user.status,
            role: user.roles[0].name,
        };

        const accessToken = this.jwtService.sign(payload);
        return {
            expiresIn: this.configService.get<string>('JWT_EXPIRATION_TIME'),
            accessToken: accessToken,
        };
    }

    generateToken(user: User, type: TokenTypes): string {
        const payload = {
            id: user.id,
            email: user.email,
            tokenType: type,
        };
        this.jwtService.verify;
        const token = this.jwtService.sign(payload, {
            expiresIn: this.configService.get<string>(
                'VERIFICATION_MAIL_EXPIRATION_TIME',
            ),
        });
        return token;
    }

    async verifyToken(token: string, type: TokenTypes): Promise<any> {
        try {
            const decoded = await this.jwtService.verifyAsync(token, {
                secret: this.configService.get<string>('JWT_SECRET_KEY'),
                ignoreExpiration: false,
            });
            if (decoded.tokenType !== type) {
                throw new BadRequestException('Token không hợp lệ');
            }

            return decoded;
        } catch (error) {
            throw new BadRequestException('Email xác thực đã hết hạn');
        }
    }

    async login(loginPayload: LoginPayloadDTO): Promise<LoginResponseDTO> {
        const { email, password } = loginPayload;

        const user = await this.usersService.findUserByEmail(email);
        if (!user) {
            throw new UnauthorizedException(
                'Email hoặc mật khẩu không chính xác',
            );
        }
        const isValid = await bcrypt.compare(password, user.passwordHash);
        if (!isValid) {
            throw new UnauthorizedException(
                'Email hoặc mật khẩu không chính xác',
            );
        }
        if (user.status === UserStatus.Inactive) {
            throw new UnauthorizedException(
                'Vui lòng kiểm tra email để kích hoạt tài khoản của bạn',
            );
        }
        if (user.status === UserStatus.Banned) {
            throw new UnauthorizedException(
                'Tài khoản của bạn đã bị cấm, Vui lòng liên hệ chúng tôi để biết thêm chi tiết',
            );
        }
        if (user.status !== UserStatus.Active) {
            throw new UnauthorizedException();
        }

        const response = this.generateAccessToken(user);
        return response;
    }

    async register(user: User, redirectUrl: string): Promise<User> {
        await this.validateValidNewUser(user);

        let createdUser;
        await this.connection.transaction(async manager => {
            createdUser = await this.usersService.createUser(user, manager);
            await this.usersService.addToRole(
                createdUser,
                Roles.Customer,
                manager,
            );
        });

        const token = this.generateToken(
            createdUser,
            TokenTypes.VerifyEmailToken,
        );

        const linkVerifyWithToken = `${redirectUrl}?token=${token}`;

        this.mailerService
            .sendMail({
                to: createdUser.email,
                subject: MailSubject.VERIFICATION_MAIL,
                template: 'verification-mail',
                context: {
                    name: `${createdUser.lastName} ${createdUser.firstName}`,
                    linkVerifyWithToken,
                },
            })
            .catch(err => {
                Logger.error(err);
            });

        return createdUser;
    }

    async validateValidNewUser(user: User) {
        // validate mail and phone must be uniqued
        const existedUserPhoneOrEmail = await this.usersService.findOneByEmailOrPhone(
            user.email,
            user.phone,
        );

        if (existedUserPhoneOrEmail) {
            const error = new ApiError();
            if (user.email === existedUserPhoneOrEmail.email) {
                error.add('email', 'Địa chỉ email đã có người sử dụng');
            }
            if (user.phone === existedUserPhoneOrEmail.phone) {
                error.add('phone', 'Số điện thoại đã có người sử dụng');
            }

            throw new BadRequestException(error.messages);
        }
    }

    requestResetPassword(user: User, redirectUrl: string): void {
        const token = this.generateToken(user, TokenTypes.ResetPasswordToken);
        const linkResetWithToken = `${redirectUrl}?token=${token}`;

        this.mailerService
            .sendMail({
                to: user.email,
                subject: MailSubject.RESET_PASSWORD,
                template: 'request-reset-password',
                context: {
                    name: `${user.lastName} ${user.firstName}`,
                    linkResetWithToken,
                },
            })
            .catch(err => {
                console.log(err);
            });
    }

    async resetPassword(user: User, newPassword: string): Promise<void> {
        user.passwordHash = await bcrypt.hash(
            newPassword,
            CommonConfig.SALT_ROUNDS,
        );

        await this.usersService.update(user);
    }
}
