import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';

import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { RequestUser } from './dto/req-user.interface';
import { UsersService } from 'users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        readonly configService: ConfigService,
        private readonly usersService: UsersService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get<string>('JWT_SECRET_KEY'),
        });
    }
    async validate(requestUser: RequestUser): Promise<RequestUser> {
        if (!requestUser || !requestUser.id) {
            return null;
        }
        const validUser = await this.usersService.getActiveUserByEmail(
            requestUser.email,
        );
        if (!validUser) {
            throw new UnauthorizedException(
                'Token không hợp lệ hoặc đã hết hạn',
            );
        }
        return {
            id: requestUser.id,
            email: requestUser.email,
            status: requestUser.status,
            role: requestUser.role,
        };
    }
}
