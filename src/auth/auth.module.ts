import { Module, forwardRef } from '@nestjs/common';

import { AnonymousStrategy } from './anonymous.strategy';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { CartsModule } from 'carts/carts.module';
import { ConfigModule } from 'config/config.module';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'users/users.module';

@Module({
    imports: [
        forwardRef(() => UsersModule),
        PassportModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => {
                return {
                    secret: configService.get<string>('JWT_SECRET_KEY'),
                    signOptions: {
                        expiresIn: configService.get<string>(
                            'JWT_EXPIRATION_TIME',
                        ),
                    },
                };
            },
            inject: [ConfigService],
        }),
        forwardRef(() => CartsModule),
    ],
    providers: [AuthService, JwtStrategy, AnonymousStrategy],
    controllers: [AuthController],
    exports: [AuthService],
})
export class AuthModule {}
