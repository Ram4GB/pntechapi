import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { RequestUser } from './dto/req-user.interface';
import { Roles } from 'shared/constants/roles';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const roles = this.reflector.get<string[]>(
            'roles',
            context.getHandler(),
        );

        const notSetRole = !roles || !roles.length;
        const setRoleIsAnonymous = roles && roles.includes(Roles.Anonymous);

        if (notSetRole || setRoleIsAnonymous) {
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const user: RequestUser = request.user;

        return this.matchRoles(roles, user.role);
    }

    matchRoles(roles: string[], userRole: string): boolean {
        return roles.includes(userRole);
    }
}
