import { APP_FILTER, APP_PIPE } from '@nestjs/core';

import { AllExceptionFilter } from 'shared/filters/all-exception.filter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from 'auth/auth.module';
import { BannersModule } from './banners/banners.module';
import { BlogsModule } from './blogs/blogs.module';
import { CartsModule } from 'carts/carts.module';
import { CategoriesModule } from 'categories/categories.module';
import { CategorydiscountsModule } from 'categorydiscounts/categorydiscounts.module';
import { ChatsModule } from './chats/chats.module';
import { ConfigModule } from 'config/config.module';
import { ConfigService } from '@nestjs/config';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { HelpersModule } from 'shared/helpers/helpers.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { MediaModule } from 'media/media.module';
import { Module } from '@nestjs/common';
import { MulterConfigService } from 'config/multer-config.services';
import { MulterModule } from '@nestjs/platform-express';
import { OrdersModule } from 'orders/orders.module';
import { PaginationModule } from 'shared/pagination/pagination.module';
import { ProductImportsModule } from './product-imports/product-imports.module';
import { ProductsModule } from 'products/products.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'users/users.module';
import { ValidationPipe } from 'shared/pipes/validation.pipe';
import { join } from 'path';

@Module({
    imports: [
        ConfigModule,
        TypeOrmModule.forRootAsync({
            inject: [ConfigService],
            useFactory: (configService: ConfigService) =>
                configService.get('database'),
        }),
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'uploads'),
            serveRoot: '/uploads',
        }),
        MulterModule.registerAsync({
            useClass: MulterConfigService,
        }),
        MailerModule.forRoot({
            transport:
                'smtp://testmail231199@gmail.com:quyen123@smtp.gmail.com',
            defaults: {
                from: '"PnTech" <testmail231199@gmail.com>',
            },
            template: {
                dir: join(__dirname, '..', '/mail-templates'),
                adapter: new HandlebarsAdapter(),
                options: {
                    strict: true,
                },
            },
        }),
        ProductsModule,
        CategoriesModule,
        CategorydiscountsModule,
        PaginationModule,
        HelpersModule,
        UsersModule,
        AuthModule,
        CartsModule,
        OrdersModule,
        MediaModule,
        BannersModule,
        ChatsModule,
        BlogsModule,
        ProductImportsModule,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        { provide: APP_PIPE, useClass: ValidationPipe },
        { provide: APP_FILTER, useClass: AllExceptionFilter },
    ],
})
export class AppModule {}
